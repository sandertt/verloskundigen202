<?php add_action( 'vc_before_init', 'dt_sc_portfolio_item_vc_map' );
function dt_sc_portfolio_item_vc_map() {
	vc_map( array(
		"name" => esc_html__( "Single Portfolio Item", 'pregnancy-core' ),
		"base" => "dt_sc_portfolio_item",
		"icon" => "dt_sc_portfolio_item",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			// Post ID
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'ID', 'pregnancy-core' ),
				'param_name' => 'id',
				'description' => esc_html__( 'Enter post ID', 'pregnancy-core' ),
				'admin_label' => true
			),

			// Post style
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Style','pregnancy-core'),
				'param_name' => 'style',
				'value' => array(
					esc_html__('Type 1','pregnancy-core') => 'type1', 
					esc_html__('Type 2','pregnancy-core') => 'type2', 
					esc_html__('Type 3','pregnancy-core') => 'type3', 
					esc_html__('Type 4','pregnancy-core') => 'type4', 
					esc_html__('Type 5','pregnancy-core') => 'type5', 
					esc_html__('Type 6','pregnancy-core') => 'type6', 
					esc_html__('Type 7','pregnancy-core') => 'type7', 
					esc_html__('Type 8','pregnancy-core') => 'type8', 
					esc_html__('Type 9','pregnancy-core') => 'type9',
					esc_html__('Pregnancy Portfolio style','pregnancy-core') => 'dt-sc-portfolio-content'
				),
				'std' => 'type1'
			)
		)
	) );
}?>