<?php add_action( 'vc_before_init', 'dt_sc_pricing_table_item_pregnancy_vc_map' );
function dt_sc_pricing_table_item_pregnancy_vc_map() {
	vc_map( array( 
		"name" => esc_html__( "Pricing Table ", 'pregnancy-core' ),
		"base" => "dt_sc_pricing_table_item_pregnancy",
		"icon" => "dt_sc_pricing_table_item_pregnancy",
		"category" => DT_VC_CATEGORY,		
		"params" => array(

			// Heading
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", 'pregnancy-core' ),
				'admin_label' => true,
      			"param_name" => "heading"
      		),

      		// selected
      		array(
      			'type' => 'checkbox',
      			'heading' => esc_html__( 'Is active?', 'pregnancy-core' ),
				'admin_label' => true,
      			'param_name' => 'highlight',
      			'description' => esc_html__( 'If checked pricing box will be highlighted', 'pregnancy-core' ),
      			'value' => array( esc_html__( 'Yes', 'pregnancy-core' ) => 'yes' )
      		),


			// Currency
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Currency", 'pregnancy-core' ),
      			"param_name" => "currency",
      			"description" => esc_html__("Enter the currency for price e.g. $157.99 enter $ here",'pregnancy-core'),
      		),

			// Price
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Price", 'pregnancy-core' ),
      			"param_name" => "price",
      			"description" => esc_html__("Enter the price for this package e.g. $157.99 enter 157 here",'pregnancy-core'),
      			),


      		// Content
      		array(
      			'type' => 'textarea_html',
      			'heading' => esc_html__( 'Content', 'pregnancy-core' ),
      			'param_name' => 'content',
				'value' => '<ul><li>Lorem ipsum dolor sit</li><li>Praesent convallis nibh</li><li>Nullam ac sapien sit</li><li>Phasellus auctor augue</li></ul>'
      		),

		)
	) );
}?>