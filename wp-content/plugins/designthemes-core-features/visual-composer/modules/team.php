<?php add_action( 'vc_before_init', 'dt_sc_team_vc_map' );
function dt_sc_team_vc_map() {
	vc_map( array(
		"name" => esc_html__( "Team", 'pregnancy-core' ),
		"base" => "dt_sc_team",
		"icon" => "dt_sc_team",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			# Name
			array(
				'type' => 'textfield',
				'param_name' => 'name',
				'heading' => esc_html__( 'Name', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter name', 'pregnancy-core' )
			),

			# Role
			array(
				'type' => 'textfield',
				'param_name' => 'role',
				'heading' => esc_html__( 'Role', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter role', 'pregnancy-core' )
			),

			# Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image','pregnancy-core'),
                'param_name' => 'image'
            ),
			
            # Team style
            array(
				'type' => 'dropdown',
				'heading' => esc_html__('Team Style','pregnancy-core'),
            	'param_name' => 'teamstyle',
            	'value' => array(
            		esc_html__('Default','pregnancy-core') => '',
					esc_html__('Social on hover','pregnancy-core') => 'hide-social-show-on-hover',
					esc_html__('Social and Role on hover','pregnancy-core') => 'hide-social-role-show-on-hover',
					esc_html__('Details on hover','pregnancy-core') => 'hide-details-show-on-hover',
					esc_html__('Show details & Social on hover','pregnancy-core') => 'hide-social-show-on-hover details-on-image',
					esc_html__('Horizontal','pregnancy-core') => 'type2',
					esc_html__('Rounded','pregnancy-core') => 'hide-social-show-on-hover rounded',
					esc_html__('With Qualification','pregnancy-core') => 'with-qualification'
            	)
            ),
			
			# Qualification
			array(
				'type' => 'textfield',
				'param_name' => 'qualification',
				'heading' => esc_html__( 'Qualification', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter Qualification', 'pregnancy-core' ),
				'dependency' => array( 'element' => 'teamstyle', 'value' => 'with-qualification')
			),
			
			# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-core" ),
      			"param_name" => "title",
      			"admin_label" => true,
				'dependency' => array( 'element' => 'teamstyle', 'value' => 'with-qualification')
      		),
			
			// Title Link
			array(
				'type' => 'vc_link',
				'heading' => esc_html__( 'Title Link', 'pregnancy-core' ),
				'param_name' => 'link',
				'description' => esc_html__( 'Add link to title', 'pregnancy-core' ),
			),

            # Social Icon Style
            array(
				'type' => 'dropdown',
				'heading' => esc_html__('Social Icon Style','pregnancy-core'),
            	'param_name' => 'socialstyle',
            	'value' => array(
            		esc_html__('Default','pregnancy-core') => '' ,
            		esc_html__('Rounded Border','pregnancy-core') => 'rounded-border' ,
            		esc_html__('Rounded Square','pregnancy-core') => 'rounded-square' ,
            		esc_html__('Square Border','pregnancy-core') => 'square-border' ,
            		esc_html__('Diamond Square Border','pregnancy-core') => 'diamond-square-border' ,
            		esc_html__('Hexagon Border','pregnancy-core') => 'hexagon-border'
            	)
            ),

            # Facebook
			array(
				'type' => 'textfield',
				'param_name' => 'facebook',
				'heading' => esc_html__( 'Facebook', 'pregnancy-core' ),
			),

            # Twitter
			array(
				'type' => 'textfield',
				'param_name' => 'twitter',
				'heading' => esc_html__( 'Twitter', 'pregnancy-core' ),
			),

            # Google
			array(
				'type' => 'textfield',
				'param_name' => 'pinterest',
				'heading' => esc_html__( 'Pinterest', 'pregnancy-core' ),
			),

      		// Content
            array(
            	'type' => 'textarea_html',
            	'heading' => esc_html__('Content','pregnancy-core'),
            	'param_name' => 'content',
            	'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit elit turpis, a porttitor tellus sollicitudin at.'
            ),

      		# Class
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-core' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular icon box element differently - add a class name and refer to it in custom CSS','pregnancy-core')
      		)			
		)
	) );	
}?>