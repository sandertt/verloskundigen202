<?php add_action( 'vc_before_init', 'dt_sc_team_carousel_wrapper_vc_map' );
function dt_sc_team_carousel_wrapper_vc_map() {

	class WPBakeryShortCode_dt_sc_team_carousel_wrapper extends WPBakeryShortCodesContainer {
	}

	class WPBakeryShortCode_dt_sc_team_carousel_item extends WPBakeryShortCode {
	}

	vc_map( array(
		"name" => esc_html__( "Team carousel", 'pregnancy-core' ),
		"base" => "dt_sc_team_carousel_wrapper",
		"icon" => "dt_sc_team_carousel_wrapper",
		"category" => DT_VC_CATEGORY,
		"content_element" => true,
		"js_view" => 'VcColumnView',
		'as_parent' => array( 'only' => 'dt_sc_team_carousel_item' ),
		'description' => esc_html__( 'Team carousel wrapper', 'pregnancy-core' ),
		"params" => array(


			#Scroll
			array(
				'type' => 'textfield',
				'param_name' => 'scroll',
				'heading' => esc_html__( 'Scroll', 'pregnancy-core' ),
				'value' => '3',				
				'description' => esc_html__( 'The number of items to scroll at once', 'pregnancy-core' )
			),

			#Visible
			array(
				'type' => 'textfield',
				'param_name' => 'visible',
				'heading' => esc_html__( 'Visible', 'pregnancy-core' ),
				'value' => '3',
 				'description' => esc_html__( 'The number of items to show at once', 'pregnancy-core' )
			),
			
			# Class
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-core' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular element differently - add a class name and refer to it in custom CSS','pregnancy-core')
      		)			
		)		
	) );
}?>