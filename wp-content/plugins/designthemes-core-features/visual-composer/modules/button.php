<?php add_action( 'vc_before_init', 'dt_sc_button_vc_map' );
function dt_sc_button_vc_map() {

	global $variations;

	global $dt_animation_types;

	vc_map( array(
		"name" => esc_html__( "Button", 'pregnancy-core' ),
		"base" => "dt_sc_button",
		"icon" => "dt_sc_button",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			// Button Text
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Text', 'pregnancy-core' ),
				'param_name' => 'title',
				'value' => esc_html__( 'Text on the button', 'pregnancy-core' ),
			),

			// Button Link
			array(
				'type' => 'vc_link',
				'heading' => esc_html__( 'URL (Link)', 'pregnancy-core' ),
				'param_name' => 'link',
				'description' => esc_html__( 'Add link to button', 'pregnancy-core' ),
			),

			// Button Size
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Size', 'pregnancy-core' ),
				'description' => esc_html__( 'Select button display size', 'pregnancy-core' ),
				'param_name' => 'size',
				'value' => array(
					esc_html__( 'Small', 'pregnancy-core' ) => 'small',
					esc_html__( 'Medium', 'pregnancy-core' ) => 'medium',
					esc_html__( 'Large', 'pregnancy-core' ) => 'large',
					esc_html__( 'Xlarge', 'pregnancy-core' ) => 'xlarge',
				),
				'std' => 'small'
			),

			// Content Color			
			array(
				"type" => "colorpicker",
      			"heading" => esc_html__( "Text color", 'pregnancy-core' ),
      			"param_name" => "textcolor",
      			"description" => esc_html__( "Select text color", 'pregnancy-core' ),
      		),

      		// Variation
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__( 'Background Color', 'pregnancy-core' ),
      			'admin_label' => true,
      			'param_name' => 'color',
      			'value' => $variations,
      			'description' => esc_html__( 'Select button background color', 'pregnancy-core' ),
      		),

			// BG Color			
			array(
				"type" => "colorpicker",
      			"heading" => esc_html__( "Custom Background color", 'pregnancy-core' ),
      			"param_name" => "bgcolor",
      			"description" => esc_html__( "Select button background color", 'pregnancy-core' ),
				'dependency' => array( 'element' => 'color', 'value' =>'-' )
      		),      		      					

			// Button Style
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Style', 'pregnancy-core' ),
				'description' => esc_html__( 'Select button display style', 'pregnancy-core' ),
				'param_name' => 'style',
				'value' => array(
					esc_html__( 'None', 'pregnancy-core') => '',
					esc_html__( 'Bordered', 'pregnancy-core' ) => 'bordered',
					esc_html__( 'Filled', 'pregnancy-core' ) => 'filled',
					esc_html__( 'Filled Rounded Corner', 'pregnancy-core' ) => 'filled rounded-corner',
					esc_html__( 'Rounded Corner', 'pregnancy-core' ) => 'rounded-corner',
					esc_html__( 'Rounded Border', 'pregnancy-core' ) => 'rounded-border',
					esc_html__( 'Fully Rounded Border', 'pregnancy-core' ) => 'fully-rounded-border',
				),				
			),

			// Icon Type
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__('Icon Type','pregnancy-core'),
      			'param_name' => 'icon_type',
      			'value' => array(
      				esc_html__('None', 'pregnancy-core' ) => '',	 
      				esc_html__('Font Awesome', 'pregnancy-core' ) => 'fontawesome' ,
      				esc_html__('Class','pregnancy-core') => 'css_class'
      			),
      			'std' => ''
      		),

			// Icon Alignment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon Alignment', 'pregnancy-core' ),
				'description' => esc_html__( 'Select icon alignment', 'pregnancy-core' ),
				'param_name' => 'iconalign',
				'value' => array(
					esc_html__( 'Left', 'pregnancy-core' ) => 'icon-left with-icon',
					esc_html__( 'Right', 'pregnancy-core' ) => 'icon-right with-icon',
				),
				'dependency' => array( 'element' => 'icon_type', 'value' => array( 'fontawesome', 'css_class' ) ),
				'std' => ''
			),

      		// Font Awesome
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Font Awesome', 'pregnancy-core' ),
				'param_name' => 'iconclass',
				'settings' => array( 'emptyIcon' => false, 'iconsPerPage' => 4000 ),
				'dependency' => array( 'element' => 'icon_type', 'value' => 'fontawesome' ),
				'description' => esc_html__( 'Select icon from library', 'pregnancy-core' ),
			),

			// Custom Class
            array(
            	'type' => 'textfield',
            	'heading' => esc_html__( 'Custom icon class', 'pregnancy-core' ),
            	'param_name' => 'icon_css_class',
            	'dependency' => array( 'element' => 'icon_type', 'value' => 'css_class' )
            ),

			// Button Animation
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Animation', 'pregnancy-core' ),
				'description' => esc_html__( 'Select button animation', 'pregnancy-core' ),
				'param_name' => 'animation',
				'value' => $dt_animation_types
			),			

          	// Extra class name
          	array(
          		'type' => 'textfield',
          		'heading' => esc_html__( 'Extra class name', 'pregnancy-core' ),
          		'param_name' => 'class',
          		'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'pregnancy-core' )
          	),

			// Custom CSS
			array(
				'type' => 'css_editor',
				'heading' => esc_html__( 'CSS box', 'pregnancy-core' ),
				'param_name' => 'css',
				'group' => esc_html__( 'Design Options', 'pregnancy-core' )
			),
		)
	) );
} ?>