<?php add_action( 'vc_before_init', 'dt_sc_testimonial_vc_map' );
function dt_sc_testimonial_vc_map() {
	vc_map( array(
		"name" => esc_html__( "Testimonial", 'pregnancy-core' ),
		"base" => "dt_sc_testimonial",
		"icon" => "dt_sc_testimonial",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			# Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Type', 'pregnancy-core' ),
				'description' => esc_html__( 'Select testimonial type', 'pregnancy-core' ),
				'param_name' => 'type',
				'value' => array(
					esc_html__('Type 1','pregnancy-core') => 'type1',
					esc_html__('Type 2','pregnancy-core') => 'type2',
					esc_html__('Type 3','pregnancy-core') => 'type3',
					esc_html__('Type 4','pregnancy-core') => 'type4',
					esc_html__('Type 5','pregnancy-core') => 'type5',
					esc_html__('Type 6','pregnancy-core') => 'type6',
					esc_html__('Type 7','pregnancy-core') => 'type7',
					esc_html__('Type 8','pregnancy-core') => 'type8',
					esc_html__('Client Quotes','pregnancy-core') => 'dt-sc-clients-quotes'
				),
				'std' => 'type1'
			),

			# Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image','pregnancy-core'),
                'param_name' => 'image'
            ),

			# Name
			array(
				'type' => 'textfield',
				'param_name' => 'name',
				'heading' => esc_html__( 'Name', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter name', 'pregnancy-core' )
			),

			# Role
			array(
				'type' => 'textfield',
				'param_name' => 'role',
				'heading' => esc_html__( 'Role', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter role', 'pregnancy-core' )
			),

      		// Content
            array(
            	'type' => 'textarea_html',
            	'heading' => esc_html__('Content','pregnancy-core'),
            	'param_name' => 'content',
            	'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit elit turpis, a porttitor tellus sollicitudin at.Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
            ),

      		# Class
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-core' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular icon box element differently - add a class name and refer to it in custom CSS','pregnancy-core')
      		)						
		)
	) );
} ?>