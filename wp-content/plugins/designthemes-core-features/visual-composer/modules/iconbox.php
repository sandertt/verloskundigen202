<?php add_action( 'vc_before_init', 'dt_sc_iconbox_vc_map' );
function dt_sc_iconbox_vc_map() {

	vc_map( array(
		"name" => esc_html__( "Icon box", 'pregnancy-core' ),
        "base" => "dt_sc_iconbox",
		"icon" => "dt_sc_iconbox",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			# Types
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__( 'Types', 'pregnancy-core' ),
      			'param_name' => 'type',
      			'value' => array( 
      				esc_html__('Type 1','pregnancy-core') => 'type1',		esc_html__('Type 2','pregnancy-core') => 'type2',		esc_html__('Type 3','pregnancy-core') => 'type3',
      				esc_html__('Type 4','pregnancy-core') => 'type4',		esc_html__('Type 5','pregnancy-core') => 'type5',		esc_html__('Type 6','pregnancy-core') => 'type6',
      				esc_html__('Type 7','pregnancy-core') => 'type7',		esc_html__('Type 8','pregnancy-core') => 'type8',		esc_html__('Type 9','pregnancy-core') => 'type9',
      				esc_html__('Type 10','pregnancy-core') => 'type10',		esc_html__('Type 11','pregnancy-core') => 'type11',      esc_html__('Type 12','pregnancy-core') => 'type12',
                    esc_html__('Type 13','pregnancy-core') => 'type13',      esc_html__('Type 14','pregnancy-core') => 'type14', 		
					esc_html__('Bordered Icon Box','pregnancy-core') => 'bordered-image-icon-box', esc_html__('Icon Box with Sub Title','pregnancy-core') => 'icon-box-with-subtitle'
      			),
      			'description' => esc_html__( 'Select icon box type', 'pregnancy-core' ),
      			'std' => 'type1',
      			'admin_label' => true
      		),

      		# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", 'pregnancy-core' ),
      			"param_name" => "title"
      		),

      		# Sub Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Sub Title", 'pregnancy-core' ),
      			"param_name" => "subtitle",
				"dependency" => array(
					"element" => "type",
					"value" => array('type1','type2','type3','type4','type5','type6','type7','type8','type9','type10','type11','type12','type13','type14','icon-box-with-subtitle'),
				)
      		),
			
			# pregnancy Custom Icon
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Image', 'pregnancy-core' ),
				'param_name' => 'pregnancy_iconurl',
				'description' => esc_html__( 'Select image from media library', 'pregnancy-core' ),
				'dependency' => array( 'element' => 'type', 'value' => array('bordered-image-icon-box','icon-box-with-subtitle') )
			),

      		# Icon Type
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__('Icon Type','pregnancy-core'),
      			'param_name' => 'icon_type',
      			'value' => array( 
                              esc_html__('Image','pregnancy-core') => 'image',
                              esc_html__('Font Awesome', 'pregnancy-core' ) => 'fontawesome' ,
                              esc_html__('Class','pregnancy-core') => 'css_class',
                              esc_html__('None','pregnancy-core') => 'none' ),
      			'std' => 'fontawesome',
				"dependency" => array(
					"element" => "type",
					"value" => array('type1','type2','type3','type4','type5','type6','type7','type8','type9','type10','type11','type12','type13','type14'),
				),
      		),

      		# Font Awesome
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Font Awesome', 'pregnancy-core' ),
				'param_name' => 'icon',
				'value' => 'fa fa-info-circle',
				'settings' => array( 'emptyIcon' => false, 'iconsPerPage' => 4000 ),
				'dependency' => array(
					'element' => 'icon_type',
					'value' => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library', 'pregnancy-core' ),
			),

			# Custom Icon
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Image', 'pregnancy-core' ),
				'param_name' => 'iconurl',
				'description' => esc_html__( 'Select image from media library', 'pregnancy-core' ),
				'dependency' => array( 'element' => 'icon_type', 'value' => 'image' )
			),

                  # Custom Class
                  array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Custom class', 'pregnancy-core' ),
                        'param_name' => 'icon_css_class',
                        'value' => '',
                        'dependency' => array(
                              'element' => 'icon_type',
                              'value' => 'css_class',
                        )
                  ),      		

      		# URL
      		array(
      			'type' => 'vc_link',
      			'heading' => esc_html__( 'URL (Link)', 'pregnancy-core' ),
      			'param_name' => 'link',
      			'description' => esc_html__( 'Add link to icon box', 'pregnancy-core' )
      		),

      		# Content
      		array(
      			'type' => 'textarea_html',
      			'heading' => esc_html__( 'Content', 'pregnancy-core' ),
      			'param_name' => 'content',
      			'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit elit turpis, a porttitor tellus sollicitudin at. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'
      		),

      		# Class
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-core' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular icon box element differently - add a class name and refer to it in custom CSS','pregnancy-core')
      		),

                  array(
                        'type' => 'textarea',
                        'heading' => "Inline styles",
                        'param_name' => 'addstyles',
                        'description' => esc_html__( 'Enter inline styles for this iconbox', 'pregnancy-core' )
                  )      		
		)
	) );
}?>