<?php add_action( 'vc_before_init', 'dt_sc_portfolios_vc_map' );
function dt_sc_portfolios_vc_map() {

	$arr = array( esc_html__('Yes','pregnancy-core') => 'yes', esc_html__('No','pregnancy-core') => 'no' );

	vc_map( array(
		"name" => esc_html__( "Portfolio Items", 'pregnancy-core' ),
		"base" => "dt_sc_portfolios",
		"icon" => "dt_sc_portfolios",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			// Post Count
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Post Counts', 'pregnancy-core' ),
				'param_name' => 'count',
				'description' => esc_html__( 'Enter post count', 'pregnancy-core' ),
				'admin_label' => true
			),

			// Post column
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Columns','pregnancy-core'),
				'param_name' => 'column',
				'value' => array(
					esc_html__('II Columns','pregnancy-core') => 2 ,
					esc_html__('III Columns','pregnancy-core') => 3,
					esc_html__('IV Columns','pregnancy-core') => 4,

				),
				'std' => '3'
			),

			// Post style
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Style','pregnancy-core'),
				'param_name' => 'style',
				'value' => array(
					esc_html__('Type 1','pregnancy-core') => 'type1', 
					esc_html__('Type 2','pregnancy-core') => 'type2', 
					esc_html__('Type 3','pregnancy-core') => 'type3', 
					esc_html__('Type 4','pregnancy-core') => 'type4', 
					esc_html__('Type 5','pregnancy-core') => 'type5', 
					esc_html__('Type 6','pregnancy-core') => 'type6', 
					esc_html__('Type 7','pregnancy-core') => 'type7', 
					esc_html__('Type 8','pregnancy-core') => 'type8', 
					esc_html__('Type 9','pregnancy-core') => 'type9',
					esc_html__('Pregnancy Portfolio style','pregnancy-core') => 'dt-sc-portfolio-content'
				),
				'std' => 'type1',
				'admin_label' => true
			),

			// Allow Grid Space
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Allow Grid Space','pregnancy-core'),
				'param_name' => 'allow_gridspace',
				'value' => $arr
			),

			// Allow Filter
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Allow Filter','pregnancy-core'),
				'param_name' => 'allow_filter',
				'value' => $arr
			),

			// Term ID(s)
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Terms', 'pregnancy-core' ),
				'param_name' => 'terms',
				'description' => esc_html__( 'Enter Portfolio Terms', 'pregnancy-core' )
			),						
		)
	) );
} ?>