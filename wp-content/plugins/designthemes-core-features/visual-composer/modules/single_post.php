<?php add_action( 'vc_before_init', 'dt_sc_post_vc_map' );
function dt_sc_post_vc_map() {

	$arr = array( esc_html__('Yes','pregnancy-core') => 'yes', esc_html__('No','pregnancy-core') => 'no' );

	vc_map( array(
		"name" => esc_html__( "Single Post", 'pregnancy-core' ),
		"base" => "dt_sc_post",
		"icon" => "dt_sc_post",
		"category" => DT_VC_CATEGORY,
		"description" => esc_html__("Show single post",'pregnancy-core'),
		"params" => array(

			// Post ID
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'ID', 'pregnancy-core' ),
				'param_name' => 'id',
				'description' => esc_html__( 'Enter post ID', 'pregnancy-core' ),
				'admin_label' => true
			),

			// Post style
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Style','pregnancy-core'),
				'param_name' => 'style',
				'value' => array(
					esc_html__('Default','pregnancy-core') => '',
					esc_html__('Date Left','pregnancy-core') => 'entry-date-left',
					esc_html__('Date and Author Left','pregnancy-core') => 'entry-date-author-left',
					esc_html__('Medium','pregnancy-core') => 'blog-medium-style',
					esc_html__('Medium Highlight','pregnancy-core') => 'blog-medium-style dt-blog-medium-highlight',
					esc_html__('Medium Skin Highlight','pregnancy-core') => 'blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight',
					esc_html__('Pregnancy Blog Style','pregnancy-core') => 'dt-sc-blog-gradient-content'	
				)
			),

			// Allow excerpt
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Allow Excerpt','pregnancy-core'),
				'param_name' => 'allow_excerpt',
				'value' => $arr,
				'dependency' => array( 'element' => 'style', 'value' => array('','entry-date-left','entry-date-author-left','blog-medium-style','blog-medium-style dt-blog-medium-highlight','blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight') )
			),

			// Excerpt Length
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Excerpt Length', 'pregnancy-core' ),
				'param_name' => 'excerpt_length',
				'value' => 40,
				'dependency' => array( 'element' => 'style', 'value' => array('','entry-date-left','entry-date-author-left','blog-medium-style','blog-medium-style dt-blog-medium-highlight','blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight') )
			),

			// Show Post Format?
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show Post Format?','pregnancy-core'),
				'param_name' => 'show_post_format',
				'value' => $arr
			),

			// Show Author ?
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show Author ?','pregnancy-core'),
				'param_name' => 'show_author',
				'value' => $arr
			),

			// Show Date ?
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show Date ?','pregnancy-core'),
				'param_name' => 'show_date',
				'value' => $arr
			),

			// Show Comment ?
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show Comment ?','pregnancy-core'),
				'param_name' => 'show_comment',
				'value' => $arr
			),

			// Show Category?
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show Category?','pregnancy-core'),
				'param_name' => 'show_category',
				'value' => $arr
			),

			// Show Tag?
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show Tag?','pregnancy-core'),
				'param_name' => 'show_tag',
				'value' => $arr
			),

			// Button Style
			array(
				'type' => 'textarea_html',
				'heading' => esc_html__('Read more Button','pregnancy-core'),
				'param_name' => 'content',
				'value' => '[dt_sc_button size="small" iconclass="fa fa-long-arrow-right" iconalign="icon-right with-icon" style="filled" target="_blank" class="type1" title="Read More"/]'
			)
		)
	) );	
}?>