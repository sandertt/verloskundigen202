<?php add_action( 'vc_before_init', 'dt_sc_number_counter_vc_map' );
function dt_sc_number_counter_vc_map() {

	global $variations;

	vc_map( array(
		"name" => esc_html__("Counting Number", 'pregnancy-core'),
		"base" => "dt_sc_number_counter",
		"icon" => "dt_sc_number_counter",
		"category" => DT_VC_CATEGORY,
		"description" => esc_html__("Add different types of counting number",'pregnancy-core'),
		"params" => array(

			# Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Type', 'pregnancy-core'),
				'param_name' => 'type',
				'value' => array( esc_html__('Type1','pregnancy-core') => 'type1',
					esc_html__('Type2','pregnancy-core') => 'type2',
					esc_html__('Type3','pregnancy-core') => 'type3',
					esc_html__('Type4','pregnancy-core') => 'type4',
					esc_html__('Type5','pregnancy-core') => 'type5',
					esc_html__('Type6','pregnancy-core') => 'type6', 
					esc_html__('Rounded Box','pregnancy-core') => 'dt-sc-rounded-box',
					esc_html__('Image Flip Counter','pregnancy-core') => 'image-flip-counter' ),
				'std' => 'type1'
			),

			# Icon Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Icon Type', 'pregnancy-core'),
				'param_name' => 'icon_type',
				'value' => array( esc_html__('Icon class','pregnancy-core') => 'icon_class', esc_html__('Image','pregnancy-core') => 'icon_url' ),
				'dependency' => array('element' => 'type','value' => array('type1','type2','type3','type4','type5','type6','dt-sc-rounded-box') )
			),

			# Icon Class
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Icon Class', 'pregnancy-core'),
				'param_name' => 'icon',
				'dependency' => array('element' => 'icon_type','value' => 'icon_class')
			),

			# Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image URL', 'pregnancy-core'),
				'param_name' => 'iconurl',
				'dependency' => array('element' => 'icon_type','value' => array('icon_url') )
			),
			
			# Background Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Background Image URL', 'pregnancy-core'),
				'param_name' => 'bg_iconurl',
				'dependency' => array('element' => 'type','value' => array('dt-sc-rounded-box') )
			),
			
			# Counter Image 1
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image URL', 'pregnancy-core'),
				'param_name' => 'counter_iconurl',
				'dependency' => array('element' => 'type','value' => array('image-flip-counter') )
			),
			
			# Hover Image 2
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Counter Hover Image URL', 'pregnancy-core'),
				'param_name' => 'hover_iconurl',
				'dependency' => array('element' => 'type','value' => 'image-flip-counter')
			),


      		# Value
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Number Value", 'pregnancy-core' ),
      			"param_name" => "value",
      		),

      		# Append Text
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Append Text", 'pregnancy-core' ),
      			"param_name" => "append",
				'dependency' => array('element' => 'type','value' => array('type1','type2','type3','type4','type5','type6','dt-sc-rounded-box') )
      		),

      		# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", 'pregnancy-core' ),
      			"param_name" => "title",
      		),

			# Extra Class
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-core' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular icon box element differently - add a class name and refer to it in custom CSS','pregnancy-core')
      		)
		)
	) );	
}?>