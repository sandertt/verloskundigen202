<?php add_action( 'vc_before_init', 'dt_sc_intro_text_vc_map' );
function dt_sc_intro_text_vc_map() {
	vc_map( array(
		"name" => esc_html__( "Intro Text", 'pregnancy-core' ),
		"base" => "dt_sc_intro_text",
		"icon" => "dt_sc_intro_text",
		"category" => DT_VC_CATEGORY,
		'description' => esc_html__( 'Section for Intro Section', 'pregnancy-core' ),
		"params" => array(

			# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-core" ),
      			"param_name" => "title",
				"value" => esc_html__( 'Having Personal Questions? You can talk to a doctor' ),
      			"admin_label" => true
      		),
			
			// Button Text
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Text', 'pregnancy-core' ),
				'param_name' => 'button_title',
				'value' => esc_html__( 'Talk To Us', 'pregnancy-core' ),
			),

			// Button Link
			array(
				'type' => 'vc_link',
				'heading' => esc_html__( 'URL (Link)', 'pregnancy-core' ),
				'param_name' => 'link',
				'description' => esc_html__( 'Add link to button', 'pregnancy-core' ),
			),

			// Font Awesome
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Font Awesome', 'pregnancy-core' ),
				'param_name' => 'iconclass',
				'settings' => array( 'emptyIcon' => false, 'iconsPerPage' => 4000 ),
				'description' => esc_html__( 'Select icon from library', 'pregnancy-core' ),
			),
			
			// Extra class name
          	array(
          		'type' => 'textfield',
          		'heading' => esc_html__( 'Extra class name', 'pregnancy-core' ),
          		'param_name' => 'class',
          		'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'pregnancy-core' )
          	),
			
		)
	) );
}?>