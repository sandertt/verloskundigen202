<?php add_action( 'vc_before_init', 'dt_sc_partners_carousel_vc_map' );
function dt_sc_partners_carousel_vc_map() {
	vc_map( array(
		"name" => esc_html__( "Partners carousel", 'pregnancy-core' ),
		"base" => "dt_sc_partners_carousel",
		"icon" => "dt_sc_partners_carousel",
		"category" => DT_VC_CATEGORY,
		'description' => esc_html__( 'Partners carousel with images', 'pregnancy-core' ),
		"params" => array(
			
			#Image
			array(
				'type' => 'attach_images',
				'heading' => esc_html__( 'Images', 'pregnancy-core' ),
				'param_name' => 'images',
				'description' => esc_html__( 'Select partner images from media library', 'pregnancy-core' )
			),

			#Scroll
			array(
				'type' => 'textfield',
				'param_name' => 'scroll',
				'heading' => esc_html__( 'Scroll', 'pregnancy-core' ),
				'value' => '3',				
				'description' => esc_html__( 'The number of items to scroll at once', 'pregnancy-core' )
			),

			#Visible
			array(
				'type' => 'textfield',
				'param_name' => 'visible',
				'heading' => esc_html__( 'Visible', 'pregnancy-core' ),
				'value' => '3',
 				'description' => esc_html__( 'The number of items to show at once', 'pregnancy-core' )
			)
		)	 
	) );
}?>