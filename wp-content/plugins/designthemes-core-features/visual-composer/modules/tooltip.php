<?php add_action( 'vc_before_init', 'dt_sc_tooltip_vc_map' );
function dt_sc_tooltip_vc_map() {
	vc_map( array(
		"name" => esc_html__( "Tooltip", 'pregnancy-core' ),
		"base" => "dt_sc_tooltip",
		"icon" => "dt_sc_tooltip",
		"category" => DT_VC_CATEGORY,
		'description' => esc_html__( 'Section for Tooltips', 'pregnancy-core' ),
		"params" => array(

			// Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Type', 'pregnancy-core'),
				'param_name' => 'type',
				'admin_label' => true,
				'value' => array(
					esc_html__('Default','pregnancy-core') => '',
					esc_html__('Boxed','pregnancy-core') => 'boxed',
					esc_html__('Line','pregnancy-core') => 'dt-sc-tooltip-one',
					esc_html__('Box','pregnancy-core') => 'dt-sc-tooltip-two',
					esc_html__('Classic','pregnancy-core') => 'dt-sc-tooltip-three',
				),
				'std' => ''
			),

			// Position
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Position', 'pregnancy-core'),
				'param_name' => 'position',
				'admin_label' => true,
				'value' => array(
					esc_html__('Top','pregnancy-core') => 'top',
					esc_html__('Right','pregnancy-core') => 'right',
					esc_html__('Bottom','pregnancy-core') => 'bottom',
					esc_html__('Left','pregnancy-core') => 'left',
				),
				'std' => 'top',
				'dependency' => array( 'element' => 'type', 'value' =>array('','boxed') )
			),
			
			// Position
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Position', 'pregnancy-core'),
				'param_name' => 'p_position',
				'admin_label' => true,
				'value' => array(
					esc_html__('Top','pregnancy-core') => 'top',
					//esc_html__('Right','pregnancy-core') => 'right',
					esc_html__('Bottom','pregnancy-core') => 'bottom',
					//esc_html__('Left','pregnancy-core') => 'left',
				),
				'std' => 'top',
				'dependency' => array( 'element' => 'type', 'value' =>array('dt-sc-tooltip-one','dt-sc-tooltip-two','dt-sc-tooltip-three') )
			),

			// BG Color
			array(
				"type" => "colorpicker",
				"heading" => esc_html__( "Background Color", 'pregnancy-core' ),
				"param_name" => "bgcolor",
				"description" => esc_html__( "Select tooltip background color", 'pregnancy-core' ),
				'value' =>'#000000',
				'dependency' => array( 'element' => 'type', 'value' =>'boxed' )

      		),

			// Text Color
			array(
				"type" => "colorpicker",
				"heading" => esc_html__( "Text Color", 'pregnancy-core' ),
				"param_name" => "textcolor",
				"description" => esc_html__( "Select tooltip text color", 'pregnancy-core' ),
				"value" => '#ffffff',
				'dependency' => array( 'element' => 'type', 'value' =>'boxed' )
      		),

      		# URL
      		array(
      			'type' => 'vc_link',
      			'heading' => esc_html__( 'URL (Link)', 'pregnancy-core' ),
      			'param_name' => 'link',
      			'description' => esc_html__( 'Add tooltip link', 'pregnancy-core' ),
				'dependency' => array( 'element' => 'type', 'value' =>array('','boxed') )
      		),
			
			# Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Tooltip Image', 'pregnancy-core'),
				'param_name' => 'image',
				'dependency' => array( 'element' => 'type', 'value' => 'dt-sc-tooltip-three','dt-sc-tooltip-one')
			),
			
			# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-core" ),
      			"param_name" => "title",
      			"admin_label" => true,
				'dependency' => array( 'element' => 'type', 'value' => array('dt-sc-tooltip-one','dt-sc-tooltip-two','dt-sc-tooltip-three') )
      		),

      		# Content
      		array(
      			'type' => 'textarea',
      			'heading' => esc_html__( 'Content', 'pregnancy-core' ),
      			'param_name' => 'content',
      			'value' => 'Lorem ipsum dolor sit amet',
      		),
			
		)
	) );
}?>