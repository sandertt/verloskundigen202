<?php add_action( 'vc_before_init', 'dt_sc_twitter_tweets_vc_map' );
function dt_sc_twitter_tweets_vc_map() {

	vc_map( array( 
		"name" => esc_html__( "Twitter tweets", 'pregnancy-core' ),
		"base" => "dt_sc_twitter_tweets",
		"icon" => "dt_sc_twitter_tweets",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			# Consumer Key
			array(
				'type' => 'textfield',
				'param_name' => 'consumerkey',
				'heading' => esc_html__( 'Consumer key', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter Consumer key', 'pregnancy-core' ),
			),

			# Consumer secret
			array(
				'type' => 'textfield',
				'param_name' => 'consumersecret',
				'heading' => esc_html__( 'Consumer secret', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter Consumer secret', 'pregnancy-core' ),
			),

			# Access token 
			array(
				'type' => 'textfield',
				'param_name' => 'accesstoken',
				'heading' => esc_html__( 'Access token', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter Access token', 'pregnancy-core' ),
			),

			# Access token secret
			array(
				'type' => 'textfield',
				'param_name' => 'accesstokensecret',
				'heading' => esc_html__( 'Access token secret', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter Access token secret', 'pregnancy-core' ),
			),

			# Consumer Key
			array(
				'type' => 'textfield',
				'param_name' => 'username',
				'heading' => esc_html__( 'Twitter username', 'pregnancy-core' ),
				'description' => esc_html__( 'Enter Twitter username', 'pregnancy-core' ),
			),

			# Avatar
			array(
				'type' => 'dropdown',
				'param_name' => 'useravatar',
				'heading' => esc_html__('Show avatar?','pregnancy-core'),
				'value' => array( esc_html__('Yes','pregnancy-core') => 'yes' , esc_html__('No','pregnancy-core') => 'no' ),
				'std' => 'no'
			)
		)		
	) );
}?>