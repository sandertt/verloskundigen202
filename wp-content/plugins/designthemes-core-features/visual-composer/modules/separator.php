<?php add_action( 'vc_before_init', 'dt_sc_separator_vc_map' );
function dt_sc_separator_vc_map() {
	vc_map( array(
		"name" => esc_html__( "Separator", 'pregnancy-core' ),
		"base" => "dt_sc_separator",
		"icon" => "dt_sc_separator",
		"category" => DT_VC_CATEGORY,
		"params" => array(

			// Style
			array(
				'type' => 'dropdown',
				'param_name' => 'style',
				'value' => array(
					esc_html__( 'Horizontal', 'pregnancy-core' ) => 'horizontal',
					esc_html__( 'Vertical', 'pregnancy-core' ) => 'vertical',
				),
      			'admin_label' => true,
				'heading' => esc_html__( 'Style', 'pregnancy-core' ),
				'description' => esc_html__( 'Select separator display style', 'pregnancy-core' )
			),

			// Horizontal Separator
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Type', 'pregnancy-core'),
				'param_name' => 'horizontal_type',
				'value' => array( esc_html__('Small','pregnancy-core') => 'small',
					esc_html__('Single Line','pregnancy-core') => 'single-line',
					esc_html__('Single Line Dashed','pregnancy-core') => 'single-line-dashed',
					esc_html__('Double Border','pregnancy-core') => 'double-border',
					esc_html__('Diamond','pregnancy-core') => 'diamond',
					esc_html__('Single Line Dotted','pregnancy-core') => 'single-line-dotted'
				),
				'std' => 'small',
				'dependency' => array( 'element' => 'style', 'value' => 'horizontal' )				
			),

			// Vertical Separator
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Type', 'pregnancy-core'),
				'param_name' => 'vertical_type',
				'value' => array( esc_html__('Normal','pregnancy-core') => '', esc_html__('Small','pregnancy-core') => 'small' ),
				'std' => 'small',
				'dependency' => array( 'element' => 'style', 'value' => 'vertical' )				
			),
			
			# Class
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-core' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular element differently - add a class name and refer to it in custom CSS','pregnancy-core')
      		)
		)
	) );	
}?>