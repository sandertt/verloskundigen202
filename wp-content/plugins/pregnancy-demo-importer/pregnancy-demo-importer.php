<?php
/*
 * Plugin Name:	Pregnancy Demo Importer
 * URI: 	http://wedesignthemes.com/plugins/pregnancy-demo-importer
 * Description: A simple wordpress plugin designed to import demo contents for <strong>PREGNANCY</strong> theme 
 * Version: 	1.0
 * Author: 		DesignThemes 
 * Text Domain: pregnancy-importer
 * Author URI:	http://themeforest.net/user/designthemes
 */
if (! class_exists ( 'DTPregnancyImporter' )) {
	class DTPregnancyImporter {

		function __construct() {

			add_action('wp_ajax_pregnancy_ajax_importer',  array( $this, 'pregnancy_ajax_importer' ) );
		}

		function pregnancy_ajax_importer() {
			require_once plugin_dir_path ( __FILE__ ) . 'importer/import.php';
			die();
		}
	}

	new DTPregnancyImporter();
}