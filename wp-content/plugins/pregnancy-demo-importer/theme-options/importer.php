<!-- #import -->
<div id="import" class="bpanel-content">
    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">
        <ul class="sub-panel">
            <li><a href="#tab1"><?php esc_html_e('Import Demo', 'pregnancy-importer');?></a></li>
        </ul>

        <?php $content = array(
                '--' => esc_html('All', 'pregnancy-importer'),
                'pages'  => esc_html('Pages', 'pregnancy-importer'),
                'posts' => esc_html('Posts', 'pregnancy-importer'),
                'portfolios' => esc_html('Portfolio', 'pregnancy-importer'),
                'contactforms' => esc_html('Contact Forms', 'pregnancy-importer'),
				'procedure' => esc_html('Procedure', 'pregnancy-importer'),
				'doctors' => esc_html('Doctor', 'pregnancy-importer')
            );

            function pregnancy_sort( $array ) {
                asort( $array );
                return $array;
            }

            $pregnancy_demos = array(
                
                'pregnancy' => array(
                    'label' =>  'Pregnancy',
                    'link' => 'http://wedesignthemes.com/themes/dt-pregnancy/',
                    'content' => $content
                )
                
            );

		$pregnancy_demos = pregnancy_sort($pregnancy_demos); ?>

        <!-- #tab1 -->
        <div id="tab1" class="tab-content">
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Import Demo', 'pregnancy-importer');?></h3>
                </div>
                <div class="box-content dttheme-import">

                    <!-- 1. Choose Demo -->
                    <p class="note">
                        <?php esc_html_e('Before starting the import, you need to install all plugins that you want to use.', 'pregnancy-importer'); ?>
                        <br /><?php esc_html_e('If you are planning to use the shop, please install WooCommerce plugin.', 'pregnancy-importer');?></p>
                    <div class="hr_invisible"> </div>
                    <div class="column one-third"><label><?php esc_html_e('Demo', 'pregnancy-importer');?></label></div>
                    <div class="column two-third last">
                        <select name="demo" class="demo medium dt-chosen-select">
                            <option data-link="http://wedesignthemes.com/dt-pregnancy/" value="">-- <?php esc_html_e('Select', 'pregnancy-importer');?> --</option>
                            <?php foreach( $pregnancy_demos as $key => $pregnancy_demo ) : ?>
                                    <option data-link="<?php echo esc_attr( $pregnancy_demo['link'] ); ?>" value="<?php echo esc_attr($key); ?>"><?php echo esc_html( $pregnancy_demo['label'] ); ?></option>
                            <?php endforeach; ?>
                        </select>
                                               
                    </div> 
                    <div class="hr_invisible"> </div>

                    <!-- 2. Choose Content -->
                    <?php foreach($pregnancy_demos as $pregnancy_demo_key => $pregnancy_demo) :?>
                            <div class="pregnancy-demos <?php echo esc_attr($pregnancy_demo_key); ?>-demo hide">
                                <div class="column one-third"><label><?php esc_html_e('Import', 'pregnancy-importer');?></label></div>
                                <div class="column two-third last">
                                    <select name="import" class="import medium dt-chosen-select">
                                        <option value="">-- <?php esc_html_e('Select', 'pregnancy-importer');?> --</option>
                                        <option value="all"><?php esc_html_e('All', 'pregnancy-importer') ?></option>
                                        <option value="content"><?php esc_html_e('Content', 'pregnancy-importer') ?></option>
                                        <option value="menu"><?php esc_html_e('Menu', 'pregnancy-importer') ?></option>
                                        <option value="options"><?php esc_html_e('Options', 'pregnancy-importer') ?></option>
                                        <option value="widgets"><?php esc_html_e('Widgets', 'pregnancy-importer') ?></option>
                                    </select>
                                </div>

                                <div class="hr_invisible"> </div>

                                <!-- 2.1. Content Type  -->
                                <div class="row-content hide">
                                    <div class="column one-third">
                                        <label for="content"><?php esc_html_e('Content', 'pregnancy-importer');?></label>
                                    </div>
                                    <div class="column two-third last">
                                        <select name="content" class="medium dt-chosen-select">
                                            <?php foreach( $pregnancy_demo['content'] as $key => $value ): ?>
                                                    <option value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html($value); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    <?php endforeach;?>

                    <!-- 3. Attachment -->
                    <div class="row-attachments hide">
                        <div class="column one-third"><?php esc_html_e('Attachments', 'pregnancy-importer');?></div>
                        <div class="column two-third last">
                            <fieldset>
                                <label for="attachments"><input type="checkbox" value="0" id="attachments" name="attachments"><?php esc_html_e('Import attachments', 'pregnancy-importer');?></label>
                                <p class="description"><?php esc_html_e('Download all attachments from the demo may take a while. Please be patient.', 'pregnancy-importer');?></p>
                            </fieldset>
                        </div>
                        <div class="hr_invisible"> </div>
                    </div>

                    <!-- Import Button -->
                    <div class="column one-column">
                        <div class="hr_invisible"> </div>
                        <div class="column one-third">&nbsp;</div>
                        <div class="column two-third last">
                            <a href="#" class="dttheme-import-button bpanel-button black-btn" title="<?php esc_html_e('Import demo data', 'pregnancy');?>"><?php esc_html_e('Import Demo Data', 'pregnancy');?></a>
                        </div>
                    </div>
                    <div class="hr"></div>
                </div>            
            </div>
        </div>
    </div>
</div><!-- #import end-->