<?php
add_action( 'vc_before_init', 'dt_sc_procedure_list_vc_map' );
function dt_sc_procedure_list_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;

	vc_map( array(
		"name" => esc_html__("Procedure List", "pregnancy-addon"),
		"base" => "dt_sc_procedure_list",
		"icon" => "dt_sc_procedure_list",
		"category" => $plural_name,
		"params" => array(

           		# ID
            	array(
					"type" => "textfield",
					"heading" => esc_html__( "Enter Procedure ID", "pregnancy-addon" ),
					"param_name" => "procedure_id",
					"value" => '',
					"description" => esc_html__( 'Enter ID of procedure to display.', 'pregnancy-addon' ),
            	),
				
				# Button Text
            	array(
					"type" => "textfield",
					"heading" => esc_html__( "Button Text", "pregnancy-addon" ),
					"param_name" => "button_text",
					"value" => esc_html__('View Procedure', 'pregnancy-addon'),
					"description" => esc_html__( 'Enter button text.', 'pregnancy-addon' ),
            	)      		
	     )		
	) );
}?>