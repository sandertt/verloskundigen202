<?php
add_action( 'vc_before_init', 'dt_sc_trimester_vc_map' );
function dt_sc_trimester_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	vc_map( array(
		"name" => esc_html__("Trimester time line", "pregnancy-addon"),
		"base" => "dt_sc_trimester",
		"icon" => "dt_sc_trimester",
		"category" => $plural_name,
		'description' => esc_html__("Add pregnancy trimester time line", "pregnancy-addon"),
		"params" => array(

     		//STAGE ONE
			# Stage 1 Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Stage1 Image', 'pregnancy-addon'),
				'param_name' => 'stage_1_image',
			),
			
			# Stage1 - 1st week count
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage1- 1st week count", "pregnancy-addon" ),
      			"param_name" => "stage_1_week_count_1",
      			"admin_label" => true,
				"value" => '4'
      		),
			
			# Stage 1 week count I Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage1 - 1st week count description','pregnancy-addon'),
				'param_name' => 'stage_1_week_count_1_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 1 week count II
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage1 - 2nd week count", "pregnancy-addon" ),
      			"param_name" => "stage_1_week_count_2",
      			"admin_label" => true,
				"value" => '9'
      		),
			
			# Stage 1 week count II Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage1 - 2nd week count description','pregnancy-addon'),
				'param_name' => 'stage_1_week_count_2_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 1 week count III
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage1 - 3rd week count", "pregnancy-addon" ),
      			"param_name" => "stage_1_week_count_3",
      			"admin_label" => true,
				"value" => '13'
      		),
			
			# Stage 1 week count III Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage1 - 3rd week count description','pregnancy-addon'),
				'param_name' => 'stage_1_week_count_3_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 1 title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage 1 Title", "pregnancy-addon" ),
      			"param_name" => "stage_1_title",
      			"admin_label" => true
      		),
			
			# Stage 1 weight
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage 1 weight", "pregnancy-addon" ),
      			"param_name" => "stage_1_weight",
      			"admin_label" => true
      		),
			
			//STAGE TWO
			# Stage 2 Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Stage2 Image', 'pregnancy-addon'),
				'param_name' => 'stage_2_image',
			),
			
			# Stage2 - 1st week count
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage2- 1st week count", "pregnancy-addon" ),
      			"param_name" => "stage_2_week_count_1",
      			"admin_label" => true,
				"value" => '18'
      		),
			
			# Stage 2 week count I Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage2 - 1st week count description','pregnancy-addon'),
				'param_name' => 'stage_2_week_count_1_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 2 week count II
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage2 - 2nd week count", "pregnancy-addon" ),
      			"param_name" => "stage_2_week_count_2",
      			"admin_label" => true,
				"value" => '22'
      		),
			
			# Stage 2 week count II Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage2 - 2nd week count description','pregnancy-addon'),
				'param_name' => 'stage_2_week_count_2_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 2 week count III
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage2 - 3rd week count", "pregnancy-addon" ),
      			"param_name" => "stage_2_week_count_3",
      			"admin_label" => true,
				"value" => '27'
      		),
			
			# Stage 2 week count III Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage2 - 3rd week count description','pregnancy-addon'),
				'param_name' => 'stage_2_week_count_3_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 2title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage 2 Title", "pregnancy-addon" ),
      			"param_name" => "stage_2_title",
      			"admin_label" => true
      		),
			
			# Stage 2 weight
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage 2 weight", "pregnancy-addon" ),
      			"param_name" => "stage_2_weight",
      			"admin_label" => true
      		),
			
			//STAGE THREE
			# Stage 3 Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Stage3 Image', 'pregnancy-addon'),
				'param_name' => 'stage_3_image',
			),
			
			# Stage3 - 1st week count
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage3- 1st week count", "pregnancy-addon" ),
      			"param_name" => "stage_3_week_count_1",
      			"admin_label" => true,
				"value" => '31'
      		),
			
			# Stage 3 week count I Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage3 - 1st week count description','pregnancy-addon'),
				'param_name' => 'stage_3_week_count_1_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 3 week count II
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage3 - 2nd week count", "pregnancy-addon" ),
      			"param_name" => "stage_3_week_count_2",
      			"admin_label" => true,
				"value" => '36'
      		),
			
			# Stage 3 week count II Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage3 - 2nd week count description','pregnancy-addon'),
				'param_name' => 'stage_3_week_count_2_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 3 week count III
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage3 - 3rd week count", "pregnancy-addon" ),
      			"param_name" => "stage_3_week_count_3",
      			"admin_label" => true,
				"value" => '40'
      		),
			
			# Stage 3 week count III Description
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Stage3 - 3rd week count description','pregnancy-addon'),
				'param_name' => 'stage_3_week_count_3_content',
				'value' => base64_encode('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			),
			
			# Stage 3 title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage 3 Title", "pregnancy-addon" ),
      			"param_name" => "stage_3_title",
      			"admin_label" => true
      		),
			
			# Stage 3 weight
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Stage 3 weight", "pregnancy-addon" ),
      			"param_name" => "stage_3_weight",
      			"admin_label" => true
      		),
			

		)
	) );
}?>