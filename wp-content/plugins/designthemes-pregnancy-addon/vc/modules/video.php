<?php
add_action( 'vc_before_init', 'dt_sc_video_section_vc_map' );
function dt_sc_video_section_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	vc_map( array(
		"name" => esc_html__("Video Section", "pregnancy-addon"),
		"base" => "dt_sc_video_section",
		"icon" => "dt_sc_video_section",
		"category" => $plural_name,
		'description' => esc_html__("Add Video Section", "pregnancy-addon"),
		"params" => array(

			
			#video type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Select Video Type', 'pregnancy-addon'),
				'param_name' => 'video_type',
				'value' => array( esc_html__('Type 1','pregnancy-addon') => 'type1',
					esc_html__('Type 2','pregnancy-addon') => 'type2',
				),
				'std' => 'type1',
			),
			
			# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-addon" ),
      			"param_name" => "title",
      			"admin_label" => true,
				'dependency' => array( 'element' => 'video_type', 'value' => 'type1'),
      		),
			
			
      		# Video url
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Video URL', 'pregnancy-addon'),
				'param_name' => 'video_url',
				'description' => esc_html__( 'Enter link to video ( Eg: http://player.vimeo.com/video/18439821 )', 'pregnancy-addon' ),
			),
			
			# Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image','pregnancy-addon'),
				'param_name' => 'image'
            )
		)
	) );
}?>