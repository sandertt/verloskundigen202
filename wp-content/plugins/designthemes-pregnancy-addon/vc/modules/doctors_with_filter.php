<?php
add_action( 'vc_before_init', 'dt_sc_doctors_with_filter_vc_map' );
function dt_sc_doctors_with_filter_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;

	vc_map( array(
		"name" => esc_html__("Filterable Doctors", "pregnancy-addon"),
		"base" => "dt_sc_doctors_with_filter",
		"icon" => "dt_sc_doctors_with_filter",
		"category" => $plural_name,
		'description' => esc_html__("To Show doctors with filterable option", "pregnancy-addon"),
		"params" => array(
			array( 
				'type' => 'dropdown',
				'param_name' => 'column',
				'value' => array(
					esc_html__( 'Two Column', 'pregnancy-addon' ) => '2',
					esc_html__( 'Three Columns', 'pregnancy-addon' ) => '3',
					esc_html__( 'Four Columns', 'pregnancy-addon') => '4'
				),
      			'admin_label' => true,
      			'std' => 4,
				'heading' => esc_html__( 'Layout', 'pregnancy-addon' ),
				'description' => esc_html__( 'Select filterable doctor display layout.', 'pregnancy-addon' )				
			),
			
			array( 
				'type' => 'dropdown',
				'param_name' => 'show_doc_category_filter',
				'value' => array(
					esc_html__( 'Yes', 'pregnancy-addon' ) => 'yes',
					esc_html__( 'No', 'pregnancy-addon' ) => 'no',
				),
      			'admin_label' => true,
      			'std' => 'no',
				'heading' => esc_html__( 'Display Category Filter', 'pregnancy-addon' ),
				'description' => esc_html__( 'Select yes/no to display doctor category filter.', 'pregnancy-addon' )				
			)			
		)
	) );
}?>