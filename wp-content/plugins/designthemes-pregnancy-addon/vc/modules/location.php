<?php
add_action( 'vc_before_init', 'dt_sc_location_vc_map' );
function dt_sc_location_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	vc_map( array(
		"name" => esc_html__("Location", "pregnancy-addon"),
		"base" => "dt_sc_location",
		"icon" => "dt_sc_location",
		"category" => $plural_name,
		'description' => esc_html__("Add location", "pregnancy-addon"),
		"params" => array(

     		# Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Add image', 'pregnancy-addon'),
				'param_name' => 'image'
			),
			
			# Font Awesome
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Font Awesome', 'pregnancy-addon' ),
				'param_name' => 'iconclass',
				'settings' => array( 'emptyIcon' => false, 'iconsPerPage' => 4000 ),
				'description' => esc_html__( 'Select icon from library', 'pregnancy-addon' ),
			),
			
			# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-addon" ),
      			"param_name" => "title",
      			"admin_label" => true
      		),

			# Address
			array(
				'type' => 'textarea_html',
				'heading' => esc_html__('Address','pregnancy-addon'),
				'param_name' => 'content',
				'value' => '<p> 3524 Deerfield Drive <br> Valdosta, GA 31601 <br> (973) 486-4862 </p>'
			),
			

		)
	) );
}?>