<?php
add_action( 'vc_before_init', 'dt_sc_featured_product_carousel_vc_map' );
function dt_sc_featured_product_carousel_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	vc_map( array(
		"name" => esc_html__("Featured Products Carousel", "pregnancy-addon"),
		"base" => "dt_sc_featured_product_carousel",
		"icon" => "dt_sc_featured_product_carousel",
		"category" => $plural_name,
		'description' => esc_html__("Add featured product carousel", "pregnancy-addon"),
		"params" => array(

			#featured product type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Select Type', 'pregnancy-addon'),
				'param_name' => 'featured_prd_type',
				'value' => array( esc_html__('With description','pregnancy-addon') => 'type1',
					esc_html__('Without description','pregnancy-addon') => 'type2',
				),
				'std' => 'type1',
			),
			
			# title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-addon" ),
      			"param_name" => "title",
      			"admin_label" => true,
				'dependency' => array( 'element' => 'featured_prd_type', 'value' => 'type1')
      		),
			
			# Description
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Description','pregnancy-addon'),
				'param_name' => 'content',
				'value' => '<p>Duas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>',
				'dependency' => array( 'element' => 'featured_prd_type', 'value' => 'type1')
			),
			
			#featured product columns
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Select Column Type', 'pregnancy-addon'),
				'param_name' => 'column',
				'value' => array( esc_html__('III Columns','pregnancy-addon') => 'featured-prd-3-cols',
					esc_html__('IV Columns','pregnancy-addon') => 'featured-prd-4-cols',
				),
				'std' => 'featured-prd-3-cols',
			),
			
			#Scroll
			array(
				'type' => 'textfield',
				'param_name' => 'scroll',
				'heading' => esc_html__( 'Scroll', 'pregnancy-core' ),
				'value' => '3',				
				'description' => esc_html__( 'The number of items to scroll at once', 'pregnancy-core' )
			),

			#Visible
			array(
				'type' => 'textfield',
				'param_name' => 'visible',
				'heading' => esc_html__( 'Visible', 'pregnancy-core' ),
				'value' => '3',
 				'description' => esc_html__( 'The number of items to show at once', 'pregnancy-core' )
			),

		)
	) );
}?>