<?php
add_action( 'vc_before_init', 'dt_sc_title_desc_vc_map' );
function dt_sc_title_desc_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	vc_map( array(
		"name" => esc_html__("Title", "pregnancy-addon"),
		"base" => "dt_sc_title_desc",
		"icon" => "dt_sc_title_desc",
		"category" => $plural_name,
		'description' => esc_html__("Add title with description", "pregnancy-addon"),
		"params" => array(

     		#title type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Select Title Type', 'pregnancy-addon'),
				'param_name' => 'title_type',
				'value' => array( esc_html__('Title','pregnancy-addon') => 'title',
					esc_html__('Title With Description','pregnancy-addon') => 'title-with-desc',
				),
				'std' => 'title-with-desc',
			),
			
			# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-addon" ),
      			"param_name" => "title",
      			"admin_label" => true
      		),
			
			# separator alignment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Title align', 'pregnancy-addon'),
				'param_name' => 'title_align',
				'value' => array( esc_html__('Center','pregnancy-addon') => 'title-center',
					esc_html__('Left','pregnancy-addon') => 'title-left',
					esc_html__('Right','pregnancy-addon') => 'title-right'
				),
				'dependency' => array( 'element' => 'title_type', 'value' => 'title'),
				'std' => 'border',
			),
			
			# separator alignment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Separator align', 'pregnancy-addon'),
				'param_name' => 'separator_align',
				'value' => array( esc_html__('Top','pregnancy-addon') => 'sep-top',
					esc_html__('Bottom','pregnancy-addon') => 'sep-bottom'
					
				),
				'dependency' => array( 'element' => 'title_type', 'value' => 'title'),
				'std' => 'border',
			),

      		# Image url
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Separator Image', 'pregnancy-addon'),
				'param_name' => 'image',
				'dependency' => array( 'element' => 'title_type', 'value' => 'title-with-desc')
			),
			
			# Separator color
      		array(
      			'type' => 'colorpicker',
      			'heading' => esc_html__( 'Separator color', 'pregnancy-addon' ),
      			'param_name' => 'separator_image_color',
      			'description' => esc_html__( 'Select separator image color', 'pregnancy-addon' ),
				'dependency' => array( 'element' => 'title_type', 'value' => 'title-with-desc')
      		),
			
			# Separator style
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Separator display style', 'pregnancy-addon'),
				'param_name' => 'separator_display_style',
				'value' => array( esc_html__('Border','pregnancy-addon') => 'border',
					esc_html__('Dashed','pregnancy-addon') => 'dashed',
					esc_html__('Dotted','pregnancy-addon') => 'dotted',
					esc_html__('Double','pregnancy-addon') => 'double',
					esc_html__('Shadow','pregnancy-addon') => 'shadow',
				),
				'dependency' => array( 'element' => 'title_type', 'value' => 'title-with-desc'),
				'std' => 'border',
			),
			
			
			# Title Description
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Title Description','pregnancy-addon'),
				'param_name' => 'content',
				'value' => '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>',
				'dependency' => array( 'element' => 'title_type', 'value' => 'title-with-desc')
			),
			
			# Extra Class name
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-addon' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular element differently - add a class name and refer to it in custom CSS','pregnancy-addon')
      		)

		)
	) );
}?>