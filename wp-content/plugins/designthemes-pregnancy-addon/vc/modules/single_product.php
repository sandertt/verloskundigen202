<?php
add_action( 'vc_before_init', 'dt_sc_single_product_vc_map' );
function dt_sc_single_product_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	vc_map( array(
		"name" => esc_html__("Single Product", "pregnancy-addon"),
		"base" => "dt_sc_single_product",
		"icon" => "dt_sc_single_product",
		"category" => $plural_name,
		'description' => esc_html__("Add single product", "pregnancy-addon"),
		"params" => array(

			# Product ID
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Product ID", "pregnancy-addon" ),
      			"param_name" => "product_id",
      			"admin_label" => true
      		),
			
			# Excerpt Length
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Excerpt Length", "pregnancy-addon" ),
      			"param_name" => "excerpt_length",
      			"admin_label" => true
      		),

		)
	) );
}?>