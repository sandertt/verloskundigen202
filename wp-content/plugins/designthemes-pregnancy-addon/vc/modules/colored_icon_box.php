<?php
add_action( 'vc_before_init', 'dt_sc_colored_icon_box_vc_map' );
function dt_sc_colored_icon_box_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	global $colored_icon_box_variations;
	
	vc_map( array(
		"name" => esc_html__("Colored Icon Box", "pregnancy-addon"),
		"base" => "dt_sc_colored_icon_box",
		"icon" => "dt_sc_colored_icon_box",
		"category" => $plural_name,
		'description' => esc_html__("Add colored icon box", "pregnancy-addon"),
		"params" => array(

     		# Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Colored Icon box Types', 'pregnancy-addon'),
				'param_name' => 'colored_icnbx_type',
				'value' => array( esc_html__('Type 1','pregnancy-addon') => 'type1',
					esc_html__('Type 2','pregnancy-addon') => 'type2'
				),
				'std' => 'type1',
			),
			
			# Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Title", "pregnancy-addon" ),
      			"param_name" => "title",
      			"admin_label" => true
      		),

      		# Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image', 'pregnancy-addon'),
				'param_name' => 'image'
			),
			
			# Hover icon Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Hover Icon Image', 'pregnancy-addon'),
				'param_name' => 'hover_icn_image',
				'dependency' => array( 'element' => 'colored_icnbx_type', 'value' => 'type1')
			),
			
			# URL
      		array(
      			'type' => 'vc_link',
      			'heading' => esc_html__( 'URL (Link)', 'pregnancy-addon' ),
      			'param_name' => 'link',
      			'description' => esc_html__( 'Add link to icon box', 'pregnancy-addon' ),
				'dependency' => array( 'element' => 'colored_icnbx_type', 'value' => 'type1')
      		),
			
			# Variation
            array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Color', 'pregnancy-addon' ),
				'admin_label' => true,
				'param_name' => 'variation',
				'value' => $colored_icon_box_variations,
				'description' => esc_html__( 'Select Text color', 'pregnancy-addon' ),
            ),
			
			# Extra Class name
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'pregnancy-addon' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular element differently - add a class name and refer to it in custom CSS','pregnancy-addon')
      		),
			
			# Content
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Content','pregnancy-addon'),
				'param_name' => 'content',
				'value' => 'Et harum quidem rerum facilis est et expedita distinctio. Non provident, similique sunt in culpa qui animi, id est laborum et dolorum fuga.',
				'dependency' => array( 'element' => 'colored_icnbx_type', 'value' => 'type2')
			),

		)
	) );
}?>