<?php
add_action( 'vc_before_init', 'dt_sc_cycle_section_vc_map' );
function dt_sc_cycle_section_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	global $cycle_section_variations;
	
	vc_map( array(
		"name" => esc_html__("Cycle Section", "pregnancy-addon"),
		"base" => "dt_sc_cycle_section",
		"icon" => "dt_sc_cycle_section",
		"category" => $plural_name,
		'description' => esc_html__("Add cycle section", "pregnancy-addon"),
		"params" => array(

     		# Center placed Title
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Center Placed Text", "pregnancy-addon" ),
      			"param_name" => "center_title",
      			"admin_label" => true
      		),
			
			# Center placed Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Center placed image', 'pregnancy-addon'),
				'param_name' => 'center_placed_image'
			),
			
			# Center placed Image color
      		array(
      			'type' => 'colorpicker',
      			'heading' => esc_html__( 'Center placed Imagee background color', 'pregnancy-addon' ),
      			'param_name' => 'center_placed_img_color',
      			'description' => esc_html__( 'Select Center placed Imagee background color', 'pregnancy-addon' ),
      		),

      		# Icon 1 Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Icon 1-Image', 'pregnancy-addon'),
				'param_name' => 'icon_image_1'
			),
			
			# Icon 1 image content
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Icon 1-Image content','pregnancy-addon'),
				'param_name' => 'icon_image_1_content',
				'value' => 'Lorem ipsum dolor sit amet'
			),
			
			# Icon 1 Variation
            array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon 1-Image variation', 'pregnancy-addon' ),
				'admin_label' => true,
				'param_name' => 'icon_image_1_variation',
				'value' => $cycle_section_variations,
				'description' => esc_html__( 'Select Icon 1-Image variation', 'pregnancy-addon' ),
            ),
			
			# Icon 1 lightbox Content
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Icon 1-Image Lightbox Content','pregnancy-addon'),
				'param_name' => 'icon1_lightbox_content',
				'value' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			),
			
			
			# Icon 2 Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Icon 2-Image', 'pregnancy-addon'),
				'param_name' => 'icon_image_2'
			),
			
			# Icon 2 image content
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Icon 2-Image content','pregnancy-addon'),
				'param_name' => 'icon_image_2_content',
				'value' => 'Lorem ipsum dolor sit amet'
			),
			
			# Icon 2 Variation
            array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon 2-Image variation', 'pregnancy-addon' ),
				'admin_label' => true,
				'param_name' => 'icon_image_2_variation',
				'value' => $cycle_section_variations,
				'description' => esc_html__( 'Select Icon 2-Image variation', 'pregnancy-addon' ),
            ),
			
			# Icon 2 lightbox Content
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Icon 2-Image Lightbox Content','pregnancy-addon'),
				'param_name' => 'icon2_lightbox_content',
				'value' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			),
			
			
			# Icon 3 Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Icon 3-Image', 'pregnancy-addon'),
				'param_name' => 'icon_image_3'
			),
			
			# Icon 3 image content
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Icon 3-Image content','pregnancy-addon'),
				'param_name' => 'icon_image_3_content',
				'value' => 'Lorem ipsum dolor sit amet'
			),
			
			# Icon 3 Variation
            array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon 3-Image variation', 'pregnancy-addon' ),
				'admin_label' => true,
				'param_name' => 'icon_image_3_variation',
				'value' => $cycle_section_variations,
				'description' => esc_html__( 'Select Icon 3-Image variation', 'pregnancy-addon' ),
            ),
			# Icon 3 lightbox Content
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Icon 3-Image Lightbox Content','pregnancy-addon'),
				'param_name' => 'icon3_lightbox_content',
				'value' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			),
			
			
			# Icon 4 Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Icon 4-Image', 'pregnancy-addon'),
				'param_name' => 'icon_image_4'
			),
			
			# Icon 4 image content
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Icon 4-Image content','pregnancy-addon'),
				'param_name' => 'icon_image_4_content',
				'value' => 'Lorem ipsum dolor sit amet'
			),
			
			# Icon 4 Variation
            array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon 4-Image variation', 'pregnancy-addon' ),
				'admin_label' => true,
				'param_name' => 'icon_image_4_variation',
				'value' => $cycle_section_variations,
				'description' => esc_html__( 'Select Icon 4-Image variation', 'pregnancy-addon' ),
            ),
			# Icon 4 lightbox Content
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Icon 4-Image Lightbox Content','pregnancy-addon'),
				'param_name' => 'icon4_lightbox_content',
				'value' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			),
			
			
			# Icon 5 Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Icon 5-Image', 'pregnancy-addon'),
				'param_name' => 'icon_image_5'
			),
			
			# Icon 5 image content
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Icon 5-Image content','pregnancy-addon'),
				'param_name' => 'icon_image_5_content',
				'value' => 'Lorem ipsum dolor sit amet'
			),
			
			# Icon 5 Variation
            array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon 5-Image variation', 'pregnancy-addon' ),
				'admin_label' => true,
				'param_name' => 'icon_image_5_variation',
				'value' => $cycle_section_variations,
				'description' => esc_html__( 'Select Icon 5-Image variation', 'pregnancy-addon' ),
            ),
			# Icon 5 lightbox Content
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Icon 5-Image Lightbox Content','pregnancy-addon'),
				'param_name' => 'icon5_lightbox_content',
				'value' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			),
			
			
			# Icon 6 Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Icon 6-Image', 'pregnancy-addon'),
				'param_name' => 'icon_image_6'
			),
			
			# Icon 6 image content
			array(
				'type' => 'textarea',
				'heading' => esc_html__('Icon 6-Image content','pregnancy-addon'),
				'param_name' => 'icon_image_6_content',
				'value' => 'Lorem ipsum dolor sit amet'
			),
			
			# Icon 6 Variation
            array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon 6-Image variation', 'pregnancy-addon' ),
				'admin_label' => true,
				'param_name' => 'icon_image_6_variation',
				'value' => $cycle_section_variations,
				'description' => esc_html__( 'Select Icon 6-Image variation', 'pregnancy-addon' ),
            ),
			# Icon 6 lightbox Content
			array(
				'type' => 'textarea_raw_html',
				'heading' => esc_html__('Icon 6-Image Lightbox Content','pregnancy-addon'),
				'param_name' => 'icon6_lightbox_content',
				'value' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			),

		)
	) );
}?>