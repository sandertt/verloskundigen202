<?php
add_action( 'vc_before_init', 'dt_sc_appointment_vc_map' );
function dt_sc_appointment_vc_map() {

	$plural_name    = esc_html__('Pregnancy  Addon', 'pregnancy-addon');
	if( function_exists( 'pregnancy_opts_get' ) ) :
		$plural_name	=	pregnancy_opts_get( 'plural-pregnancy-name', $plural_name );
	endif;
	
	vc_map( array(
		"name" => esc_html__("Appointment", "pregnancy-addon"),
		"base" => "dt_sc_reserve_appointment",
		"icon" => "dt_sc_reserve_appointment",
		"category" => $plural_name,
		'description' => esc_html__("Appointment Form", "pregnancy-addon"),
		"params" => array(

			
			# Service IDs
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Service IDs", "pregnancy-addon" ),
      			"param_name" => "serviceids",
      			"admin_label" => true
      		),
			
			# Staff IDs
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Staff IDs", "pregnancy-addon" ),
      			"param_name" => "staffids",
      			"admin_label" => true
      		),
			

		)
	) );
}?>