<?php if( !class_exists('DTVCPregnancyModule') ) {
	class DTVCPregnancyModule {

		function __construct() {

			add_action ( 'after_setup_theme', array ( $this, 'dt_map_pregnancy_shortcodes' ) , 1000 );

			add_action( 'admin_enqueue_scripts', array( $this, 'dt_pregnancy_vc_admin_scripts')  );
		}

		function dt_map_pregnancy_shortcodes() {

			$path = plugin_dir_path ( __FILE__ ).'modules/';
			$modules = array(

				'dt_sc_bmi_calc' => $path.'bmi_calc.php',
				'dt_sc_title_desc' => $path.'title_desc.php',
				'dt_sc_colored_icon_box' => $path.'colored_icon_box.php',
				'dt_sc_cycle_section' => $path.'cycle_section.php',
				'dt_sc_location' => $path.'location.php',
				'dt_sc_appointment' => $path.'appointment.php',
				'dt_sc_video_section' => $path.'video.php',
				'dt_sc_procedure_list' => $path.'procedure_list.php',
				'dt_sc_current_doctor_info' => $path.'current_doctor_info.php',
				'dt_sc_doctors_with_filter' => $path.'doctors_with_filter.php',
				'dt_sc_trimester' => $path.'trimester.php',
				'dt_sc_single_product' => $path.'single_product.php',
				'dt_sc_featured_product_carousel' => $path.'featured_product_carousel.php',
			);

			// Apply filters so you can easily modify the modules 100%
			$modules = apply_filters( 'vcex_builder_modules', $modules );

			if( !empty( $modules ) ){
				foreach ( $modules as $key => $val ) {
					require_once( $val );
				}
			}
		}

		function dt_pregnancy_vc_admin_scripts( $hook ) {

			if($hook == "post.php" || $hook == "post-new.php") {
				wp_enqueue_style( 'dt-pregnancy-vc-admin', plugins_url ('designthemes-pregnancy-addon') . '/vc/style.css', array (), false, 'all' );
			}			
		}

	}
}