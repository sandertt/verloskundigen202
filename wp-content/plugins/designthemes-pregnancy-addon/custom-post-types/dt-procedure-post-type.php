<?php
if (! class_exists ( 'DTProcedurePostType' )) {
	class DTProcedurePostType {

		/**
		 * A function constructor calls initially
		 */
		function __construct() {

			// Add Hook into the 'init()' action
			add_action ( 'init', array (
				$this,
				'dt_init'
			) );

			// Add Hook into the 'admin_init()' action
			add_action ( 'admin_init', array (
				$this,
				'dt_admin_init'
			) );

			// Add Hook into the 'template_include' filter
			add_filter ( 'template_include', array (
				$this,
				'dt_template_include'
			) );
		}

		/**
		 * A function hook that the WordPress core launches at 'init' points
		 */
		function dt_init() {
			$this->createPostType ();
			add_action ( 'save_post', array (
				$this,
				'save_post_meta' 
			) );
		}

		/**
		 * A function hook that the WordPress core launches at 'admin_init' points
		 */
		function dt_admin_init() {
			
			add_action ( 'add_meta_boxes', array (
				$this,
				'dt_add_procedure_meta_box' 
			) );
			
			add_filter ( "manage_edit-dt_procedure_columns", array (
				$this,
				"dt_procedure_edit_columns" 
			) );
			
			add_action ( "manage_posts_custom_column", array (
				$this,
				"dt_procedure_columns_display" 
			), 10, 2 );
		}

		/**
		 */
		function createPostType() {
			$postslug	 	= 'dt_procedure'; 					
			$singular_name  = __('Procedure', 'pregnancy-addon'); 	$plural_name  = __('Procedure', 'pregnancy-addon');

			if( function_exists( 'pregnancy_opts_get' ) ) :
				$postslug 		=	pregnancy_opts_get( 'single-procedure-slug', 'dt_procedure' );
				$singular_name  =	pregnancy_opts_get( 'singular-procedure-name', __('Procedure', 'pregnancy-addon') );
				$plural_name	=	pregnancy_opts_get( 'plural-procedure-name', __('Procedure', 'pregnancy-addon') );
			endif;
			
			$labels = array (
					'name'				=> 	$plural_name,
					'all_items' 		=> 	__ ( 'All ', 'pregnancy-addon' ) . $plural_name,
					'singular_name' 	=> 	$singular_name,
					'add_new' 			=> 	__ ( 'Add New', 'pregnancy-addon' ),
					'add_new_item' 		=> 	__ ( 'Add New ', 'pregnancy-addon' ) . $singular_name,
					'edit_item' 		=> 	__ ( 'Edit ', 'pregnancy-addon' ) . $singular_name,
					'new_item' 			=> 	__ ( 'New ', 'pregnancy-addon' ) . $singular_name,
					'view_item' 		=> 	__ ( 'View ', 'pregnancy-addon' ) . $singular_name,
					'search_items' 		=>	__ ( 'Search ', 'pregnancy-addon' ) . $plural_name,
					'not_found' 		=> 	__ ( 'No ', 'pregnancy-addon' ) . $plural_name . __ ( ' found', 'pregnancy-addon' ),
					'not_found_in_trash' => __ ( 'No ', 'pregnancy-addon' ) . $plural_name . __ ( ' found in Trash', 'pregnancy-addon' ),
					'parent_item_colon' => 	__ ( 'Parent ', 'pregnancy-addon' ) . $singular_name . ':',
					'menu_name' 		=> 	$plural_name
			);
			
			$args = array (
					'labels' => $labels,
					'hierarchical' => false,
					'description' => __( 'This is custom post type ', 'pregnancy-addon' ) . $plural_name,
					'supports' => array (
						'title',
						'editor',
						'excerpt',
						'thumbnail'
					),
					
					'public' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					'menu_position' => 10,
					'menu_icon' => 'dashicons-portfolio',
					
					'show_in_nav_menus' => true,
					'publicly_queryable' => true,
					'exclude_from_search' => false,
					'has_archive' => true,
					'query_var' => true,
					'can_export' => true,
					'rewrite' => array( 'slug' => $postslug ),
					'capability_type' => 'post'
			);

			register_post_type ( 'dt_procedure', $args );

		}

		/**
		 */
		function dt_add_procedure_meta_box() {
			add_meta_box ( 'dt-procedure-default-metabox', __ ( 'Default Options', 'pregnancy-addon' ), array (
				$this,
				'dt_default_metabox' 
			), 'dt_procedure', 'normal', 'default' );
		}
		
		/**
		 */
		function dt_default_metabox() {
			include_once plugin_dir_path ( __FILE__ ) . 'metaboxes/dt_procedure_default_metabox.php';
		}
		
		/**
		 *
		 * @param unknown $columns        	
		 * @return multitype:
		 */
		function dt_procedure_edit_columns($columns) {
			
			$newcolumns = array (
				"cb" => "<input type=\"checkbox\" />",
				"dt_procedure_thumb" => __("Image", "pregnancy-addon"),
				"title" => __("Title", "pregnancy-addon"),
				"author" => __("Author", "pregnancy-addon")
			);
			$columns = array_merge ( $newcolumns, $columns );
			return $columns;
		}
		
		/**
		 *
		 * @param unknown $columns
		 * @param unknown $id
		 */
		function dt_procedure_columns_display($columns, $id) {
			global $post;
			
			switch ($columns) {

				case "dt_procedure_thumb" :
				    $image = wp_get_attachment_image(get_post_thumbnail_id($id), array(75,75));
					if(!empty($image)):
					  	echo !empty($image) ? $image : '';
					endif;
				break;
			}
		}

		/**
		 */
		function save_post_meta($post_id) {

			if( key_exists ( '_inline_edit',$_POST )) :
				if ( wp_verify_nonce($_POST['_inline_edit'], 'inlineeditnonce')) return;
			endif;
			
			if( key_exists( 'dt_theme_procedure_meta_nonce',$_POST ) ) :
				if ( ! wp_verify_nonce( $_POST['dt_theme_procedure_meta_nonce'], 'dt_theme_pregnancy_nonce') ) return;
			endif;
		 
			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
			
			if (!current_user_can('edit_post', $post_id)) :
				return;
			endif;
			
			if ( (key_exists('post_type', $_POST)) && ('dt_procedure' == $_POST['post_type']) ) :

				$layout = isset($_POST['layout']) ? $_POST['layout'] : '';
				if($layout) :

					$settings = array ();
					$settings['layout'] = $layout;

					$settings['sub-title-bg'] = isset ( $_POST['sub-title-bg'] ) ? $_POST['sub-title-bg'] : "";
					$settings['sub-title-bg-repeat'] = isset ( $_POST['sub-title-bg-repeat'] ) ? $_POST['sub-title-bg-repeat'] : "";
					$settings['sub-title-opacity'] = isset ( $_POST['sub-title-opacity'] ) ? $_POST['sub-title-opacity'] : "";
					$settings['sub-title-bg-position'] = isset ( $_POST['sub-title-bg-position'] ) ? $_POST['sub-title-bg-position'] : "";
					$settings['sub-title-bg-color'] = isset ( $_POST['sub-title-bg-color'] ) ? $_POST['sub-title-bg-color'] : "";

					if($layout == 'with-both-sidebar') {
						$settings['show-standard-sidebar-left'] = isset( $_POST['show-standard-sidebar-left'] ) ? $_POST['show-standard-sidebar-left'] : '';
						$settings['show-standard-sidebar-right'] = isset( $_POST['show-standard-sidebar-right'] ) ? $_POST['show-standard-sidebar-right'] : '';
						$settings['widget-area-left'] = isset( $_POST['dttheme']['widgetareas-left'] ) ? array_unique(array_filter($_POST['dttheme']['widgetareas-left'])) : '';
						$settings['widget-area-right'] = isset( $_POST['dttheme']['widgetareas-right'] ) ? array_unique(array_filter($_POST['dttheme']['widgetareas-right'])) : '';
					} elseif($layout == 'with-left-sidebar') {
						$settings['show-standard-sidebar-left'] = isset( $_POST['show-standard-sidebar-left'] ) ? $_POST['show-standard-sidebar-left'] : '';
						$settings['widget-area-left'] =  isset($_POST['dttheme']['widgetareas-left']) ? array_unique(array_filter($_POST['dttheme']['widgetareas-left'])) : '';
					} elseif($layout == 'with-right-sidebar') {
						$settings['show-standard-sidebar-right'] = isset( $_POST['show-standard-sidebar-right'] ) ? $_POST['show-standard-sidebar-right'] : '';
						$settings['widget-area-right'] =  isset($_POST['dttheme']['widgetareas-right']) ? array_unique(array_filter($_POST['dttheme']['widgetareas-right'])) : '';
					}

					$settings ['post-layout'] = isset ( $_POST ['post-layout'] ) ? $_POST ['post-layout'] : "";
					$settings ['meta_title'] = isset ( $_POST['dttheme-meta-title'] ) ? $_POST['dttheme-meta-title'] : "";
					$settings ['meta_value'] = isset ( $_POST['dttheme-meta-value'] ) ? $_POST['dttheme-meta-value'] : "";
	
					update_post_meta ( $post_id, "_custom_settings", array_filter ( $settings ) );
	
				endif;
			endif;
		}

		/**
		 * To load pregnancy pages in front end
		 *
		 * @param string $template        	
		 * @return string
		 */
		function dt_template_include($template) {
			if (is_singular( 'dt_procedure' )) {
				if (! file_exists ( get_stylesheet_directory () . '/single-dt_procedure.php' )) {
					$template = plugin_dir_path ( __FILE__ ) . 'templates/single-dt_procedure.php';
				}
			} 
			return $template;
		}
	}
}
?>