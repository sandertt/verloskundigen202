<?php
if (! class_exists ( 'DTProcedureCustomPostType' )) {

	class DTProcedureCustomPostType {

		function __construct() {

			// Add Hook into the 'wp_enqueue_scripts()' action			
			add_action ( 'wp_enqueue_scripts', array ( $this, 'dt_wp_enqueue_scripts' ) );

			// Pregnancy custom post type
			require_once plugin_dir_path ( __FILE__ ) . '/dt-procedure-post-type.php';
			if (class_exists ( 'DTProcedurePostType' )) {
				new DTProcedurePostType();
			}

			// Doctor custom post type
			require_once plugin_dir_path ( __FILE__ ) . '/dt-doctor-post-type.php';
			if( class_exists('DTDoctorPostType') ) {
				new DTDoctorPostType();
			}
		}
		
		/**
		 * A function hook that the WordPress core launches at 'wp_enqueue_scripts' points
		 * Works in both front and back end
		 */
		function dt_wp_enqueue_scripts() {
			
			wp_enqueue_script ( 'dt-pregnancy-addon-scripts', plugins_url ('designthemes-pregnancy-addon') . '/js/pregnancy.js', array (), false, true );
			wp_enqueue_style ( 'dt-pregnancy-addon', plugins_url ('designthemes-pregnancy-addon') . '/css/pregnancy.css', array (), false, 'all' );
		}
	}
}?>