<?php
/*
 * Plugin Name:	DesignThemes Pregnancy Addon
 * URI: 	http://wedesignthemes.com/plugins/designthemes-pregnancy-addon
 * Description: A simple wordpress plugin designed to implements <strong>pregnancy features of DesignThemes</strong> 
 * Version: 	1.0
 * Author: 		DesignThemes
 * Text Domain: pregnancy-addon
 * Author URI:	http://themeforest.net/user/designthemes
 */
if (! class_exists ( 'DTPregnancyAddon' )) {

	class DTPregnancyAddon {

		function __construct() {

			$this->plugin_dir_path = plugin_dir_path ( __FILE__ );

			// Add Hook into the 'init()' action
			add_action ( 'init', array (
					$this, 'dtLoadPluginTextDomain'
			) );

			// Register Custom Post Types
			require_once plugin_dir_path ( __FILE__ ) . '/custom-post-types/register-post-types.php';

			if (class_exists ( 'DTProcedureCustomPostType' )) {
				$dt_procedure_custom_posts = new DTProcedureCustomPostType();
			}
			
			// Register Reservation System
			require_once plugin_dir_path( __FILE__ ).'/reservation/register-reservation-system.php';

			if (class_exists ( 'DTReservationSystem' )) {
				$dt_reservation_system = new DTReservationSystem ();
			}

			// Register Shortcodes
			require_once plugin_dir_path ( __FILE__ ) . '/shortcodes/shortcodes.php';

			if (class_exists ( 'DTPregnancyShortcodesDefinition' )) {
				new DTPregnancyShortcodesDefinition ();
			}
			
			// Register Widgets
			require_once plugin_dir_path ( __FILE__ ) . '/widgets/register-widgets.php';

			if (class_exists ( 'DTPregnancyWidgets' )) {
				$dt_procedure_widgets = new DTPregnancyWidgets ();
			}

			// Register Visual Composer
			require_once plugin_dir_path ( __FILE__ ) . '/vc/register-vc.php';
			if(class_exists('DTVCPregnancyModule')){
				new DTVCPregnancyModule();
			}			
		}

		/**
		 * To load text domain
		 */
		function dtLoadPluginTextDomain() {
			load_plugin_textdomain ( 'pregnancy-addon', false, dirname ( plugin_basename ( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * To load plugin activate
		 */
		public static function dtPregnancyAddonActivate() {
			//Shell script file for agenda
			$sh = plugin_dir_path( __FILE__ ).'reservation/cron/send_agenda_cron.sh';
			$php = plugin_dir_path( __FILE__ ).'reservation/cron/send_agenda_cron.php';

			$sh_content = file_get_contents($sh);

			if ( preg_match( '/doc_root/', $sh_content ) ) {
                $sh_content = preg_replace( '/doc_root/', $_SERVER[ 'DOCUMENT_ROOT' ], $sh_content );
            }
            if ( preg_match( '/full_path/', $sh_content ) ) {
                $sh_content = preg_replace( '/full_path/', $php, $sh_content );
            }
            file_put_contents( $sh, $sh_content );
		}

		/**
		 * To load plugin deactivate
		 */
		public static function dtPregnancyAddonDectivate() {
			$sh = plugin_dir_path( __FILE__ ).'reservation/cron/send_agenda_cron.sh';
			file_put_contents( $sh, "#!/bin/sh\ncd doc_root\nphp -f full_path" );
		}
	}
}

if (class_exists ( 'DTPregnancyAddon' )) {

	register_activation_hook ( __FILE__, array (
			'DTPregnancyAddon',
			'dtPregnancyAddonActivate' 
	) );
	register_deactivation_hook ( __FILE__, array (
			'DTPregnancyAddon',
			'dtPregnancyAddonDectivate' 
	) );

	$dt_pregnancy_plugin = new DTPregnancyAddon ();
}
?>