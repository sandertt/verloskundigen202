<?php
if ( !class_exists( 'DTStaffPostType' ) ) {

	class DTStaffPostType {

		function __construct() {
			add_action ( 'init', array (
				$this,
				'dt_init'
			) );

			add_action( 'admin_init', array(
				$this,
				'dt_admin_init'
			) );
		}

		function dt_init() {
			$labels = array(
				'name' => __('Staffs', 'pregnancy' ),
				'singular_name' => __('Staff', 'pregnancy' ),
				'menu_name' => __('Staffs', 'pregnancy' ),
				'add_new' => __('Add Staff', 'pregnancy' ),
				'add_new_item' => __('Add New Staff', 'pregnancy' ),
				'edit' => __('Edit Staff', 'pregnancy' ),
				'edit_item' => __('Edit Staff', 'pregnancy' ),
				'new_item' => __('New Staff', 'pregnancy' ),
				'view' => __('View Staff', 'pregnancy' ),
				'view_item' => __('View Staff', 'pregnancy' ),
				'search_items' => __('Search Staffs', 'pregnancy' ),
				'not_found' => __('No Staffs found', 'pregnancy' ),
				'not_found_in_trash' => __('No Staffs found in Trash', 'pregnancy' ),
				'parent_item_colon' => __('Parent Staff:', 'pregnancy' ),
			);

			$args = array(
				'labels' => $labels,
				'hierarchical' => false,
				'description' => __('This is Custom Post type named as Staffs','pregnancy-addon'),
				'supports' => array('title', 'editor', 'thumbnail'),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'menu_position' => 20,
				'menu_icon' => 'dashicons-businessman',
			);

			register_post_type('dt_staffs', $args );
		}

		function dt_admin_init() {
			add_action ( 'add_meta_boxes', array (
				$this,
				'dt_add_staff_meta_box' 
			) );

			add_action ( 'save_post', array (
				$this,
				'save_staff_post_meta' 
			) );
			
			add_action ( 'pre_post_update', array (
				$this,
				'save_staff_post_meta' 
			) );
		}

		function dt_add_staff_meta_box() {

			add_meta_box( 'dt-member-service-metabox', esc_html__('Choose Services','pregnancy-addon'),
				array( $this, 'dt_member_service_metabox'), 'dt_staffs', 'side', 'default');

			add_meta_box( 'dt-member-metabox', esc_html__('Set Personal Information','pregnancy-addon'),
				array( $this, 'dt_member_metabox'), 'dt_staffs', 'normal',  'default');

			add_meta_box( 'dt-member-schedule-metabox', esc_html__('Choose Schedule','pregnancy-addon'),
				array( $this, 'dt_member_schedule_metabox'), 'dt_staffs', 'normal', 'default');	
		}

		function dt_member_service_metabox() {
			include_once plugin_dir_path ( __FILE__ ) . 'metaboxes/dt_member_service_metabox.php';
		}

		function dt_member_metabox() {
			include_once plugin_dir_path ( __FILE__ ) . 'metaboxes/dt_member_metabox.php';
		}

		function dt_member_schedule_metabox() {
			include_once plugin_dir_path ( __FILE__ ) . 'metaboxes/dt_member_schedule_metabox.php';
		}

		function save_staff_post_meta( $post_id ) {

			if (defined ( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE)
				return $post_id;

			if ( isset($_POST['_info']) ):
				$info = array_unique(array_filter($_POST['_info']));
				update_post_meta ( $post_id, "_info", array_filter ( $info ) );
			endif;

			if (isset($_POST['_timer'])):
				$timer = $_POST['_timer'];
				update_post_meta ( $post_id, "_timer", $timer );
			endif;

			if (isset($_POST['_services'])):
				$services = array_unique(array_filter($_POST['_services']));
				update_post_meta ( $post_id, "_services", array_filter ( $services ) );
			endif;
		}
	}
}?>
