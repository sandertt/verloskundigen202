<?php
if ( !class_exists( 'DTCustomerPostType' ) ) {

	class DTCustomerPostType {

		function __construct() {
			add_action ( 'init', array (
				$this,
				'dt_init'
			) );

			add_action( 'admin_init', array(
				$this,
				'dt_admin_init'
			) );
		}

		function dt_init() {
			$labels = array(
				'name' => __('Customers', 'pregnancy' ),
				'singular_name' => __('Customer', 'pregnancy' ),
				'menu_name' => __('Customers', 'pregnancy' ),
				'add_new' => __('Add Customer', 'pregnancy' ),
				'add_new_item' => __('Add New Customer', 'pregnancy' ),
				'edit' => __('Edit Customer', 'pregnancy' ),
				'edit_item' => __('Edit Customer', 'pregnancy' ),
				'new_item' => __('New Customer', 'pregnancy' ),
				'view' => __('View Customer', 'pregnancy' ),
				'view_item' => __('View Customer', 'pregnancy' ),
				'search_items' => __('Search Customers', 'pregnancy' ),
				'not_found' => __('No Customers found', 'pregnancy' ),
				'not_found_in_trash' => __('No Customers found in Trash', 'pregnancy' ),
				'parent_item_colon' => __('Parent Customer:', 'pregnancy' ),
			);

			$args = array(
				'labels' => $labels,
				'hierarchical' => false,
				'description' => __('This is Custom Post type named as Customers','pregnancy-addon'),
				'supports' => array('title'),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'menu_position' => 20,
				'menu_icon' => 'dashicons-groups',
			);

			register_post_type('dt_customers', $args );
		}

		function dt_admin_init() {
			add_action ( 'add_meta_boxes', array (
				$this,
				'dt_add_customer_meta_box' 
			) );

			add_action ( 'save_post', array (
				$this,
				'save_customer_post_meta' 
			) );
			
			add_action ( 'pre_post_update', array (
				$this,
				'save_customer_post_meta' 
			) );
		}

		function dt_add_customer_meta_box() {

			add_meta_box( 'dt-member-metabox', esc_html__('Set Personal Information','pregnancy-addon'),
				array( $this, 'pregnancy_dt_customer_metabox'), 'dt_customers', 'normal',  'default');
		}

		function pregnancy_dt_customer_metabox() {
			include_once plugin_dir_path ( __FILE__ ) . 'metaboxes/pregnancy_dt_customer_metabox.php';
		}

		function save_customer_post_meta( $post_id ) {

			if (defined ( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE)
				return $post_id;

			if ( isset($_POST['_info']) ):
				$info = array_unique(array_filter($_POST['_info']));
				update_post_meta ( $post_id, "_info", array_filter ( $info ) );
			endif;
		}
	}
}?>
