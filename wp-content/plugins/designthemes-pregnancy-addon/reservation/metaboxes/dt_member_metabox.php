<?php global $post;
	$info = get_post_meta( $post->ID, "_info", true );
	$info = is_array( $info ) ? $info : array();

	$price = array_key_exists('price', $info) ? $info['price'] : "";
	$emailid = array_key_exists('emailid', $info) ? $info['emailid'] : "";?>

<div class="custom-box">
	<div class="column one-sixth"><?php _e( 'Price', 'pregnancy' );?></div>
	<div class="column five-sixth last">
		<input type="text" name="_info[price]" class='small-text' value="<?php echo esc_attr( $price ); ?>">
		<?php echo pregnancy_dt_appointment_currency_symbol( pregnancy_option( 'pregnancy_appointment', 'currency' ) );?>
	</div>
</div>

<div class="custom-box">
	<div class="column one-sixth"><?php _e( 'Email Id', 'pregnancy' );?></div>
	<div class="column five-sixth last">
		<input type="text" name="_info[emailid]" class='regular-text' value="<?php echo esc_attr( $emailid ); ?>">
	</div>
</div>