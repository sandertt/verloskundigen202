<?php
class DTPregnancyShortcodesDefinition {

	function __construct() {

		/* Procedure List */
		add_shortcode ( "dt_sc_procedure_list", array (
			$this,
			"dt_sc_procedure_list"
		) );


		/* Pregnancy List2 */
		add_shortcode ( "dt_sc_procedure_list2", array (
			$this,
			"dt_sc_procedure_list2"
		) );

		/* Workout */
		add_shortcode ( "dt_sc_workout", array (
			$this,
			"dt_sc_workout"
		) );

		/* Process Step */
		add_shortcode ( "dt_sc_process_step", array (
			$this,
			"dt_sc_process_step"
		) );


		/* Pregnancy Nav */
		add_shortcode ( "dt_sc_procedure_nav", array (
			$this,
			"dt_sc_procedure_nav"
		) );

		/* Pregnancy Info */
		add_shortcode ( "dt_sc_pregnancy_info", array (
			$this,
			"dt_sc_pregnancy_info"
		) );

		/* BMI */
		add_shortcode ( "dt_sc_bmi_calc", array(
			$this,
			"dt_sc_bmi_calc"
		) );
		
		/* Title with desc */
		add_shortcode ( "dt_sc_title_desc", array(
			$this,
			"dt_sc_title_desc"
		) );
		
		/* Colored Icon Box */
		add_shortcode ( "dt_sc_colored_icon_box", array(
			$this,
			"dt_sc_colored_icon_box"
		) );
		
		/* Cycle Section */
		add_shortcode ( "dt_sc_cycle_section", array(
			$this,
			"dt_sc_cycle_section"
		) );
		
		/* Location Section */
		add_shortcode ( "dt_sc_location", array(
			$this,
			"dt_sc_location"
		) );
		
		/* appointment */
		add_shortcode ( "dt_sc_reserve_appointment", array(
			$this,
			"dt_sc_reserve_appointment"
		) );
		
		/* dt_sc_video_section */
		add_shortcode ( "dt_sc_video_section", array(
			$this,
			"dt_sc_video_section"
		) );
		
		/* dt_sc_current_doctor_info */
		add_shortcode ( "dt_sc_current_doctor_info", array(
			$this,
			"dt_sc_doctor_item"
		) );
		
		/* dt_sc_doctors_info */
		add_shortcode ( "dt_sc_doctors_info", array(
			$this,
			"dt_sc_doctors_info"
		) );
		
		/* dt_sc_doctors_with_filter */
		add_shortcode ( "dt_sc_doctors_with_filter", array(
			$this,
			"dt_sc_doctors_with_filter"
		) );
		
		// Ajax's for Doctor filtering Shortcode
		add_action( 'wp_ajax_dt_sc_filter_doctors', array(
			$this, 'dt_sc_filter_doctors'
		) );

		add_action( 'wp_ajax_nopriv_dt_sc_filter_doctors', array(
			$this, 'dt_sc_filter_doctors'
		) );
		
		/* Trimster Section */
		add_shortcode ( "dt_sc_trimester", array(
			$this,
			"dt_sc_trimester"
		) );
		
		/* Single Product Section */
		add_shortcode ( "dt_sc_single_product", array(
			$this,
			"dt_sc_single_product"
		) );
		
		/* Featured Product Carousel section */
		add_shortcode ( "dt_sc_featured_product_carousel", array(
			$this,
			"dt_sc_featured_product_carousel"
		) );
		
	}

	/**
	 *
	 * @param string $content
	 * @return string
	 */
	function dtShortcodeHelper($content = null) {
		$content = do_shortcode ( shortcode_unautop ( $content ) );
		$content = preg_replace ( '#^<\/p>|^<br \/>|<p>$#', '', $content );
		$content = preg_replace ( '#<br \/>#', '', $content );
		return trim ( $content );
	}


	/**
	 * procedure list : filterable procedure list
	 * @return string
	 */
	function dt_sc_procedure_list($attrs, $content = null) {
		extract(shortcode_atts(array(
			'procedure_id' => '',
			'button_text' =>'View Procedure'
		), $attrs));

		$out = "";

		#Performing query...
		$args = array('post_type' => 'dt_procedure' , 'p' => $procedure_id,
			);

		$the_query = new WP_Query($args);
		if($the_query->have_posts()):

				while($the_query->have_posts()): $the_query->the_post();
					$PID = $procedure_id;

					$out .= '<div class="dt-sc-popular-procedures dt-sc-practices">';
						$out .= '<div class="image">';
								if(has_post_thumbnail()):
									$attr = array('title' => get_the_title(), 'alt' => get_the_title());
									$out .= get_the_post_thumbnail($PID, 'full', $attr);
								else:
									$out .= '<img src="http://place-hold.it/500x351&text='.esc_attr(get_the_title()).'" alt="'.esc_attr(get_the_title()).'" title="'.esc_attr(get_the_title()).'"/>';
								endif;
						$out .= '</div>';
						$out .= '<div class="details"><h3><a href="'.esc_url(get_permalink()).'" title="'.esc_attr(get_the_title()).'">'.esc_html(get_the_title()).'</a></h3><p>'.get_the_excerpt().'</p><a class="view" href="'.esc_url(get_permalink()).'" title="'.esc_attr(get_the_title()).'">'.esc_html($button_text).'</a></div>'; 
					$out .= '</div>';
				endwhile;
		wp_reset_postdata();
		else:
			$out .= '<h2>'.esc_html__("Nothing Found.", "pregnancy-addon").'</h2>';
			$out .= '<p>'.esc_html__("Apologies, but no results were found for the requested archive.", "pregnancy-addon").'</p>';
		endif;

		return $out;
	}

	/**
	 * pregnancy list2
	 * @return string
	 */
	function dt_sc_procedure_list2($attrs, $content = null) {
		extract(shortcode_atts(array(
			'limit' => -1,
			'categories' => '',
			'button_text' => esc_html__('Join Training', 'pregnancy-addon')
		), $attrs));

		global $post;
		$out = "";

		if(empty($categories)) {
			$cats = get_categories('taxonomy=procedure_entries&hide_empty=1');
			$cats = get_terms( array('procedure_entries'), array('fields' => 'ids'));		
		} else {
			$cats = explode(',', $categories);
		}

		#Performing query...
		$args = array('post_type' => 'dt_procedure', 'posts_per_page' => $limit,'tax_query' => array( array( 'taxonomy' => 'procedure_entries', 'field' => 'id', 'terms' => $cats ) ) );
		$the_query = new WP_Query($args);
		if($the_query->have_posts()): $i = 1;
		 while($the_query->have_posts()): $the_query->the_post();

			$temp_class = "";

			if($i == 1) $temp_class = " first";
			if($i == 2) $i = 1; else $i = $i + 1;

			$out .= '<div class="column dt-sc-one-half '.esc_attr($temp_class).'">';
				$out .= '<div class="dt-sc-training">';
					$out .= '<div class="dt-sc-training-thumb"> ';
						if(has_post_thumbnail()):
							$attr = array('title' => get_the_title(), 'alt' => get_the_title());
							$out .= get_the_post_thumbnail($post->ID, 'training-list2', $attr);
						else:
							$out .= '<img src="http://place-hold.it/280x311&text='.esc_attr(get_the_title()).'" alt="'.esc_attr(get_the_title()).'" title="'.esc_attr(get_the_title()).'" />';
						endif;
						$out .= '<div class="dt-sc-training-thumb-overlay">';
							$out .= '<a class="dt-sc-button small filled" title="'.esc_attr(get_the_title()).'" href="'.esc_url(get_permalink()).'">'.esc_html($button_text).'</a>';
						$out .= '</div>';
					$out .= '</div>';
					$out .= '<div class="dt-sc-training-details">';
						$out .= '<h6>'.esc_html(get_the_title()).'</h6>';
						$pregnancy_settings = get_post_meta($post->ID, '_custom_settings', true);
						$pregnancy_settings = is_array ( $pregnancy_settings ) ? $pregnancy_settings : array ();
						$out .= '<ul>';
							if(array_key_exists('timing', $pregnancy_settings))
								$out .= '<li> <span class="pe-icon pe-stopwatch"> </span> '.esc_html($pregnancy_settings['timing']).' </li>';

								$author_id = $post->post_author;
								$out .= '<li> <span class="pe-icon pe-user"> </span> '.esc_html(get_the_author_meta( 'user_nicename' , $author_id )).' </li>';

							if(array_key_exists('duration', $pregnancy_settings))
								$out .= '<li> <span class="pe-icon pe-date"> </span> '.esc_html($pregnancy_settings['duration']).' </li>';
						$out .= '</ul>';
						$out .= pregnancy_excerpt(20);
						$out .= '<div class="dt-sc-training-details-overlay">';
							$out .= '<h6>'.esc_html(get_the_title()).'</h6>';
							if(array_key_exists('price', $pregnancy_settings))
								$out .= '<div class="price"> <sup> '.esc_html($pregnancy_settings['pre_price']).' </sup> '.esc_html($pregnancy_settings['price']).' /<sub>'.esc_html($pregnancy_settings['post_price']).' </sub> </div>';
						$out .= '</div>';
					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
			if($i == 1) $out .= '<div class="dt-sc-hr-invisible-small"> </div>';

		 endwhile;
		wp_reset_postdata();
		else:
			$out .= '<h2>'.esc_html__('Nothing Found.', 'pregnancy-addon').'</h2>';
			$out .= '<p>'.esc_html__('Apologies, but no results were found for the requested archive.', 'pregnancy-addon').'</p>';
		endif;

		return $out;
	}	

	/**
	 * workout
	 * @return string
	 */
	function dt_sc_workout($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'title' => '',
			'subtitle' => '',
			'image' => '',
			'link' => '',
			'add_icon' => '',
			'iconclass' => '',
			'class' => ''
		), $attrs ) );

		if( empty( $image ) )
			$class .= ' no-workout-thumb';

		$out = '<div class="dt-sc-workouts '.esc_attr($class).'">';

			if(!empty($image)):
				$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => 'full' ));
				$image = $image['thumbnail'];

				$out .= '<div class="dt-sc-workouts-thumb">';
					$out .= $image;
				$out .= '</div>';
			endif;

			$out .= '<div class="dt-sc-workouts-details">';
				$out .= '<h6>'.esc_html($subtitle).'</h6>';
				$out .= '<h4>'.esc_html($title).'</h4>';
				$out .= DTPregnancyShortcodesDefinition::dtShortcodeHelper ( $content );

				$link = ( '||' === $link ) ? '' : $link;
				$link = vc_build_link( $link );
				$a_href = $link['url'];
				$a_title = $link['title'];
				$a_target = $link['target'];

				$icon = "";
				if( $add_icon == 'true' && !empty( $iconclass ) ) {
					$icon = '<span class="'.esc_attr($iconclass).'"> </span>';
				}

				$out .= '<a class="dt-sc-button small filled" title="'.esc_attr($a_title).'" href="'.esc_url($a_href).'">'.esc_html($a_title).esc_html($icon).'</a>';
			$out .= '</div>';

		$out .= '</div>';

		return $out;
	}

	/**
	 * process step
	 * @return string
	 */
	function dt_sc_process_step($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'image' => '',
			'step' => '',
			'title' => '',
			'class' => ''
		), $attrs ) );

		$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => 'full' ));
		$image = $image['thumbnail'];

		$image = !empty( $image ) ? $image : '<img src="http://place-hold.it/130x130"/>';

		$out = '<div class="dt-sc-process-steps '.esc_attr($class).'">';
			$out .= '<div class="dt-sc-process-thumb">';
				$out .= esc_html($image);
				$out .= '<div class="dt-sc-process-thumb-overlay">';
					$out .= '<h5>'.esc_html($step).'</h5>';
				$out .= '</div>';
			$out .= '</div>';
			$out .= '<div class="dt-sc-process-details">';
				$out .= '<h5>'.esc_html($title).'</h5>';
				$out .= DTPregnancyShortcodesDefinition::dtShortcodeHelper ( $content );
			$out .= '</div>';
		$out .= '</div>';

		return $out;
	}

	/**
	 * pregnancy nav
	 * @return string
	 */
	function dt_sc_procedure_nav($attrs, $content = null) {
		extract(shortcode_atts(array(
			'limit' => -1
		), $attrs));

		global $post;
		$postID = $post->ID;
		
		$out = $temp = "";

		$args = array('post_type' => 'dt_procedure', 'posts_per_page' => $limit);
		$the_query = new WP_Query($args);
		if($the_query->have_posts()):
		 $out = '<ul class="dt-sc-fitness-pregnancy-nav">';
		 while($the_query->have_posts()): $the_query->the_post();
			if($postID == get_the_ID())
				$temp = ' class="current_page_item"';

		 	$out .= '<li'.$temp.'> <a title="'.get_the_title().'" href="'.get_permalink().'">'.get_the_title().'</a> </li>';
			$temp = "";
		 endwhile;
		 $out .= '</ul>';
		wp_reset_postdata();
		else:
			$out .= '<h2>'.esc_html__("Nothing Found.", "pregnancy-addon").'</h2>';
		endif;
		
		return $out;
	}

	/**
	 * pregnancy info
	 * @return string
	 */
	function dt_sc_pregnancy_info($attrs, $content = null) {
		extract(shortcode_atts(array(
			'meta' => ''
		), $attrs));

		global $post;
		$out = "";

		$out = '<div class="dt-sc-fitness-pregnancy-short-details-wrapper">';
			if(has_post_thumbnail()):
				$attr = array('title' => get_the_title(), 'alt' => get_the_title());
				$out .= get_the_post_thumbnail($post->ID, 'full', $attr);
			else:
				$out .= '<img src="http://place-hold.it/900x445&text='.get_the_title().'" alt="'.get_the_title().'" title="'.get_the_title().'" />';
			endif;

			if($meta != 'no'):
				$out .= '<div class="dt-sc-fitness-pregnancy-short-details">';
					$pregnancy_settings = get_post_meta($post->ID, '_custom_settings', true);
					$pregnancy_settings = is_array ( $pregnancy_settings ) ? $pregnancy_settings : array ();

					if(array_key_exists('subtitle', $pregnancy_settings))
						$out .= '<h2>'.$pregnancy_settings['subtitle'].'</h2>';
					$out .= '<ul>';
						if(array_key_exists('levels', $pregnancy_settings))
							$out .= '<li> <span> '.esc_html__('Workout Levels', 'pregnancy-addon').' </span> : '.esc_html($pregnancy_settings['levels']).' </li>';
						if(array_key_exists('timing', $pregnancy_settings))
							$out .= '<li> <span> '.esc_html__('Workout Timing', 'pregnancy-addon').' </span> : '.esc_html($pregnancy_settings['timing']).' </li>';
						if(array_key_exists('duration', $pregnancy_settings))
							$out .= '<li> <span> '.esc_html__('Goal Duration', 'pregnancy-addon').' </span> : '.esc_html($pregnancy_settings['duration']).' </li>';

						if( array_key_exists('meta_title', $pregnancy_settings) ):
							foreach( $pregnancy_settings['meta_title'] as $key => $title ):
								$value = $pregnancy_settings['meta_value'][$key];
								if( !empty($value) ):
									$out .= '<li> <span> '.esc_html($title).' </span> : '.esc_html($value).' </li>';
								endif;
							endforeach;
						endif;
					$out .= '</ul>';
				$out .= '</div>';
			endif;
		$out .= '</div>';
		
		return $out;
	}

	function dt_sc_bmi_calc( $attrs, $content = null ){
		extract(shortcode_atts(array(
			'title' => '',
			'css' => ''
		), $attrs));

		$class = vc_shortcode_custom_css_class( $css );

		$out = "<div class='dt-sc-bmi-calculator ".esc_attr($class)."'>";
			$out .= '<h5>'.esc_html($title).'</h5>';
			$out .= '<form name="frmbmi" action="#" method="post">';

				$out .= '<div class="column dt-sc-one-third first group-textbox">';
					$out .= '<label>'.esc_html__('Height', 'pregnancy-addon').'<span>'.esc_html__('Ft/in', 'pregnancy-addon').'</span></label>';
					$out .= '<input name="txtfeet" placeholder="'.esc_html__('FT', 'pregnancy-addon').'" type="text" required="required">';
					$out .= '<input name="txtinches" placeholder="'.esc_html__('IN', 'pregnancy-addon').'" type="text" required="required">';
				$out .= '</div>';

				$out .= '<div class="column dt-sc-one-third">';
					$out .= '<label>'.esc_html__('Weight', 'pregnancy-addon').'<span>'.esc_html__('LBS', 'pregnancy-addon').'</span> </label>';
					$out .= '<input name="txtlbs" type="text" required="required">';
				$out .= '</div>';

				$out .= '<div class="column dt-sc-one-third">';
					$out .= '<label>'.esc_html__('Select Gender', 'pregnancy-addon').'</label>';
					$out .= '<div class="selection-box">';
						$out .= '<select>';
							$out .= '<option>'.esc_html__('Male', 'pregnancy-addon').'</option>';
							$out .= '<option>'.esc_html__('Female', 'pregnancy-addon').'</option>';
						$out .= '</select>';
					$out .= '</div>';
				$out .= '</div>';

				$out .= '<input name="subbmi" value="'.esc_html__('Calculate BMI', 'pregnancy-addon').'" type="submit">';
				$out .= '<input type="reset" value="'.esc_html__('Reset', 'pregnancy-addon').'">';
				$out .= '<div class="dt-sc-bmi-result">';
					$out .= '<div class="column dt-sc-one-third first">';
						$out .= '<label> <span>'.esc_html__('Your BMI is', 'pregnancy-addon').'</span> </label>';
					$out .= '</div>';
					$out .= '<div class="column dt-sc-one-fifth">';
						$out .= '<input name="txtbmi" placeholder="0.0" type="text" readonly>';
					$out .= '</div>';
					$out .= '<div class="column dt-sc-one-third">';
						$out .= '<a href="#tblbmicontent" class="fancyInline">'.esc_html__('View BMI Class', 'pregnancy-addon').'<span class="pe-icon pe-angle-right-circle"> </span></a>';
					$out .= '</div>';
				$out .= '</div>';

			$out .= '</form>';
			$out .= '<div id="tblbmicontent" class="tblbmi">';
				$out .= '<div class="dt-bmi-inner-content">';
					$out .= DTPregnancyShortcodesDefinition::dtShortcodeHelper ( $content );
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';

		return $out;
	}
	
	/**
	 * title desc
	 * @return string
	 */
	function dt_sc_title_desc($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'title_type' => '',
			'title' => '',
			'image' => '',
			'separator_image_color' => '',
			'separator_display_style' => '',
			'separator_align'  => '',
			'title_align' => '',
			'class' =>''
		), $attrs ) );

		$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => 'full' ));
		$image = $image['p_img_large'][0];
		
		$title_class = '';
		
		if($title_type == "title"){
			$title_class = 'dt-sc-simple-title';
		}else{
			$title_class = 'with-desc';
		}
		
		$out = '<div class="dt-sc-title '.$separator_display_style.' '.$title_align.' '.' '.$separator_align.' '.$title_class.$class.' ">';
				$out .= '<h2>'.esc_html($title).'</h2>';
				if($title_type != "title"){
					$out .= '<span><img src="'.$image.'" alt="" style="background-color:'.$separator_image_color.';'.'"></span>';
					$out .= !empty($content) ? '<h3>'.DTPregnancyShortcodesDefinition::dtShortcodeHelper ( $content ).'</h3>' : '';
				}
		$out .= '</div>';

		return $out;
	}
	
	/**
	 * colored icon box
	 * @return string
	 */
	function dt_sc_colored_icon_box($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'colored_icnbx_type' => 'type1',
			'title' => '',
			'image' => '',
			'hover_icn_image' => '',
			'link' => '',
			'variation' => '',
			'class' => '',
		), $attrs ) );

		$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => 'full' ));
		$image = $image['thumbnail'];
		
		$hover_icn_image = wpb_getImageBySize( array( 'attach_id' => $hover_icn_image, 'thumb_size' => 'full' ));
		$hover_icn_image = $hover_icn_image['thumbnail'];
		
		//parse link by vc
		$link = ( '||' === $link ) ? '' : $link;
		$link = vc_build_link( $link );
		$a_href = $link['url'];
		$a_title = $link['title'];
		$a_target = $link['target'];

		if(preg_match('#^{{#', $a_href) === 1) {
			$a_href =  str_replace ( '{{', '[', $a_href );
			$a_href =  str_replace ( '}}', '/]', $a_href );
			$a_href = do_shortcode($a_href);
		}else {
			$a_href = esc_url ( $a_href );
		}
		
		if(empty($a_target)){
			$a_target = '_self';
		}

		if( $colored_icnbx_type == "type1") {
		  $out = '<div class="dt-sc-colored-icon-box  '.$variation.' " >';
			$out .= $image;
			$out .= "<div class='dt-sc-colored-content'>
						<a class='icon-image' target='{$a_target}' title='{$a_title}' href='{$a_href}'>";
			$out .= 		$hover_icn_image;
			$out .=		'</a>';
			$out .= "<div class='dt-sc-colored-content-title'>
						<h3><a target='{$a_target}' title='{$a_title}' href='{$a_href}'>";
			$out .= 		$title;
			$out .= '	</a></h3>
					 </div>
					 </div>
			    </div>';
		}else{
			$out = '<div class="dt-sc-icon-box colored  '.$variation.' " >';
			$out .= '<div class="icon-wrapper">
						<span class="image-inside">';
			$out .= 		$image;
			$out .= '   </span>
					 </div>';
					 
			$out .= "<div class='icon-content'>";
			$out .= 	'<p>'.DTPregnancyShortcodesDefinition::dtShortcodeHelper ( $content ).'</p>';
			$out .= 	'<h4>'.$title.'</h4>';
			$out .= "</div>
			       </div>";
		}
				
		return $out;
	}
	
	/**
	 * cycle section
	 * @return string
	 */
	function dt_sc_cycle_section($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'center_title' => '',
			'center_placed_image' => '',
			'center_placed_img_color' => '',
			'icon_image_1' => '',
			'icon_image_1_content' => 'Lorem ipsum dolor sit amet',
			'icon_image_1_variation' => '',
			'icon1_lightbox_content' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			
			'icon_image_2' => '',
			'icon_image_2_content' => 'Lorem ipsum dolor sit amet',
			'icon_image_2_variation' => '',
			'icon2_lightbox_content' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			
			'icon_image_3' => '',
			'icon_image_3_content' => 'Lorem ipsum dolor sit amet',
			'icon_image_3_variation' => '',
			'icon3_lightbox_content' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			
			'icon_image_4' => '',
			'icon_image_4_content' => 'Lorem ipsum dolor sit amet',
			'icon_image_4_variation' => '',
			'icon4_lightbox_content' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			
			'icon_image_5' => '',
			'icon_image_5_content' => 'Lorem ipsum dolor sit amet',
			'icon_image_5_variation' => '',
			'icon5_lightbox_content' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			
			'icon_image_6' => '',
			'icon_image_6_content' => 'Lorem ipsum dolor sit amet',
			'icon_image_6_variation' => '',
			'icon6_lightbox_content' => base64_encode( '<p>I am raw html block.<br/>Click edit button to change this html</p>' ),
			
		), $attrs ) );

		$icon_image_1 = wpb_getImageBySize( array( 'attach_id' => $icon_image_1, 'thumb_size' => 'full' ));
		$icon_lightbox_image_1 = $icon_image_1['p_img_large'][0];
		$icon_image_1 = $icon_image_1['thumbnail'];
		
		$icon_image_2 = wpb_getImageBySize( array( 'attach_id' => $icon_image_2, 'thumb_size' => 'full' ));
		$icon_lightbox_image_2 = $icon_image_2['p_img_large'][0];
		$icon_image_2 = $icon_image_2['thumbnail'];
		
		$icon_image_3 = wpb_getImageBySize( array( 'attach_id' => $icon_image_3, 'thumb_size' => 'full' ));
		$icon_lightbox_image_3 = $icon_image_3['p_img_large'][0];
		$icon_image_3 = $icon_image_3['thumbnail'];
		
		$icon_image_4 = wpb_getImageBySize( array( 'attach_id' => $icon_image_4, 'thumb_size' => 'full' ));
		$icon_lightbox_image_4 = $icon_image_4['p_img_large'][0];
		$icon_image_4 = $icon_image_4['thumbnail'];
		
		$icon_image_5 = wpb_getImageBySize( array( 'attach_id' => $icon_image_5, 'thumb_size' => 'full' ));
		$icon_lightbox_image_5 = $icon_image_5['p_img_large'][0];
		$icon_image_5 = $icon_image_5['thumbnail'];
		
		$icon_image_6 = wpb_getImageBySize( array( 'attach_id' => $icon_image_6, 'thumb_size' => 'full' ));
		$icon_lightbox_image_6 = $icon_image_6['p_img_large'][0];
		$icon_image_6 = $icon_image_6['thumbnail'];
		
		$center_placed_image = wpb_getImageBySize( array( 'attach_id' => $center_placed_image, 'thumb_size' => 'full' ));
		$center_placed_image = $center_placed_image['p_img_large'][0];
		
		$out = '<div class="circular-content">
					<div class="center-placed-content">
						<div class="ico-content-group" style="background-image:url('.$center_placed_image.'); background-repeat:no-repeat; 
																							background-position:center center; background-color:'.$center_placed_img_color.';'.'">';
		$out .= 			'<p>'.$center_title.'</p>';
		$out .= '		</div>
					</div>';
		$out .= '	<div class="ico ico1 '.$icon_image_1_variation.'">
						<div class="circular-wrapper1">
							<a id="inline_html_simple" target="#icon-img-1-lightbox-content" data-type="inline" class="icon-cover" href="'.$icon_lightbox_image_1.'">';
		$out .= 				$icon_image_1;
		$out .= '			</a>
							<div class="ico-content ico-content1">
								<h3>'.$icon_image_1_content.'</h3>';
		$out .= '			</div>
						</div>';
		#lightbox content 
		$out .= "		<div id='icon-img-1-lightbox-content' class='icon-img-1-lightbox-content ".$icon_image_1_variation." ' style='display:none;'>
							<div class='light-box-display-content'>
								<div class='light-box-display-content-container'>";
		$out .= 					rawurldecode( base64_decode( strip_tags( $icon1_lightbox_content ) ) );
		$out .= "				</div>
							</div>
						</div>
					</div>";
				
		$out .= '	<div class="ico ico2 '.$icon_image_2_variation.'">
						<div class="circular-wrapper2">
							<a id="inline_html_simple2" target="#icon-img-2-lightbox-content" data-type="inline" class="icon-cover" href="'.$icon_lightbox_image_2.'">';
		$out .= 				$icon_image_2;
		$out .= '			</a>
							<div class="ico-content ico-content2">
								<h3>'.$icon_image_2_content.'</h3>';
		$out .= '			</div>
						</div>';
		#lightbox content
		$out .= "		<div id='icon-img-2-lightbox-content' class='icon-img-2-lightbox-content ".$icon_image_2_variation." ' style='display:none;'>
							<div class='light-box-display-content'>
								<div class='light-box-display-content-container'>";
		$out .= 					rawurldecode( base64_decode( strip_tags( $icon2_lightbox_content ) ) );
		$out .= "				</div>
							</div>
						</div>
					</div>";
				
		$out .= '	<div class="ico ico3 '.$icon_image_3_variation.'">
						<div class="circular-wrapper3">
							<a id="inline_html_simple3" target="#icon-img-3-lightbox-content" data-type="inline" class="icon-cover" href="'.$icon_lightbox_image_3.'">';
		$out .= 				$icon_image_3;
		$out .= '			</a>
							<div class="ico-content ico-content3">
								<h3>'.$icon_image_3_content.'</h3>';
		$out .= '			</div>
						</div>';
		#lightbox content
		$out .= "		<div id='icon-img-3-lightbox-content' class='icon-img-3-lightbox-content ".$icon_image_3_variation." ' style='display:none;'>
							<div class='light-box-display-content'>
								<div class='light-box-display-content-container'>";
		$out .= 					rawurldecode( base64_decode( strip_tags( $icon3_lightbox_content ) ) );
		$out .= "				</div>
							</div>
						</div>
					</div>";
				
		$out .= '	<div class="ico ico4 '.$icon_image_4_variation.'">
						<div class="circular-wrapper4">
							<a id="inline_html_simple4" target="#icon-img-4-lightbox-content" data-type="inline" class="icon-cover" href="'.$icon_lightbox_image_4.'">';
		$out .= 				$icon_image_4;
		$out .= '			</a>
							<div class="ico-content ico-content4">
								<h3>'.$icon_image_4_content.'</h3>';
		$out .= '			</div>
						</div>';
		#lightbox content
		$out .= "		<div id='icon-img-4-lightbox-content' class='icon-img-4-lightbox-content ".$icon_image_4_variation." ' style='display:none;'>
							<div class='light-box-display-content'>
								<div class='light-box-display-content-container'>";
		$out .= 					rawurldecode( base64_decode( strip_tags( $icon4_lightbox_content ) ) );
		$out .= "				</div>
							</div>
						</div>
					</div>";
				
		$out .= '	<div class="ico ico5 '.$icon_image_5_variation.'">
						<div class="circular-wrapper5">
							<a id="inline_html_simple5" target="#icon-img-5-lightbox-content" data-type="inline" class="icon-cover" href="'.$icon_lightbox_image_5.'">';
		$out .= 				$icon_image_5;
		$out .= '			</a>
							<div class="ico-content ico-content5">
								<h3>'.$icon_image_5_content.'</h3>';
		$out .= '			</div>
						</div>';
		#lightbox content
		$out .= "		<div id='icon-img-5-lightbox-content' class='icon-img-5-lightbox-content ".$icon_image_5_variation." ' style='display:none;'>
							<div class='light-box-display-content'>
								<div class='light-box-display-content-container'>";
		$out .= 					rawurldecode( base64_decode( strip_tags( $icon5_lightbox_content ) ) );
		$out .= "				</div>
							</div>
						</div>
					</div>";
				
		$out .= '	<div class="ico ico6 '.$icon_image_6_variation.'">
						<div class="circular-wrapper6">
							<a id="inline_html_simple6" target="#icon-img-6-lightbox-content" data-type="inline" class="icon-cover" href="'.$icon_lightbox_image_6.'">';
		$out .= 				$icon_image_6;
		$out .= '			</a>
							<div class="ico-content ico-content6">
								<h3>'.$icon_image_6_content.'</h3>';
		$out .= '			</div>
						</div>';
		#lightbox content
		$out .= "		<div id='icon-img-6-lightbox-content' class='icon-img-6-lightbox-content ".$icon_image_6_variation." ' style='display:none;'>
							<div class='light-box-display-content'>
								<div class='light-box-display-content-container'>";
		$out .= 					rawurldecode( base64_decode( strip_tags( $icon6_lightbox_content ) ) );
		$out .= "				</div>
							</div>
						</div>
					</div>
				</div>";

		return $out;
	}
	
	
	/**
	 * Location
	 * @return string
	 */
	function dt_sc_location($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'image' => '',
			'iconclass' => 'fa-map-marker',
			'title' => '',
			
		), $attrs ) );

		$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => 'full' ));
		$image = $image['thumbnail'];
		
		$out = '<div class="dt-sc-contact-info with-image">';
			$out .= $image;
			$out .= '<span class="'.$iconclass.'"> </span>';
			$out .= '<h6>'.$title.'</h6>';
			$out .= '<p>'.DTPregnancyShortcodesDefinition::dtShortcodeHelper ( $content ).'</p>';
		$out .= '</div>';
			
		return $out;
	}
	

	// Appointments
	function dt_sc_reserve_appointment($attrs, $content = null) {
		
		extract(shortcode_atts(array( 
			'serviceids' => '', 
			'staffids' => '', 
		), $attrs)); 
		
		$out = '';
		
		$url = pregnancy_dt_get_page_permalink_by_its_template('tpl-reservation-type1.php');
			$url = isset($url) ? $url : '';
			
			if($url != '') {
				
		$out = '<form class="dt-sc-reservation-form preg-appointment-form" name="reservation-schedule-form" method="post" action="'.$url.'">';
		
		$out .= '<div class="vc_row wpb_row vc_row-fluid">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<p><input type="text" id="cli-name" name="cli-name"  placeholder="'.__('Name','pregnancy-addon').'"></p>
                			</div>
						</div>
					</div>';

		$out .= '	<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">                                  
								<p><input type="text" id="cli-phone" name="cli-phone" placeholder="'.__('Phone','pregnancy-addon').'"></p>
            				</div>
						</div>
					</div>';
				
		$out .= '	<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
				  
							  <select name="services" id="services" class="dt-select-service">
								<option value="">'. __('Type of Service','pregnancy-addon').'</option>';
									if($serviceids != '') {
										$serviceids_arr = explode(',', $serviceids);
										$cp_services = get_posts( array('post_type'=>'dt_services', 'posts_per_page'=>'-1', 'post__in' => $serviceids_arr, 'suppress_filters' => false ) );
									} else {
										$cp_services = get_posts( array('post_type'=>'dt_services', 'posts_per_page'=>'-1', 'suppress_filters' => false ) );
									}
			
									if( $cp_services ){
										foreach( $cp_services as $cp_service ){
											$id = $cp_service->ID; 
											$title = $cp_service->post_title;
											$out .= "<option value='{$id}'>{$title}</option>";
										}
									}
					$out .= ' </select>
							</div>
						</div>
					</div>';

		$out .= '	<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner ">
							<div class="wpb_wrapper">
								<select name="staff" class="dt-select-staff">
									<option value="">'.__('Staff','pregnancy-addon').'</option>';
										if($staffids != '') {
											$staffids_arr = explode(',', $staffids);
											$cp_staffs = get_posts( array('post_type'=>'dt_staffs', 'posts_per_page'=>'-1', 'post__in' => $staffids_arr ) );
										} else {
											$cp_staffs = get_posts( array('post_type'=>'dt_staffs', 'posts_per_page'=>'-1' ) );
										}
										if( $cp_staffs ){
											foreach( $cp_staffs as $cp_staff ){
												$id = $cp_staff->ID;
												$title = $cp_staff->post_title;
												$out .= '<option value="'.$id.'">'.$title.'</option>';
											}
										}
					$out .=    '</select>
							</div>
						</div>
					</div>';
		
           
        $out .='<div class="wpb_column vc_column_container vc_col-sm-12">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
            		
            				<div class="selection-box form-calender-icon"><input type="text" id="datepicker" name="date"  placeholder="'.__('Available On','pregnancy-addon').'"/></div>
        				</div>
					</div>
				</div>';

		$out .='<div class="wpb_column vc_column_container vc_col-sm-6">
					<div class="vc_column-inner ">
					   <div class="wpb_wrapper">
					    <div class="selection-box form-time-icon">
							<select name="start-time" class="start-time">';
		
							$time_format = get_option( 'time_format' );
		
							$out .= '<option value="" selected="selected">'.__('From','pregnancy-addon').'</option>';
							
							$out .= '<option value="00:00">'.__(date($time_format, strtotime('12:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="01:00">'.__(date($time_format, strtotime('1:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="02:00">'.__(date($time_format, strtotime('2:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="03:00">'.__(date($time_format, strtotime('3:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="04:00">'.__(date($time_format, strtotime('4:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="05:00">'.__(date($time_format, strtotime('5:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="06:00">'.__(date($time_format, strtotime('6:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="07:00">'.__(date($time_format, strtotime('7:00 am')),'pregnancy-addon').'</option>';
							
							$out .= '<option value="08:00">'.__(date($time_format, strtotime('8:00 am')),'pregnancy-addon').'</option>';
							
							$out .= '<option value="09:00">'.__(date($time_format, strtotime('9:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="10:00">'.__(date($time_format, strtotime('10:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="11:00">'.__(date($time_format, strtotime('11:00 am')),'pregnancy-addon').'</option>';
							$out .= '<option value="12:00">'.__(date($time_format, strtotime('12:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="13:00">'.__(date($time_format, strtotime('1:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="14:00">'.__(date($time_format, strtotime('2:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="15:00">'.__(date($time_format, strtotime('3:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="16:00">'.__(date($time_format, strtotime('4:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="17:00">'.__(date($time_format, strtotime('5:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="18:00">'.__(date($time_format, strtotime('6:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="19:00">'.__(date($time_format, strtotime('7:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="20:00">'.__(date($time_format, strtotime('8:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="21:00">'.__(date($time_format, strtotime('9:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="22:00">'.__(date($time_format, strtotime('10:00 pm')),'pregnancy-addon').'</option>';
							$out .= '<option value="23:00">'.__(date($time_format, strtotime('11:00 pm')),'pregnancy-addon').'</option>';
							$out .= '</select> ';
						  
        $out .='  		</div>
						</div>
					</div>
			    </div>';
            
		$out .='<div class="wpb_column vc_column_container vc_col-sm-6">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">
						  <div class="selection-box form-time-icon">
						    <select name="end-time" class="end-time">';
						
						$out .= '<option value="" selected="selected">'.__('To','pregnancy-addon').'</option>';
						
						$out .= '<option value="09:00">'.__(date($time_format, strtotime('9:00 am')),'pregnancy-addon').'</option>';
						$out .= '<option value="10:00">'.__(date($time_format, strtotime('10:00 am')),'pregnancy-addon').'</option>';
						$out .= '<option value="11:00">'.__(date($time_format, strtotime('11:00 am')),'pregnancy-addon').'</option>';
						$out .= '<option value="12:00">'.__(date($time_format, strtotime('12:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="13:00">'.__(date($time_format, strtotime('1:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="14:00">'.__(date($time_format, strtotime('2:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="15:00">'.__(date($time_format, strtotime('3:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="16:00">'.__(date($time_format, strtotime('4:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="17:00">'.__(date($time_format, strtotime('5:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="18:00">'.__(date($time_format, strtotime('6:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="19:00">'.__(date($time_format, strtotime('7:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="20:00">'.__(date($time_format, strtotime('8:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="21:00">'.__(date($time_format, strtotime('9:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="22:00">'.__(date($time_format, strtotime('10:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="23:00">'.__(date($time_format, strtotime('11:00 pm')),'pregnancy-addon').'</option>';
						$out .= '<option value="23:59">'.__(date($time_format, strtotime('12:00 am')),'pregnancy-addon').'</option>';
						$out .= '</select> ';
		    $out .=' 	 </div>
						</div>
					</div>
				</div>';
			
			
		$out .= '<div class="wpb_column vc_column_container vc_col-sm-12">
					<div class="vc_column-inner ">
						<div class="wpb_wrapper">	
							<button class="dt-sc-button fullwidth-button small show-time-shortcode" value="'.__('Show Time', 'pregnancy-addon').'" type="submit">'.__('Check Availability', 'pregnancy-addon').'</button>
						</div>
					</div>
				</div>	';
			
		$out .= '<input type="hidden" id="staffids" name="staffids" value="'.$staffids.'" />
				 <input type="hidden" id="serviceids" name="serviceids" value="'.$serviceids.'" />';
		
		$out .='</div>';
		$out .= '</form>';

		} else {
				$out .= '<div class="dt-sc-info-box">'.__('Please create Reservation Type1 template page in order to make this shortcode work properly!', 'pregnancy-addon').'</div>';		
			}
		return $out;		
	
	}

	/**
	 * Video section
	 * @return string
	 */
	function dt_sc_video_section($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'title' => '',
			'video_url' => '',
			'image' => '',
			'video_type' => 'type1'
			
		), $attrs ) );

		$image = wpb_getImageBySize( array( 'attach_id' => $image, 'thumb_size' => 'full' ));
		$video_image = $image['p_img_large'][0];
		$image = $image['thumbnail'];
		
		
		if($video_type == 'type1'){
			$out = '<div class="ilightbox">';
			$out .=		$image;
			$out .= 	'<div class="video-overlay">
							<a class="play-button" data-type="iframe" id="fullscreen-video-section-ilightbox" data-options="width:800, height:450" href='.$video_url.'>				
								<i class="fa fa-play"></i>						
							</a>
							<h5>'.$title.'</h5>
						</div>';
						
				
			$out .= '</div>';
		}else{
			$out = '<img class="alignnone" src='.$video_image.' alt="video-thumb">
					<a class="play-button" data-type="iframe" id="fullscreen-video-section-ilightbox" data-options="width:800, height:450" href='.$video_url.'> 
						<i class="fa fa-play"></i>
					</a>';
		}

		return $out;
	}
	
	//Current doctor info
	function dt_sc_doctor_item( $attrs, $content = null ) {

		extract ( shortcode_atts ( array (
			'id' => '',
			'class' => '',
			'type'	=>	''
		), $attrs ) );

		$out = "";

		$hover = true;

		if( empty($id) ) {
			global $post;
			$id =  $post->ID;
			$hover = false;
		}

		$p = get_post( $id );

		if( $p->post_type === "dt_doctors" ) {
			$id = $p->ID;
			$title = get_the_title($id);
			$permalink = get_permalink($id);
			$terms =  get_the_terms( $id, 'doctor_departments' );

			$image =  has_post_thumbnail($id) ? get_the_post_thumbnail($id,'full') : '<img src="http://placehold.it/370X450.jpg&text='.esc_html($title).'" alt="'.esc_attr($title).'" title="'.esc_attr($title).'"/>';

			$departments = '';
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				$count = count( $terms );
				$i = 0;

				foreach ( $terms as $term ) {
					$i++;
					$departments .=  $term->name;

					if ( $count != $i ) {
						$departments .= ', ';
					}
				}
			}

			$settings = get_post_meta ( $id, '_custom_settings', TRUE );
			$settings = is_array ( $settings ) ? $settings : array ();

			$social = array_key_exists("social",$settings) ? $settings['social'] : '';
			if( !empty($social) ){
				$social = str_replace('[dt_sc_social', '[dt_sc_social class="dt-sc-sociable" ', $social);
				$social = do_shortcode($social);
			}
			
			$button = array_key_exists("button",$settings) ? $settings['button'] : '';
			if( !empty($button) ){
				$button = str_replace('[dt_sc_button', '[dt_sc_button class="fully-rounded-border fullwidth-button" ', $button);
				$button = do_shortcode($button);
			}

			if( empty($type) || $type == 'style-1' ) {

				$out .= '<div id="dt_doctors-'.esc_attr($id).'" class="dt-sc-gynecologist-single dt_doctors type-dt_doctors '.esc_attr($class).'">';
				$out .= '  <div class="dt-sc-gynecologist-wrapper">';
				$out .= '	<div class="dt-sc-gynecologist-thumb">';
				$out .= '		<a href="'.esc_url($permalink).'">';
				$out .= 			$image;
				$out .= '		</a>';
				$out .= '	</div>';
				$out .=     $button;
				$out .= '  </div>';
				$out .= '  <div class="dt-sc-gynecologist-single-details">';
								$title = array_key_exists("prefix",$settings) ? $settings['prefix'].$title : $title;
								$postfix = array_key_exists("postfix",$settings) ? $settings['postfix'] : '';

				$out .= '		<h4><a href="'.esc_url($permalink).'">'.esc_html($title).'</a><span>'.esc_html($postfix).'</span></h4>';
				$out .= '		<h5>'.esc_html($departments).'</h5>';
				$out .= 		$social.'<div class="dt-sc-hr-invisible-xsmall "> </div>';
			   if( array_key_exists('meta_title', $settings) ) {

				$out .= '		<ul class="dt-sc-gynecologist-single-meta">';
									foreach( $settings['meta_title'] as $key => $title ){
										$value = $settings['meta_value'][$key];
				
										if( filter_var($value ,FILTER_VALIDATE_URL) ){
											$value = "<a href='".esc_url($value)."'>".esc_html($value)."</a>";
										} elseif( is_email($value) ){
											$email = sanitize_email($value);
											$value = "<a href='mailto:".antispambot($email,1)."'>".antispambot($value)."</a>";
										}
				
										if( !empty($value) ) {
											$out .= '<li> <span>'.esc_html($title).'</span><div class="dt-sc-gynecologist-single-meta-data">'.$value.'</div></li>';
										}
									}
				$out .= '		</ul>';
			   }
				
				$out .= '  </div>';
				$out .= '</div>';
			} else {
				$out .= '<div class="dt-sc-team dt-sc-team-member  with-qualification">';
				$out .= '	<div class="dt-sc-team-thumb">';
				$out .= 		esc_html($image);
				$out .= '		<div class="team-overlay">
									<a href="#">View Details</a>
								</div>
							</div>';
				$out .= '	<div class="dt-sc-team-details">';
				$out .= '		<h4>'.esc_html($title).'<span>'.$postfix.'</span></h4>';
				$out .='		<h5>'.$departments.'</h5>';
				$out .= 		$social;
				$out .= '	</div>';
				$out .= '</div>';
			}
		}
		
		return $out;
	}
	
	
	#Doctors With Filter
	function dt_sc_doctors_with_filter( $attrs, $content = null ){

		extract ( shortcode_atts ( array (
			'column' =>	'4',
			'show_doc_category_filter' => 'no'
		), $attrs ) );

		$columnclass = '';
		$out ='';

		switch( $column ) {

			case '2':
				$columnclass = 'column dt-sc-one-half';
			break;

			case '3':
				$columnclass = 'column dt-sc-one-third';
			break;
			
			case '4':
				$columnclass = 'column dt-sc-one-fourth';
			break;
		}

		if($show_doc_category_filter == 'yes'){
			$out .= '	<div class="column dt-sc-one-fourth first">';
			$out .= '			<select name="department-filter">';
			$out .= '				<option value="0">'.esc_html__('All','pregnancy-addon').'</option>';
									$terms = get_terms('doctor_departments', array( 'fields' => 'id=>name'));
									foreach ( $terms as $id => $term ) {
										$out .= '<option value="'.esc_attr($id).'">'.esc_html($term).'</option>';
									}
			$out .= '			</select>';
			$out .= '	</div>';
			
			$out .='	<div class="column dt-sc-three-fourth">';
		}
		
		$out .= '		  <div class="dt-sc-doctors-sorting">';
							$out .= '<a href="#"> All </a>';
							$alphabets = array( esc_html__('A','pregnancy-addon'), esc_html__('B','pregnancy-addon'), esc_html__('C','pregnancy-addon'), 
							
								esc_html__('D','pregnancy-addon'), esc_html__('E','pregnancy-addon'), esc_html__('F','pregnancy-addon'), esc_html__('G','pregnancy-addon'), 
								
								esc_html__('H','pregnancy-addon'), esc_html__('I','pregnancy-addon'), esc_html__('J','pregnancy-addon'), esc_html__('K','pregnancy-addon'), 
								
								esc_html__('L','pregnancy-addon'), esc_html__('M','pregnancy-addon'), esc_html__('N','pregnancy-addon'), esc_html__('O','pregnancy-addon'), 
								
								esc_html__('P','pregnancy-addon'), esc_html__('Q','pregnancy-addon'), esc_html__('R','pregnancy-addon'), esc_html__('S','pregnancy-addon'), 
								
								esc_html__('T','pregnancy-addon'), esc_html__('U','pregnancy-addon'), esc_html__('V','pregnancy-addon'), esc_html__('W','pregnancy-addon'), 
								
								esc_html__('X','pregnancy-addon'), esc_html__('Y','pregnancy-addon'),esc_html__('Z','pregnancy-addon') );
								
							foreach( $alphabets as $key => $alphabet ) {
								$class = ( $key == 0 ) ? ' class="active-sort" ':'';
								$out .= '<a href="#" '.$class.'>'.$alphabet.'</a>';
							}
		$out .= '		</div>';
		
		if($show_doc_category_filter == 'yes'){
			$out .='	</div>';
		}
		
		$out .= '<div class="dt-sc-doctors-container" data-column="'.esc_attr($column).'">';

					$wp_query = new WP_Query();

					$doctors = array(
						'post_type'=>'dt_doctors',
						'posts_per_page'=>'-1',
						'suppress_filters' => false,
						'order_by'=> 'published');

					$doctors['search_doctor_title'] = esc_html__('A','pregnancy-addon');
					add_filter( 'posts_where', array( $this, 'doctor_title_filter' ), 10, 2 );


					$wp_query->query( $doctors );

					if( $wp_query->have_posts() ) {

						$i = 1;

						while( $wp_query->have_posts() ) {

							$wp_query->the_post();
							$the_id = get_the_ID();

							$temp_class = $columnclass;
							if($i == 1){
								$temp_class .= " first";
							}

							if($i == $column) $i = 1; else $i = $i + 1;
							
							$out .= '<div class="'.esc_attr($temp_class).'">';
							$sc   = '[dt_sc_doctors_info id="'.$the_id.'" type=""/]';
							$out .= do_shortcode($sc);
							$out .= '</div>';
						}
					} else {
						$out .= '<div class="column dt-sc-one-column">';
						$out .= '<h2>'.esc_html__("Nothing Found.", "pregnancy-addon").'</h2>';
						$out .= '<p>'.esc_html__("Apologies, but no results were found for the request.", "pregnancy-addon").'</p>';
						$out .= '</div>';
					}
		$out .= '</div>';

		return $out;
	}
	
	function doctor_title_filter( $where, &$wp_query ){

		global $wpdb;
		if ( $search_term = $wp_query->get( 'search_doctor_title' ) ) {						
			$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \''. esc_sql( like_escape( $search_term ) ) . '%\'';
		}

		return $where;
    }
	
	
	/* Ajax call */
    function dt_sc_filter_doctors() {

    	$out = $columnclass = '';

    	$data = $_REQUEST['data'];
    	$data = array_filter($data);

    	$column = array_key_exists( 'column', $data ) ? $data['column'] : '4';
    	switch( $column ) {

			case '2':
				$columnclass = 'column dt-sc-one-half';
			break;

			case '3':
				$columnclass = 'column dt-sc-one-third';
			break;
			
			case '4':
				$columnclass = 'column dt-sc-one-fourth';
			break;
		}


    	$wp_query = new WP_Query();
   		$doctors = array(
   			'post_type'=>'dt_doctors',
   			'posts_per_page'=>'-1',
   			'suppress_filters' => false,
   			'order_by'=> 'published');

    	if( array_key_exists('tax', $data ) ) {

    		$doctors['tax_query'][] = array( 'taxonomy' => 'doctor_departments',
    			'field' => 'id',
    			'terms' => (int) $data['tax'],
    			'operator' => 'IN');
    	}

    	if( array_key_exists('title', $data ) && ( trim($data['title']) !== 'All' ) ) {

    		$doctors['search_doctor_title'] = $data['title'];
    		add_filter( 'posts_where', array( $this, 'doctor_title_filter' ), 10, 2 );
    	}

		$wp_query->query( $doctors );
    	if( $wp_query->have_posts() ) {

    		$i = 1;

   			while( $wp_query->have_posts() ) {

   				$wp_query->the_post();
   				$the_id = get_the_ID();

   				$temp_class = $columnclass;
   				if($i == 1) {
   					$temp_class .= " first";
				}

				if($i == $column) $i = 1; else $i = $i + 1;
				$out .= '<div class="'.esc_attr($temp_class).'">';
   				$sc   = '[dt_sc_doctors_info id="'.$the_id.'" type=""/]';
   				$out .= do_shortcode($sc);
   				$out .= '</div>';
    		}
    	} else {
    		$out .= '<div class="column dt-sc-one-column">';
    		$out .= '<h2>'.esc_html__("Nothing Found.", "pregnancy-addon").'</h2>';
    		$out .= '<p>'.esc_html__("Apologies, but no results were found for the request.", "pregnancy-addon").'</p>';
    		$out .= '</div>';
    	}

    	echo !empty($out) ? $out : '';
    	die();
    }
	//doctor filter end
	
	//doctors info
	function dt_sc_doctors_info( $attrs, $content = null ) {

		extract ( shortcode_atts ( array (
			'id' => '',
			'class' => '',
			'type'	=>	'',
			'title' => ''
		), $attrs ) );

		$out = "";

		$hover = true;

		if( empty($id) ) {
			global $post;
			$id =  $post->ID;
			$hover = false;
		}

		$p = get_post( $id );

		if( $p->post_type === "dt_doctors" ) {
			$id = $p->ID;
			$title = get_the_title($id);
			$permalink = get_permalink($id);
			$terms =  get_the_terms( $id, 'doctor_departments' );

			$image =  has_post_thumbnail($id) ? get_the_post_thumbnail($id,'full') : '<img src="http://placehold.it/370X450.jpg&text='.esc_html($title).'" alt="'.esc_attr($title).'" title="'.esc_attr($title).'"/>';

			$departments = '';
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				$count = count( $terms );
				$i = 0;

				foreach ( $terms as $term ) {
					$i++;
					$departments .=  $term->name;

					if ( $count != $i ) {
						$departments .= ', ';
					}
				}
			}

			$settings = get_post_meta ( $id, '_custom_settings', TRUE );
			$settings = is_array ( $settings ) ? $settings : array ();

			$social = array_key_exists("social",$settings) ? $settings['social'] : '';
			if( !empty($social) ){
				$social = str_replace('[dt_sc_social', '[dt_sc_social class="dt-sc-sociable" ', $social);
				$social = do_shortcode($social);
			}
			
			$button = array_key_exists("button",$settings) ? $settings['button'] : '';
			if( !empty($button) ){
				$button = str_replace('[dt_sc_button', '[dt_sc_button class="fully-rounded-border fullwidth-button" ', $button);
				$button = do_shortcode($button);
			}

			if( empty($type) || $type == 'style-1' ) {

					$title = array_key_exists("prefix",$settings) ? $settings['prefix'].$title : $title;
					$postfix = array_key_exists("postfix",$settings) ? $settings['postfix'] : '';
				$out .= '<div class="dt-sc-team dt-sc-team-member  with-qualification">';
				$out .= '	<div class="dt-sc-team-thumb">';
				$out .= 		$image;
				$out .= '		<div class="team-overlay">
									<a href="'.$permalink.'">'.esc_html__("View Details","pregnancy-addon").'</a>
								</div>
							</div>';
				$out .= '	<div class="dt-sc-team-details">';
				$out .= '		<h4>'.esc_html($title).'<span>'.$postfix.'</span></h4>';
				$out .='		<h5>'.$departments.'</h5>';
				$out .= 		$social;
				$out .= '	</div>';
				$out .= '</div>';
				
			} 
		}
		
		return $out;
	}
	
	/**
	 * Trimester
	 * @return string
	 */
	function dt_sc_trimester($attrs, $content = null) {
		extract ( shortcode_atts ( array (
			'stage_1_image' => '',
			'stage_1_week_count_1' => '4',
			'stage_1_week_count_1_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_1_week_count_2' => '9',
			'stage_1_week_count_2_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_1_week_count_3'  => '13',
			'stage_1_week_count_3_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_1_title' =>'',
			'stage_1_weight' =>'',
			
			'stage_2_image' => '',
			'stage_2_week_count_1' => '18',
			'stage_2_week_count_1_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_2_week_count_2' => '22',
			'stage_2_week_count_2_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_2_week_count_3'  => '27',
			'stage_2_week_count_3_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_2_title' =>'',
			'stage_2_weight' =>'',
			
			'stage_3_image' => '',
			'stage_3_week_count_1' => '31',
			'stage_3_week_count_1_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_3_week_count_2' => '36',
			'stage_3_week_count_2_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_3_week_count_3'  => '40',
			'stage_3_week_count_3_content' => base64_encode( '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed sagittis nisi. Curab itur eget sagittis dui. In dignissim mauris augue</p>'),
			'stage_3_title' => '',
			'stage_3_weight' => '',
		), $attrs ) );

		$stage_1_image = wpb_getImageBySize( array( 'attach_id' => $stage_1_image, 'thumb_size' => 'full' ));
		$stage_1_image = $stage_1_image['thumbnail'];
		
		$stage_2_image = wpb_getImageBySize( array( 'attach_id' => $stage_2_image, 'thumb_size' => 'full' ));
		$stage_2_image = $stage_2_image['thumbnail'];
		
		$stage_3_image = wpb_getImageBySize( array( 'attach_id' => $stage_3_image, 'thumb_size' => 'full' ));
		$stage_3_image = $stage_3_image['thumbnail'];
		
		
		$out = '<div class="trimester-time-line">';
		//Stage 1
		$out .=	   '<div class="column dt-sc-one-third no-space first">'.$stage_1_image;
		$out .=' 		<div class="week-count">
							<ul>
								<li>
									<a class="count-display" href="#">'.$stage_1_week_count_1.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_1_week_count_1_content ) ) ).'</div>
								</li>
								<li>
									<a class="count-display" href="#">'.$stage_1_week_count_2.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_1_week_count_2_content ) ) ).'</div>
								</li>
								<li>
									<a class="count-display" href="#">'.$stage_1_week_count_3.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_1_week_count_3_content ) ) ).'</div>
								</li>
							</ul>
							<div class="kilo-by-week pista">
								<h3>'.$stage_1_title.'</h3>
								<div class="indication-line"></div>
								<h5>'.$stage_1_weight.'</h5>
							</div>
						</div>
					</div>';
			
		//Stage 2
		$out .=	   '<div class="column dt-sc-one-third no-space">'.$stage_2_image;
		$out .=' 		<div class="week-count">
							<ul>
								<li>
									<a class="count-display" href="#">'.$stage_2_week_count_1.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_2_week_count_1_content ) ) ).'</div>
								</li>
								<li>
									<a class="count-display" href="#">'.$stage_2_week_count_2.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_2_week_count_2_content ) ) ).'</div>
								</li>
								<li>
									<a class="count-display" href="#">'.$stage_2_week_count_3.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_2_week_count_3_content ) ) ).'</div>
								</li>
							</ul>
							<div class="kilo-by-week blue">
								<h3>'.$stage_2_title.'</h3>
								<div class="indication-line"></div>
								<h5>'.$stage_2_weight.'</h5>
							</div>
						</div>
					</div>';
					
		//Stage 3
		$out .=	   '<div class="column dt-sc-one-third no-space">'.$stage_3_image;
		$out .=' 		<div class="week-count">
							<ul>
								<li>
									<a class="count-display" href="#">'.$stage_3_week_count_1.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_3_week_count_1_content ) ) ).'</div>
								</li>
								<li>
									<a class="count-display" href="#">'.$stage_3_week_count_2.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_3_week_count_2_content ) ) ).'</div>
								</li>
								<li>
									<a class="count-display" href="#">'.$stage_3_week_count_3.'</a>
									<div class="week-count-overlay">'.rawurldecode( base64_decode( strip_tags( $stage_3_week_count_3_content ) ) ).'</div>
								</li>
							</ul>
							
							<span class="units-weeks">'.__("*units in weeks","pregnancy-addon").' </span>
							
							<div class="kilo-by-week pink">
								<h3>'.$stage_3_title.'</h3>
								<div class="indication-line"></div>
								<h5>'.$stage_3_weight.'</h5>
							</div>
						</div>
					</div>';

		$out .= '</div>';

		return $out;
	}
	
	/**
	 * Single Product
	 * @return string
	 */
	function dt_sc_single_product($attrs, $content = null) {
		extract(shortcode_atts(array(
			'product_id' => '',
			'excerpt_length' => ''
		), $attrs));

		$out = "";

		#Performing query...
		$args = array('post_type' => 'product' , 'p' => $product_id,
			);

		$the_query = new WP_Query($args);
		if($the_query->have_posts()):

				while($the_query->have_posts()): $the_query->the_post();
					$PID = $product_id;

					$out .= '<div class="woo_product_desc">';
					$out .= '	<h2><a href="'.esc_url(get_permalink()).'" title="'.esc_attr(get_the_title()).'">'.esc_html(get_the_title()).'</a></h2>';
					$out .= 		pregnancy_excerpt($excerpt_length);

									if(get_post_meta(get_the_ID(), '_sale_price', true)):
										ob_start();
										woocommerce_template_loop_price();
										$price = ob_get_clean();
										
										$out .= $price;
									endif;
							
					$add_to_cart_url = do_shortcode('[add_to_cart_url id="'.$product_id.'"]');
					
					$out .= 		do_shortcode('[add_to_cart id="'.$product_id.'" show_price="false"]');
					$out .= '</div>';
				endwhile;
		wp_reset_postdata();
		else:
			$out .= '<h2>'.esc_html__("Nothing Found.", "pregnancy-addon").'</h2>';
			$out .= '<p>'.esc_html__("Apologies, but no results were found for the requested archive.", "pregnancy-addon").'</p>';
		endif;

		return $out;
	}
	
	/**
	 * Featured products carousel
	 * @return string
	 */
	function dt_sc_featured_product_carousel($attrs, $content = null) {
		extract(shortcode_atts(array(
			'featured_prd_type' => 'type1',
			'title' => '',
			'content' => '<p>Duas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>',
			'column' => 'featured-prd-3-cols',
			'scroll' => '3',
			'visible' => '3'
		), $attrs));

		$out = "";

		#Performing query...
		$out .= '<div class="dt-sc-partners-carousel-wrapper dt-sc-products-wrapper '.$featured_prd_type.'" data-scroll="'.esc_attr($scroll).'" data-visible="'.esc_attr($visible).'">';
		
		if($featured_prd_type == "type1"){
			$out .= '	<div class="dt-sc-one-fourth column no-space first">
							<h2>'.$title.'</h2>'.$content;
			$out .= '	</div>';
			$out .= '	<div class="dt-sc-three-fourth column no-space">';
		}
			$out .= '	  <ul class="dt-sc-partners-carousel dt-sc-product-carousel products">';
				
						  $meta_query   = WC()->query->get_meta_query();
						  $meta_query[] = array(
							  'key'   => '_featured',
							  'value' => 'yes'
						  );
						  $args = array(
							  'post_type'   =>  'product',
							  'post_status' =>  'publish',
							  'showposts'   =>  -1,
							  'orderby'     =>  'date',
							  'order'       =>  'DESC',
							  'meta_query'  =>  $meta_query
						  );
	
						  $the_query = new WP_Query($args);
						  
						  if($the_query->have_posts()):
					
							while($the_query->have_posts()): $the_query->the_post();
								
								$product = get_product( $the_query->post->ID );
								$the_id = get_the_ID();
			
								$out .= '<li class="product">
											<div class="product-wrap product-thumb">
												<div class="product-image">
													<a href="'.esc_url(get_permalink()).'" title="'.esc_attr(get_the_title()).'">';
														if(has_post_thumbnail()):
															$attr = array('title' => get_the_title(), 'alt' => get_the_title());
															$out .= get_the_post_thumbnail($the_id, 'full', $attr);
														else:
															$out .= '<img src="http://place-hold.it/500x351&text='.esc_attr(get_the_title()).'" alt="'.esc_attr(get_the_title()).'" title="'.esc_attr(get_the_title()).'"/>';
														endif;
								$out .= '			</a>
												</div>
												<div class="product-detail-content">
													<h5><a href="'.esc_url(get_permalink()).'" title="'.esc_attr(get_the_title()).'">'.esc_html(get_the_title()).'</a></h5>
													<a class="view-detail-btn" href="'.esc_url(get_permalink()).'">'.esc_html__("View Details","pregnancy").'</a>
												</div>							
											</div>
										 </li>';
									
								
							endwhile;
							wp_reset_postdata();
						  else:
							$out .= '<h2>'.esc_html__("Nothing Found.", "pregnancy-addon").'</h2>';
							$out .= '<p>'.esc_html__("Apologies, but no results were found for the requested archive.", "pregnancy-addon").'</p>';
						  endif;
						  
			$out .= '	   </ul>';
			if($featured_prd_type == "type1"){ $out .= '</div>'; }
			$out .= '	 <div class="carousel-arrows">
							<a class="partners-prev prev-btn" href="">
								<span class="fa fa-chevron-left"></span>
							</a>
							<a class="partners-next next-btn" href="">
								<span class="fa fa-chevron-right"></span>
							</a>
						 </div>';
		$out .= '</div>';

		return $out;
	}

}?>