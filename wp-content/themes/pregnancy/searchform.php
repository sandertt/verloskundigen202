<!-- **Searchform** -->
<?php $search_text = empty($_GET['s']) ? esc_html__("Enter Keyword",'pregnancy') : get_search_query(); ?>
<button type="button" class="close">x</button>
<form method="get" id="searchformm" action="<?php echo esc_url( home_url('/') );?>">
    <input id="s" name="s" type="text" 
         	value="<?php echo esc_attr( $search_text );?>" class="text_input"
		    onblur="if(this.value==''){this.value='<?php echo esc_attr($search_text);?>';}"
            onfocus="if(this.value =='<?php echo esc_attr($search_text);?>') {this.value=''; }" />
	<input name="submit" type="submit"  value="<?php esc_attr_e('Go','pregnancy');?>" />
</form>
<!-- **Searchform - End** -->