<?php
/* ---------------------------------------------------------------------------
 * Default Color Styles
 * --------------------------------------------------------------------------- */

if ( ! defined( 'ABSPATH' ) ) exit; ?>

/*----*****---- Font Size ----*****----*/
body { font-size:18px; line-height:30px; }

h1, h2, h3, h4, h5, h6 { letter-spacing:0.5px; }	

h1{ font-size:40px; }
h2{ font-size:30px; }
h3{ font-size:28px; }
h4{ font-size:24px; }
h5{ font-size:22px; }
h6{ font-size:18px; }
	
/************** BPanel Options **************/
body, .layout-boxed .inner-wrapper { background-color:<?php pregnancy_opts_show('body-bgcolor','#FFF');?>;}

<?php
$skin = pregnancy_option('colors','theme-skin');
# When Choosing Custom Skin...
if($skin == 'custom'): ?>
    
    
    #main-menu ul.menu > li > a:hover, #main-menu ul li.menu-item-megamenu-parent:hover > a, #main-menu ul > li.menu-item-simple-parent:hover > a { color:<?php pregnancy_opts_show('menu-hovercolor', pregnancy_opts_get('custom-default', ''));?>; }
    #main-menu > ul.menu > li.current_page_item > a, #main-menu > ul.menu > li.current_page_ancestor > a, #main-menu > ul.menu > li.current-menu-item > a, #main-menu > ul.menu > li.current-menu-ancestor > a, .left-header #main-menu > ul.menu > li.current_page_item > a, .left-header #main-menu > ul.menu > li.current_page_ancestor > a, .left-header #main-menu > ul.menu > li.current-menu-item > a, .left-header #main-menu > ul.menu > li.current-menu-ancestor > a { color:<?php pregnancy_opts_show('menu-activecolor', pregnancy_opts_get('custom-default', ''));?>; }

	#footer a:hover, #footer .dt-sc-dark-bg a:hover, #footer .widget ul li:hover:before { color:<?php pregnancy_opts_show('footer-link-hcolor', pregnancy_opts_get('custom-default', '#0d47a1'));?>; }<?php
# When choosing predefined Skins...
else: ?>
    
    #footer .widgettitle{ color:<?php pregnancy_opts_show('footer-heading-color', '');?>; }
    
    .footer-widgets a, #footer a{ color:<?php pregnancy_opts_show('footer-link-color', '');?>; }
    
    #footer{ color:<?php pregnancy_opts_show('footer-text-color', '');?>; }
    
    .footer-widgets{ background-color:<?php pregnancy_opts_show('footer-bgcolor', '');?>; }
    
    .footer-copyright{ background-color:<?php pregnancy_opts_show('copyright-bgcolor', '');?>; }
    
    <?php $footer_bg = pregnancy_option('layout','footer-bg'); 
	if( !empty( $footer_bg ) ) { ?>
    
    .footer-widgets {background: url( <?php pregnancy_opts_show('footer-bg', '');?> ) <?php pregnancy_opts_show('footer-bg-position', 'center center')?> <?php pregnancy_opts_show('footer-bg-repeat', 'no-repeat');?>}
    
    <?php } ?>
    
    #main-menu > ul.menu > li.current_page_item > a, #main-menu > ul.menu > li.current_page_ancestor > a, #main-menu > ul.menu > li.current-menu-item > a, #main-menu > ul.menu > li.current-menu-ancestor > a, .left-header #main-menu > ul.menu > li.current_page_item > a, .left-header #main-menu > ul.menu > li.current_page_ancestor > a, .left-header #main-menu > ul.menu > li.current-menu-item > a, .left-header #main-menu > ul.menu > li.current-menu-ancestor > a { color:<?php pregnancy_opts_show('menu-activecolor', pregnancy_opts_get('custom-default', ''));?>; }
	
	#footer .dt-sc-dark-bg a:hover,#footer a:hover{ color:<?php pregnancy_opts_show('footer-link-hcolor', '');?>; }<?php

endif;?>

/*----*****---- Header  ----*****----*/
<?php
$htype = pregnancy_option('layout','header-type');
$hcolor = pregnancy_option('colors','header-bgcolor');
if( isset($htype) && ($htype == 'boxed-header') && isset($hcolor) && ($hcolor != '')): ?>
	.main-header, .boxed-header.semi-transparent-header .main-header { background: rgba(<?php $rgbcolor = pregnancy_hex2rgb(pregnancy_opts_get('header-bgcolor', '')); $rgbcolor = implode(',', $rgbcolor); echo !empty($rgbcolor) ? $rgbcolor : ''; ?>, <?php pregnancy_opts_show('header-bgcolor-opacity', '1');?>); }<?php
elseif( isset($hcolor) && ($hcolor != '') ):?>
	.main-header-wrapper, .fullwidth-header.semi-transparent-header .main-header-wrapper { background: rgba(<?php $rgbcolor = pregnancy_hex2rgb(pregnancy_opts_get('header-bgcolor', '')); $rgbcolor = implode(',', $rgbcolor); echo !empty($rgbcolor) ? $rgbcolor : ''; ?>, <?php pregnancy_opts_show('header-bgcolor-opacity', '1');?>); }<?php
endif;

$headbg = pregnancy_option('layout','header-bg');
$bgrepeat = pregnancy_opts_get('header-bg-repeat', 'no-repeat');
$bgposition = pregnancy_opts_get('header-bg-position', 'center center');
if( !empty( $headbg) ) {?>
	#main-header-wrapper { background-image: url('<?php echo !empty($headbg) ? $headbg : '';?>'); background-repeat: <?php echo !empty($bgrepeat) ? $bgrepeat : '';?>; background-position: <?php echo !empty($bgposition) ? $bgposition : '';?>; }<?php
}?>

/*----*****---- Menu  ----*****----*/
<?php
$mbg = pregnancy_option('colors','menu-bgcolor');
if( isset($mbg) ): ?>
.menu-wrapper {  background: rgba(<?php $rgbcolor = pregnancy_hex2rgb(pregnancy_opts_get('menu-bgcolor', '')); $rgbcolor = implode(',', $rgbcolor); echo !empty($rgbcolor) ? $rgbcolor : ''; ?>, <?php pregnancy_opts_show('menu-bgcolor-opacity', '1');?>); }<?php
endif; ?>

#main-menu ul.menu > li > a { color:<?php pregnancy_opts_show('menu-linkcolor','#000000');?>; }

.menu-active-highlight #main-menu > ul.menu > li.current_page_item > a, .menu-active-highlight #main-menu > ul.menu > li.current_page_ancestor > a, .menu-active-highlight #main-menu > ul.menu > li.current-menu-item > a, .menu-active-highlight #main-menu > ul.menu > li.current-menu-ancestor > a { color:<?php pregnancy_opts_show('menu-activecolor', '#ffffff');?>; }

/*----*****---- Content  ----*****----*/
<?php
$ccolor = pregnancy_option('colors','content-text-color');
if( isset($ccolor) ): ?>
	body { color:<?php pregnancy_opts_show('content-text-color', '');?>; }<?php
endif;
$ccolor = pregnancy_option('colors','content-link-color');
if( isset($ccolor) ): ?>
	a { color:<?php pregnancy_opts_show('content-link-color', '');?>; }<?php
endif;
$ccolor = pregnancy_option('colors','content-link-hcolor');
if( isset($ccolor) ): ?>
	a:hover { color:<?php pregnancy_opts_show('content-link-hcolor', '');?>; }<?php
endif;?>

/*----*****---- Heading  ----*****----*/
<?php
for($i = 1; $i <= 6; $i++):
	$hcolor = pregnancy_option("colors","heading-h{$i}-color");
	if( isset($hcolor) ):
		echo "h{$i} { color: ";
			pregnancy_opts_show("heading-h{$i}-color", "");
		echo "; }\n";	
	endif;
endfor;?>

/*----*****---- Footer ----*****----*/
<?php
$darkbg = pregnancy_option('layout','footer-darkbg');
if( isset($darkbg) ): ?>
	.footer-widgets.dt-sc-dark-bg, #footer .dt-sc-dark-bg, .footer-copyright.dt-sc-dark-bg{ color:<?php pregnancy_opts_show('footer-text-color', 'rgba(240,240,240, 1)');?>; }
	.footer-widgets.dt-sc-dark-bg a, #footer .dt-sc-dark-bg a{ color:<?php pregnancy_opts_show('footer-link-color', 'rgba(255, 255, 255, 0.6)');?>; }
	#footer .dt-sc-dark-bg h3, #footer .dt-sc-dark-bg h3 a { color:<?php pregnancy_opts_show('footer-heading-color', '#ffffff');?>; }<?php
else: ?>
	
	<?php
endif;?>

/*----*****---- Megamenu ----*****----*/
<?php
# Border,Border radius
$applymenuborder = pregnancy_option('layout','menu-border');
if( isset( $applymenuborder ) ):
	$borderstyle = pregnancy_option('layout','menu-border-style');
	$bordercolor = pregnancy_option('layout','menu-border-color');
	
	$bwtop = pregnancy_option('layout','menu-border-width-top');
	$bwright = pregnancy_option('layout','menu-border-width-right');
	$bwbottom = pregnancy_option('layout','menu-border-width-bottom');
	$bwleft = pregnancy_option('layout','menu-border-width-left');
	
	$brtop = pregnancy_option('layout','menu-border-radius-top');
	$brright = pregnancy_option('layout','menu-border-radius-right');
	$brbottom = pregnancy_option('layout','menu-border-radius-bottom');
	$brleft = pregnancy_option('layout','menu-border-radius-left');?>
    
    #main-menu ul li.menu-item-simple-parent ul, #main-menu .megamenu-child-container {
    	border-style:<?php echo !empty($borderstyle) ? $borderstyle : '';?>;
        border-color:<?php echo !empty($bordercolor) ? $bordercolor : '';?>;        
		<?php if( isset( $bwtop ) ) ?>
        	border-top-width:<?php echo !empty($bwtop) ? $bwtop : '';?>px;        
		<?php if( isset( $bwright ) ) ?>
    		border-right-width:<?php echo !empty($bwright) ? $bwright : '';?>px;        
        <?php if( isset( $bwbottom ) ) ?>
    		border-bottom-width:<?php echo !empty($bwbottom) ? $bwbottom : '';?>px;
        <?php if( isset( $bwleft ) ) ?>
        	border-left-width:<?php echo !empty($bwleft) ? $bwleft : '';?>px;
        <?php if( isset( $brtop ) ) ?>
        	border-top-left-radius:<?php echo !empty($brtop) ? $brtop : '';?>px;    
        <?php if( isset( $brright ) ) ?>
    		border-top-right-radius:<?php echo !empty($brright) ? $brright : '';?>px;        
    	<?php if( isset( $brbottom ) ) ?>
    		border-bottom-right-radius:<?php echo !empty($brbottom) ? $brbottom : '';?>px;        
    	<?php if( isset( $brleft ) ) ?>
    		border-bottom-left-radius:<?php echo !empty($brleft) ? $brleft : '';?>px;
	}
<?php
endif;
# Mega Menu Container BG Color
$menubgcolor = pregnancy_option('layout','menu-bg-color');
if( isset( $menubgcolor ) ):?>
	#main-menu ul li.menu-item-simple-parent ul, #main-menu .megamenu-child-container { background-color:<?php echo !empty($menubgcolor) ? $menubgcolor : '';?>;}<?php
endif;

# Mega Menu Container gradient
$menugrc1 =  pregnancy_option('layout','menu-gradient-color1');
$menugrc2 =  pregnancy_option('layout','menu-gradient-color2');

if( isset($menugrc1) && isset($menugrc2) ) {
	
	$p1 = (pregnancy_option('layout','menu-gradient-percent1') != NULL) ? pregnancy_option('layout','menu-gradient-percent1') : "0%";
	$p2 = (pregnancy_option('layout','menu-gradient-percent2') != NULL) ? pregnancy_option('layout','menu-gradient-percent2') : "100%";?>
    #main-menu ul li.menu-item-simple-parent ul, #main-menu .megamenu-child-container {
		background: <?php echo !empty($menugrc1) ? $menugrc1 : ''; ?>; /* Old browsers */
		background: -moz-linear-gradient(top, <?php echo !empty($menugrc1) ? $menugrc1.' '.$p1.', '.$menugrc2.' '.$p2 : ''; ?>); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, <?php echo !empty($menugrc1) ? $menugrc1.' '.$p1.', '.$menugrc2.' '.$p2 : ''; ?>); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, <?php echo !empty($menugrc1) ? $menugrc1.' '.$p1.', '.$menugrc2.' '.$p2 : ''; ?>); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo !empty($menugrc1) ? $menugrc1 : ''; ?>', endColorstr='<?php echo !empty($menugrc2) ? $menugrc2 : ''; ?>',GradientType=0 ); /* IE6-9 */
	}
    <?php  
}

# Default Menu Title text and hover color
$titletextdcolor = pregnancy_option('layout','menu-title-text-dcolor');
$titletextdhcolor = pregnancy_option('layout','menu-title-text-dhcolor');

if( isset( $titletextdcolor) )?>
	#main-menu .megamenu-child-container > ul.sub-menu > li > a, #main-menu .megamenu-child-container > ul.sub-menu > li > .nolink-menu { color:<?php echo !empty($titletextdcolor) ? $titletextdcolor : '';?>; }<?php
if( isset( $titletextdhcolor) )?>
	#main-menu .megamenu-child-container > ul.sub-menu > li > a:hover { color:<?php echo !empty($titletextdhcolor) ? $titletextdhcolor : '';?>; }
	#main-menu .megamenu-child-container > ul.sub-menu > li.current_page_item > a, #main-menu .megamenu-child-container > ul.sub-menu > li.current_page_ancestor > a, #main-menu .megamenu-child-container > ul.sub-menu > li.current-menu-item > a, #main-menu .megamenu-child-container > ul.sub-menu > li.current-menu-ancestor > a { color:<?php echo !empty($titletextdhcolor) ? $titletextdhcolor : '';?>; }<?php


# Menu Title Background
if( "true" == pregnancy_option('layout','menu-title-bg') ) :
	$menutitlebgcolor = pregnancy_option('layout','menu-title-bg-color');
	$bghovercolor = pregnancy_option('layout','menu-title-hoverbg-color');
	$menutitletxtcolor = pregnancy_option('layout','menu-title-text-color');
	$hovertxtcolor = pregnancy_option('layout','menu-title-hovertext-color');
	$menutitlebr = pregnancy_option('layout','menu-title-border-radius');?>
    #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > .nolink-menu {
    	<?php if( isset( $menutitlebgcolor ) )?>
        	background:<?php echo !empty($menutitlebgcolor) ? $menutitlebgcolor : '';?>;
        <?php if( isset( $menutitlebr ) ) ?>
        	border-radius:<?php echo !empty($menutitlebr) ? $menutitlebr : '';?>px;        
    }
    
    <?php if( isset($bghovercolor) ) {?>
    	#main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > a:hover { background:<?php echo !empty($bghovercolor) ? $bghovercolor : '';?>;}
		#main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current_page_item > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current_page_ancestor > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current-menu-item > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current-menu-ancestor > a { background:<?php echo !empty($bghovercolor) ? $bghovercolor : '';?>; }<?php
	}
	
	if( isset( $menutitletxtcolor ) ) {?>
    	#main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > .nolink-menu, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > a .menu-item-description { color:<?php echo !empty($menutitletxtcolor) ? $menutitletxtcolor : '';?>;}<?php
	}
	
	if( isset( $hovertxtcolor ) ) {?>
    	#main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > a:hover, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li > a:hover .menu-item-description { color:<?php echo !empty($hovertxtcolor) ? $hovertxtcolor : '';?>;}
		#main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current_page_item > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current_page_ancestor > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current-menu-item > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current-menu-ancestor > a, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current_page_item > a .menu-item-description
#main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current_page_ancestor > a .menu-item-description, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current-menu-item > a .menu-item-description, #main-menu .menu-item-megamenu-parent.menu-title-with-bg .megamenu-child-container > ul.sub-menu > li.current-menu-ancestor > a .menu-item-description { color:<?php echo !empty($hovertxtcolor) ? $hovertxtcolor : '';?>; }<?php
	}
endif;

#Menu Title With Border
$mtbwtop = pregnancy_option('layout','menu-title-border-width-top');
$mtbwright = pregnancy_option('layout','menu-title-border-width-right');
$mtbwbottom = pregnancy_option('layout','menu-title-border-width-bottom');
$mtbwleft = pregnancy_option('layout','menu-title-border-width-left');

if( isset($mtbwtop) || isset($mtbwright) || isset($mtbwbottom) || isset($mtbwleft) ) :

	$menutitlebrc = pregnancy_option('layout','menu-title-border-color');
	$menutitlebrs = pregnancy_option('layout','menu-title-border-style'); ?>
    #main-menu .menu-item-megamenu-parent .megamenu-child-container > ul.sub-menu > li > a, #main-menu .menu-item-megamenu-parent .megamenu-child-container > ul.sub-menu > li > .nolink-menu {
    	<?php if( isset( $mtbwtop ) ) : ?>
        		 border-top-width:<?php echo !empty($mtbwtop) ? $mtbwtop : ''; ?>px;
                 padding-top:10px;

    	<?php endif;
			  if( isset( $mtbwright ) ): ?>
        		 border-right-width:<?php echo !empty($mtbwright) ? $mtbwright : ''; ?>px;
                 padding-right:10px;

    	<?php endif;
			  if( isset( $mtbwbottom ) ): ?>
        		 border-bottom-width:<?php echo !empty($mtbwbottom) ? $mtbwbottom : ''; ?>px;
                 padding-bottom:10px;

    	<?php endif;
			  if( isset( $mtbwleft ) ): ?>
        		 border-left-width:<?php echo !empty($mtbwleft) ? $mtbwleft : ''; ?>px;
                 padding-left:10px;       
    	
        <?php endif;
		     if( isset( $menutitlebrs ) ) ?>
        	 	border-style:<?php echo !empty($menutitlebrs) ? $menutitlebrs : '';?>;
        <?php if( isset( $menutitlebrc ) ) ?>
        		 border-color:<?php echo !empty($menutitlebrc) ? $menutitlebrc : '';?>;
   }<?php	
endif;

# Default text and hover color
$textdcolor = pregnancy_option('layout','menu-link-text-dcolor');
$textdhcolor = pregnancy_option('layout','menu-link-text-dhcolor');

if( isset( $textdcolor) )?>
	#main-menu .megamenu-child-container ul.sub-menu > li > ul > li > a, #main-menu ul li.menu-item-simple-parent ul > li > a { color:<?php echo !empty($textdcolor) ? $textdcolor : '';?>; }<?php
if( isset( $textdhcolor) ) :?>
	#main-menu .megamenu-child-container ul.sub-menu > li > ul > li > a:hover, #main-menu ul li.menu-item-simple-parent ul > li > a:hover { color:<?php echo !empty($textdhcolor) ? $textdhcolor : '';?>; }
	#main-menu .megamenu-child-container ul.sub-menu > li > ul > li.current_page_item > a, #main-menu .megamenu-child-container ul.sub-menu > li > ul > li.current_page_ancestor > a, #main-menu .megamenu-child-container ul.sub-menu > li > ul > li.current-menu-item > a, #main-menu .megamenu-child-container ul.sub-menu > li > ul > li.current-menu-ancestor > a, #main-menu ul li.menu-item-simple-parent ul > li.current_page_item > a, #main-menu ul li.menu-item-simple-parent ul > li.current_page_ancestor > a, #main-menu ul li.menu-item-simple-parent ul > li.current-menu-item > a, #main-menu ul li.menu-item-simple-parent ul > li.current-menu-ancestor > a { color:<?php echo !empty($textdhcolor) ? $textdhcolor : '';?>; }<?php
endif;

# Menu Links Background
if( "true" == pregnancy_option('layout','menu-links-bg') ) :
	$menulinkbgcolor = pregnancy_option('layout','menu-link-bg-color');
	$menulinkbghovercolor = pregnancy_option('layout','menu-link-hoverbg-color');
	$menulinktxtcolor = pregnancy_option('layout','menu-link-text-color');
	$menulinkhovertxtcolor = pregnancy_option('layout','menu-link-hovertext-color');
	$menulinkbr = pregnancy_option('layout','menu-link-border-radius');
	echo "\n";?>    
    /* Menu Link */   
    #main-menu .menu-item-megamenu-parent.menu-links-with-bg .megamenu-child-container ul.sub-menu > li > ul > li > a, #main-menu ul li.menu-item-simple-parent.menu-links-with-bg ul > li > a {
    	<?php if( !is_null( $menulinkbgcolor ) || !empty( $menulinkbgcolor ) ):?>
        		background:<?php echo !empty($menulinkbgcolor) ? $menulinkbgcolor : '';?>;
        <?php endif;
			if( isset( $menulinkbr ) ) ?>
        	border-radius:<?php echo !empty($menulinkbr) ? $menulinkbr : '';?>px;
        <?php if(!is_null($menulinktxtcolor) || !empty( $menulinktxtcolor ) ): ?>
        	color:<?php echo !empty($menulinktxtcolor) ? $menulinktxtcolor : '';?>;
        <?php endif; ?>
    }
    /* Menu Link Hover */
    #main-menu .menu-item-megamenu-parent.menu-links-with-bg .megamenu-child-container ul.sub-menu > li > ul > li > a:hover, #main-menu ul li.menu-item-simple-parent.menu-links-with-bg ul > li > a:hover {
    	<?php if( !is_null( $menulinkbghovercolor ) || !empty( $menulinkbghovercolor ) ):?>
        		background:<?php echo !empty($menulinkbghovercolor) ? $menulinkbghovercolor : '';?>;
        <?php endif;
			if( !is_null( $menulinkhovertxtcolor ) || !empty( $menulinkhovertxtcolor ) ):?>
        	color:<?php echo !empty($menulinkhovertxtcolor) ? $menulinkhovertxtcolor : '';?>;
       <?php endif;?>
    }
	#main-menu .menu-item-megamenu-parent.menu-links-with-bg .megamenu-child-container ul.sub-menu > li > ul > li.current_page_item > a, #main-menu .menu-item-megamenu-parent.menu-links-with-bg .megamenu-child-container ul.sub-menu > li > ul > li.current_page_ancestor > a, #main-menu .menu-item-megamenu-parent.menu-links-with-bg .megamenu-child-container ul.sub-menu > li > ul > li.current-menu-item > a, #main-menu .menu-item-megamenu-parent.menu-links-with-bg .megamenu-child-container ul.sub-menu > li > ul > li.current-menu-ancestor > a, #main-menu ul li.menu-item-simple-parent.menu-links-with-bg ul > li.current_page_item > a, #main-menu ul li.menu-item-simple-parent.menu-links-with-bg ul > li.current_page_ancestor > a, #main-menu ul li.menu-item-simple-parent.menu-links-with-bg ul > li.current-menu-item > a, #main-menu ul li.menu-item-simple-parent.menu-links-with-bg ul > li.current-menu-ancestor > a {
    	<?php if( !is_null( $menulinkbghovercolor ) || !empty( $menulinkbghovercolor ) ):?>
        	background:<?php echo !empty($menulinkbghovercolor) ? $menulinkbghovercolor : '';?>;
        <?php endif;
			if( !is_null( $menulinkhovertxtcolor ) || !empty( $menulinkhovertxtcolor ) ):?>
        	color:<?php echo !empty($menulinkhovertxtcolor) ? $menulinkhovertxtcolor : '';?>;
        <?php endif;?>
    }<?php
endif;

#Menu link hover boder 
if( "true" == pregnancy_option('layout','menu-hover-border') ) {
	$mlhcolor = pregnancy_option('layout','menu-link-hborder-color');
	
	if( isset( $mlhcolor ) ) {?>   
      #main-menu .menu-item-megamenu-parent .megamenu-child-container ul.sub-menu > li > ul > li, #main-menu ul li.menu-item-simple-parent ul > li { width:100%; box-sizing:border-box; } 
      #main-menu .menu-item-megamenu-parent.menu-links-with-arrow .megamenu-child-container ul.sub-menu > li > ul > li > a, #main-menu ul li.menu-item-simple-parent.menu-links-with-arrow ul > li > a { padding-left:27px; }
	  #main-menu .menu-item-megamenu-parent.menu-links-with-arrow .megamenu-child-container ul.sub-menu > li > ul > li > a:before, #main-menu ul li.menu-item-simple-parent.menu-links-with-arrow ul > li > a:before { left:12px; }
      #main-menu .menu-item-megamenu-parent .megamenu-child-container ul.sub-menu > li > ul > li > a, #main-menu ul li.menu-item-simple-parent ul > li > a, #main-menu ul li.menu-item-simple-parent ul > li:last-child > a { padding:7px 10px; width:100%; box-sizing:border-box; border:1px solid transparent; }
      #main-menu .menu-item-megamenu-parent .megamenu-child-container ul.sub-menu > li > ul > li > a:hover, #main-menu ul li.menu-item-simple-parent ul > li > a:hover {
        border:1px solid <?php echo !empty($mlhcolor) ? $mlhcolor : '';?>;        
      }<?php		
	}
}

#Menu Links With Border
if( "true" == pregnancy_option('layout','menu-links-border') ) :

	$menulinkbrw = pregnancy_option('layout','menu-link-border-width');
	$menulinkbrc = pregnancy_option('layout','menu-link-border-color');
	$menulinkbrs = pregnancy_option('layout','menu-link-border-style'); ?>
    #main-menu .menu-item-megamenu-parent.menu-links-with-border .megamenu-child-container ul.sub-menu > li > ul > li > a, #main-menu ul li.menu-item-simple-parent.menu-links-with-border ul > li > a {
    	<?php if( isset( $menulinkbrw ) ) ?>
        	 border-bottom-width:<?php echo !empty($menulinkbrw) ? $menulinkbrw : '';?>px;
        <?php if( isset( $menulinkbrc ) ) ?>
        	 border-bottom-style:<?php echo !empty($menulinkbrs) ? $menulinkbrs : '';?>;
        <?php if( isset( $menulinkbrs ) ) ?>
        	 border-bottom-color:<?php echo !empty($menulinkbrc) ? $menulinkbrc : '';?>;
   }<?php	
endif;
