<?php
/* ---------------------------------------------------------------------------
 * Custom CSS from THeme option panel
 * --------------------------------------------------------------------------- */
if ( ! defined( 'ABSPATH' ) ) exit;

if( ($custom_css = pregnancy_option('layout','customcss-content')) &&  pregnancy_option('layout','enable-customcss')){
	echo stripcslashes( $custom_css )."\n";
}?>