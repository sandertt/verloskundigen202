<?php
/**
 * Theme Functions
 *
 * @package DTtheme
 * @author DesignThemes
 * @link http://wedesignthemes.com
 */

define( 'PREGNANCY_THEME_DIR', get_template_directory() );
define( 'PREGNANCY_THEME_URI', get_template_directory_uri() );
define( 'PREGNANCY_CORE_PLUGIN', WP_PLUGIN_DIR.'/designthemes-core-features' );
define( 'PREGNANCY_SETTINGS', 'pregnancy-opts' );

if (function_exists ('wp_get_theme')) :
	$themeData = wp_get_theme();
	define( 'PREGNANCY_THEME_NAME', $themeData->get('Name'));
	define( 'PREGNANCY_THEME_VERSION', $themeData->get('Version'));
endif;

define( 'PREGNANCY_LANG_DIR', PREGNANCY_THEME_DIR. '/languages' );

/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * --------------------------------------------------------------------------- */
load_theme_textdomain( 'Deukweg', PREGNANCY_LANG_DIR );

/* ---------------------------------------------------------------------------
 * Loads the Admin Panel Scripts
 * --------------------------------------------------------------------------- */
function pregnancy_admin_scripts() {

	wp_enqueue_style('pregnancy-admin', PREGNANCY_THEME_URI .'/framework/theme-options/style.css');
	wp_enqueue_style('pregnancy-chosen', PREGNANCY_THEME_URI .'/framework/theme-options/css/chosen.css');
	wp_enqueue_style('wp-color-picker');

	wp_enqueue_script('jquery-ui-tabs');
	wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_script('jquery-ui-slider');
	wp_enqueue_script('wp-color-picker');
	
	wp_enqueue_script('pregnancy-tooltip', PREGNANCY_THEME_URI . '/framework/theme-options/js/jquery.tools.min.js');
	wp_enqueue_script('pregnancy-chosen', PREGNANCY_THEME_URI . '/framework/theme-options/js/chosen.jquery.min.js');
	wp_enqueue_script('pregnancy-custom', PREGNANCY_THEME_URI . '/framework/theme-options/js/dttheme.admin.js');
	wp_enqueue_media();

	wp_localize_script('pregnancy-custom', 'objectL10n', array(
		'saveall' => esc_html__('Save All', 'pregnancy'),
		'saving' => esc_html__('Saving ...', 'pregnancy'),
		'noResult' => esc_html__('No Results Found!', 'pregnancy'),
		'resetConfirm' => esc_html__('This will restore all of your options to default. Are you sure?', 'pregnancy'),
		'importConfirm' => esc_html__('You are going to import the dummy data provided with the theme, kindly confirm?', 'pregnancy'),
		'backupMsg' => esc_html__('Click OK to backup your current saved options.', 'pregnancy'),
		'backupSuccess' => esc_html__('Your options are backuped successfully', 'pregnancy'),
		'backupFailure' => esc_html__('Backup Process not working', 'pregnancy'),
		'disableImportMsg' => esc_html__('Importing is disabled.. :), Please select atleast import type','pregnancy'),
		'restoreMsg' => esc_html__('Warning: All of your current options will be replaced with the data from your last backup! Proceed?', 'pregnancy'),
		'restoreSuccess' => esc_html__('Your options are restored from previous backup successfully', 'pregnancy'),
		'restoreFailure' => esc_html__('Restore Process not working', 'pregnancy'),
		'importMsg' => esc_html__('Click ok import options from the above textarea', 'pregnancy'),
		'importSuccess' => esc_html__('Your options are imported successfully', 'pregnancy'),
		'importFailure' => esc_html__('Import Process not working', 'pregnancy')));
}
add_action( 'admin_enqueue_scripts', 'pregnancy_admin_scripts' );

/* ---------------------------------------------------------------------------
 * Loads the Options Panel
 * --------------------------------------------------------------------------- */
require_once( PREGNANCY_THEME_DIR .'/framework/utils.php' );
//pregnancy  appointment
if( pregnancy_is_plugin_active('designthemes-pregnancy-addon/designthemes-pregnancy-addon.php') ):
	require_once( PREGNANCY_THEME_DIR .'/framework/reservation-util.php' );
endif;
require_once( PREGNANCY_THEME_DIR .'/framework/fonts.php' );
require_once( PREGNANCY_THEME_DIR .'/framework/theme-options/init.php' );

/* ---------------------------------------------------------------------------
 * Loads Theme Functions
 * --------------------------------------------------------------------------- */

// Functions --------------------------------------------------------------------
require_once( PREGNANCY_THEME_DIR .'/functions/register-functions.php' );

// Header -----------------------------------------------------------------------
require_once( PREGNANCY_THEME_DIR .'/functions/register-head.php' );

// Meta box ---------------------------------------------------------------------
require_once( PREGNANCY_THEME_DIR .'/framework/theme-metaboxes/post-metabox.php' );
require_once( PREGNANCY_THEME_DIR .'/framework/theme-metaboxes/page-metabox.php' );

// Tribe Events -----------------------------------------------------------------
if ( class_exists( 'Tribe__Events__Main' ) )
	require_once( PREGNANCY_THEME_DIR .'/framework/theme-metaboxes/event-metabox.php' );

// Menu -------------------------------------------------------------------------
require_once( PREGNANCY_THEME_DIR .'/functions/register-menu.php' );
require_once( PREGNANCY_THEME_DIR .'/functions/register-mega-menu.php' );

// Hooks ------------------------------------------------------------------------
require_once( PREGNANCY_THEME_DIR .'/functions/register-hooks.php' );

// Likes ------------------------------------------------------------------------
require_once( PREGNANCY_THEME_DIR .'/functions/register-likes.php' );

// Widgets ----------------------------------------------------------------------
add_action( 'widgets_init', 'pregnancy_widgets_init' );
function pregnancy_widgets_init() {
	require_once( PREGNANCY_THEME_DIR .'/functions/register-widgets.php' );

	if(class_exists('DTCorePlugin')) {
		register_widget('PREGNANCY_Twitter');
	}
	
	register_widget('PREGNANCY_Flickr');
	register_widget('PREGNANCY_Recent_Posts');
	register_widget('PREGNANCY_Portfolio_Widget');
}
if(class_exists('DTCorePlugin')) {
	require_once( PREGNANCY_THEME_DIR .'/functions/widgets/widget-twitter.php' );
}
require_once( PREGNANCY_THEME_DIR .'/functions/widgets/widget-flickr.php' );
require_once( PREGNANCY_THEME_DIR .'/functions/widgets/widget-recent-posts.php' );
require_once( PREGNANCY_THEME_DIR .'/functions/widgets/widget-recent-portfolios.php' );

// Plugins ---------------------------------------------------------------------- 
require_once( PREGNANCY_THEME_DIR .'/functions/register-plugins.php' );

// WooCommerce ------------------------------------------------------------------
if( function_exists( 'is_woocommerce' ) ){
	require_once( PREGNANCY_THEME_DIR .'/functions/register-woocommerce.php' );
}?>