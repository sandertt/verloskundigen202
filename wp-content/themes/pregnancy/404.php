<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php pregnancy_viewport(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php wp_head(); ?>
</head>
<?php
$type = pregnancy_opts_get('notfound-style', 'type1');

$bg = pregnancy_option('pageoptions','notfound-bg');
$opacity = pregnancy_opts_get('notfound-bg-opacity', '1');
$position = pregnancy_opts_get('notfound-bg-position', 'center center');
$repeat = pregnancy_opts_get('notfound-bg-repeat', 'no-repeat');
$color = pregnancy_option('pageoptions','notfound-bg-color');

$estyle = pregnancy_option('pageoptions','notfound-bg-style');
$color = !empty($color) ? pregnancy_hex2rgb($color) : array('f', 'f', 'f');
$style = !empty($bg) ? "background:url($bg) $position $repeat;" : '';
$style .= !empty($color) ? "background-color:rgba(  $color[0] ,  $color[1],  $color[2], {$opacity});" : '';
$style .= !empty($estyle) ? $estyle : ''; ?>

<body <?php body_class($type); ?> style="<?php echo esc_attr($style); ?>">

<div class="wrapper">
	<div class="center-content-wrapper">
		<div class="center-content"><?php
			$pageid = pregnancy_option('pageoptions','notfound-pageid');
			if( pregnancy_option('pageoptions','enable-404message') && !empty($pageid) ):
				$page = get_post( $pageid, ARRAY_A );
				echo DTCoreShortcodesDefination::dtShortcodeHelper ( stripslashes($page['post_content']) );
			elseif( pregnancy_option('pageoptions','enable-404message') ):
				echo '<div class="default-error-page">';
					echo '<h2>'.esc_html__('404 - Page Not Found', 'pregnancy').'</h2><h5>'.esc_html__('The Page you are looking for is not found or does not exist.', 'pregnancy').'</h5>';
					echo '<a class="dt-sc-button medium fully-rounded-border" target="_blank" href="'.home_url('/').'">'.esc_html__('Back to Home','pregnancy').'</a>';
				echo '</div>';
			endif; ?>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>