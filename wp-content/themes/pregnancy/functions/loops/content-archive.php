<?php
	$page_layout = pregnancy_option('pageoptions','post-archives-page-layout');
	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";
	$post_thumbnail = 'full';
	$container_class = "";
	
	$show_sidebar = true;
	if( $page_layout == "content-full-width" ){
		$show_sidebar = false;
	}

	$post_layout = pregnancy_option('pageoptions','post-archives-post-layout');
	$post_layout = isset( $post_layout ) ? $post_layout : 'one-column';
	switch($post_layout):
		default:
		case 'one-column':
			$post_class = $show_sidebar ? "column dt-sc-one-column with-sidebar blog-fullwidth" : "column dt-sc-one-column blog-fullwidth";
			$columns = 1;
		break;
		
		case 'one-half-column':
			$post_class = $show_sidebar ? "column dt-sc-one-half with-sidebar" : "column dt-sc-one-half";
			$columns = 2;
			$container_class = "apply-isotope";
		break;
		
		case 'one-third-column':
			$post_class = $show_sidebar ? "column dt-sc-one-third with-sidebar" : "column dt-sc-one-third";
			$columns = 3;
			$container_class = "apply-isotope";
		break;
	endswitch;



	$allow_excerpt = pregnancy_option('pageoptions','post-archives-enable-excerpt');
	$excerpt = pregnancy_option('pageoptions','post-archives-excerpt');
	
	$allow_read_more = pregnancy_option('pageoptions','post-archives-enable-readmore');
	$read_more = pregnancy_option('pageoptions','post-archives-readmore');

	$show_post_format = pregnancy_option('pageoptions','post-format-meta'); 
	$show_post_format = isset( $show_post_format )? "" : "hidden";
	
	$show_author_meta = pregnancy_option('pageoptions','post-author-meta');
	$show_author_meta = isset( $show_author_meta ) ? "" : "hidden";
	
	$show_date_meta = pregnancy_option('pageoptions','post-date-meta');
	$show_date_meta = isset( $show_date_meta ) ? "" : "hidden";	

	$show_comment_meta = pregnancy_option('pageoptions','post-comment-meta');
	$show_comment_meta = isset( $show_comment_meta ) ? "" : "hidden";

	$show_category_meta = pregnancy_option('pageoptions','post-category-meta');
	$show_category_meta = isset( $show_category_meta ) ? "" : "hidden";
	
	$show_tag_meta = pregnancy_option('pageoptions','post-tag-meta');
	$show_tag_meta = isset( $show_tag_meta ) ? "" : "hidden";

	$post_style = pregnancy_option('pageoptions','post-style');
	$post_style = isset( $post_style ) ? $post_style : "";

		$class_gw = '';
		if( $post_style == 'dt-sc-blog-content entry-date-left'){
			$class_gw = 'blog-grid';
		}
		
		if( $class_gw == 'blog-classic' ){
			if( $columns == 2 ){
				$post_thumbnail = 'blog-classic-two-three-col';
			}else if( $columns == 3 ){
				$post_thumbnail = 'blog-classic-two-three-col';
			}else{
				$post_thumbnail = 'full';
			}
		}
	
	if( have_posts() ) :
		$i = 1;
		echo "<div class='tpl-blog-holder ".esc_attr( $container_class )." ".esc_attr( $class_gw )."'>";
		
		while( have_posts() ):
			the_post();
			
			$temp_class = "";
			
			if($i == 1) $temp_class = $post_class." first"; else $temp_class = $post_class;
			if($i == $columns) $i = 1; else $i = $i + 1;
			
			$format = get_post_format(  get_the_id() );
			$format_link = 	get_post_format_link( $format );
			$link = get_permalink( get_the_id() );
			
			$post_meta = get_post_meta(get_the_id() ,'_dt_post_settings',TRUE);
			$post_meta = is_array($post_meta) ? $post_meta : array();
			
			$custom_class = "";?>
				<div class="<?php echo esc_attr($temp_class);?>">
                	<article id="post-<?php the_ID();?>" <?php post_class('blog-entry '.$post_style);?>>
                    	<!-- Featured Image -->
                        <?php if( $format == "image" || empty($format) ) :
                                if( has_post_thumbnail() ) :?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','pregnancy'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail($post_thumbnail);?></a>
                                            <div class="entry-format <?php echo esc_attr($show_post_format);?>">
                                                <a class="ico-format" href="<?php echo esc_url(get_post_format_link( $format ));?>"></a>
                                            </div>
                                            
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            elseif( $format === "gallery" ) :
                                if( array_key_exists("items", $post_meta) ) :
                                    echo '<div class="entry-thumb">';
                                    echo '	<ul class="entry-gallery-post-slider">';
                                                foreach ( $post_meta['items'] as $item ) {
                                                    echo "<li><img src='". esc_url($item)."'/></li>";
                                                }
                                    echo '	</ul>';
                                    echo '	<div class="entry-format '.esc_attr($show_post_format).'">';
                                    echo '		<a class="ico-format" href="'.esc_url(get_post_format_link( $format )).'"></a>';
                                    echo '	</div>';
                                    echo '</div>';
                                elseif( has_post_thumbnail() ):?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','pregnancy'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail($post_thumbnail);?></a>
                                        <div class="entry-format <?php echo esc_attr($show_post_format);?>">
                                            <a class="ico-format" href="<?php echo esc_url(get_post_format_link( $format ));?>"></a>
                                        </div>
                                            
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            elseif( $format === "video" ) :
                                if( array_key_exists('oembed-url', $post_meta) || array_key_exists('self-hosted-url', $post_meta) ) :
                                    echo '<div class="entry-thumb">';
                                    echo'	<div class="dt-video-wrap">';
                                                if( array_key_exists('oembed-url', $post_meta) ) :
                                                    echo wp_oembed_get($post_meta['oembed-url']);
                                                elseif( array_key_exists('self-hosted-url', $post_meta) ) :
                                                    echo wp_video_shortcode( array('src' => $post_meta['self-hosted-url']) );
                                                endif;
                                    echo '	</div>';
                                    echo '	<div class="entry-format '.esc_attr($show_post_format).'">';
                                    echo '		<a class="ico-format" href="'.esc_url(get_post_format_link( $format )).'"></a>';
                                    echo '	</div>';
                                    echo '</div>';
                                elseif( has_post_thumbnail() ):?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','pregnancy'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail($post_thumbnail);?></a>
                                        <div class="entry-format <?php echo esc_attr($show_post_format);?>">
                                            <a class="ico-format" href="<?php echo esc_url(get_post_format_link( $format ));?>"></a>
                                        </div>  
                                        
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            elseif( $format === "audio" ) :
                                if( array_key_exists('oembed-url', $post_meta) || array_key_exists('self-hosted-url', $post_meta) ) :
                                    echo '<div class="entry-thumb">';
                                            if( array_key_exists('oembed-url', $post_meta) ) :
                                                echo wp_oembed_get($post_meta['oembed-url']);
                                            elseif( array_key_exists('self-hosted-url', $post_meta) ) :
                                                $custom_class = "self-hosted-audio";
                                                echo wp_audio_shortcode( array('src' => $post_meta['self-hosted-url']) );
                                            endif;
                                    echo '	<div class="entry-format '.esc_attr($show_post_format).'">';
                                    echo '		<a class="ico-format" href="'.esc_url(get_post_format_link( $format )).'"></a>';
                                    echo '	</div>';
                                    echo '</div>';
                                elseif( has_post_thumbnail() ):?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','pregnancy'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail($post_thumbnail);?></a>
                                        <div class="entry-format <?php echo esc_attr($show_post_format);?>">
                                            <a class="ico-format" href="<?php echo esc_url(get_post_format_link( $format ));?>"></a>
                                        </div>
                                        
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            else:
                                if( has_post_thumbnail() ) :?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','pregnancy'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail($post_thumbnail);?></a>
                                        <div class="entry-format <?php echo esc_attr($show_post_format);?>">
                                            <a class="ico-format" href="<?php echo esc_url(get_post_format_link( $format ));?>"></a>
                                        </div>
                                        
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            endif;?>
                        <!-- Featured Image -->
                        
                        <!-- Content -->
                        <?php if( $post_style == "entry-date-left"): ?>
                        		
                                <div class="entry-details">
                                
                                    <div class="entry-date">
                                    
                                        <!-- date -->
                                        <div class="<?php echo esc_attr($show_date_meta);?>">
                                           <a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('Permalink to %s','pregnancy'), the_title_attribute('echo=0'));?>"> 
											<?php echo get_the_date('d');?>
                                             <span><?php echo get_the_date('M');?></span>
                                           </a>
                                        </div><!-- date -->
                                        
                                        <!-- comment -->
                                        <div class="comments <?php echo esc_attr($show_comment_meta);?>"><?php
                                            comments_popup_link( pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i> 0','pregnancy')),
                                                pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i> 1','pregnancy')),
                                                pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i> %','pregnancy')),
                                                '',
                                                pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i>','pregnancy')));?>
                                        </div><!-- comment -->                                         
                                     </div><!-- .entry-date -->


                                	<div class="entry-title"><?php
                                    	if(is_sticky()): ?>
                                            <div class="featured-post"> <span class="fa fa-trophy"> </span> <span class="text"> <?php esc_html_e('Featured','pregnancy'); ?> </span> </div><?php 
										endif;?>
                                    	<h4><a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('Permalink to %s','pregnancy'), the_title_attribute('echo=0'));?>"><?php the_title(); ?></a></h4>
                                    </div>
                                    
                                   <?php if( isset($allow_excerpt) && isset($excerpt) ):?>
                                    		<div class="entry-body"><?php echo pregnancy_excerpt($excerpt);?></div>
                                   <?php endif;?>

                                   <!-- Author & Category & Tag -->
                                   <div class="entry-meta-data">
                                   
                                   		<p class="author <?php echo esc_attr( $show_author_meta );?>">
                                        	<i class="pe-icon pe-user"> </i>
                                            <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>" 
                                            	title="<?php esc_attr_e('View all posts by ', 'pregnancy'); echo get_the_author();?>"><?php echo get_the_author();?></a>
                                        </p>
                                                                           
								   		<?php the_tags("<p class='tags {$show_tag_meta}'> <i class='pe-icon pe-ticket'> </i>",', ',"</p>");?>
                                        
                                        <p class="<?php echo esc_attr( $show_category_meta );?> category"><i class="pe-icon pe-network"> </i> <?php the_category(', '); ?></p>
                                        
                                   </div><!-- Category & Tag -->
                                   
                                   <!-- Read More Button -->
                                   <?php if( isset($allow_read_more) && isset($read_more) ):
											$sc = do_shortcode($read_more);
											$sc = str_replace("href='#'",'href="'.$link.'"',$sc);
											echo !empty( $sc ) ? $sc : '';
										endif;?><!-- Read More Button -->
                                   
                                </div><!-- .entry-details -->
                                
                        <?php elseif( $post_style == "entry-date-author-left"):?>
                        
                        		<div class="entry-date-author">
                                
                                	<div class="entry-date <?php echo esc_attr($show_date_meta);?>">
                                      <a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('Permalink to %s','pregnancy'), the_title_attribute('echo=0'));?>">	
										<?php echo get_the_date('d');?>
                                        <span><?php echo get_the_date('M');?></span>
                                      </a>
                                    </div>
                                    
                                    <div class="entry-author <?php echo esc_attr( $show_author_meta );?>">
                                    	<?php echo get_avatar(get_the_author_meta('ID'), 72 );?>
                                    	<a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>" 
                                        	title="<?php esc_attr_e('View all posts by ', 'pregnancy'); echo get_the_author();?>"><span><?php echo get_the_author();?></span></a>
                                    </div>
                                    
                                    <div class="comments <?php echo esc_attr($show_comment_meta);?>"><?php
										comments_popup_link( pregnancy_wp_kses( __('<i class="pe-icon pe-comment"> </i> 0','pregnancy')),
											 pregnancy_wp_kses( __('<i class="pe-icon pe-comment"> </i> 1','pregnancy')),
											 pregnancy_wp_kses( __('<i class="pe-icon pe-comment"> </i> %','pregnancy')), '',
											 pregnancy_wp_kses( __('<i class="pe-icon pe-comment"> </i>','pregnancy')));?>
                                    </div>
                                </div>
                                
                                <div class="entry-details">
                                
                                	<div class="entry-title"><?php
                                    	if(is_sticky()): ?>
                                            <div class="featured-post"> <span class="fa fa-trophy"> </span> <span class="text"> <?php esc_html_e('Featured','pregnancy'); ?> </span> </div><?php 
										endif;?>
                                    	<h4><a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('Permalink to %s','pregnancy'), the_title_attribute('echo=0'));?>"><?php the_title(); ?></a></h4>
                                    </div>
                                    
                                    <?php if( isset($allow_excerpt) && isset($excerpt) ):?>
                                    		<div class="entry-body"><?php echo pregnancy_excerpt($excerpt);?></div>
                                   <?php endif;?>
                                   
                                   <!-- Category & Tag -->
                                   <div class="entry-meta-data">
								   		<?php the_tags("<p class='tags {$show_tag_meta}'> <i class='pe-icon pe-ticket'> </i>",', ',"</p>");?>
                                        <p class="<?php echo esc_attr( $show_category_meta );?> category"><i class="pe-icon pe-network"> </i> <?php the_category(', '); ?></p>
                                   </div><!-- Category & Tag -->
                                   
                                   <!-- Read More Button -->
                                   <?php if( isset($allow_read_more) && isset($read_more) ):
								   			$sc = do_shortcode($read_more);
											$sc = str_replace("href='#'",'href="'.$link.'"',$sc);
											echo !empty( $sc ) ? $sc : '';
										endif;?><!-- Read More Button -->
                                </div>
                        
                        
                        <?php //pregnancy  style ?>
                        <?php elseif( $post_style == "dt-sc-blog-content entry-date-left" ): ?>
                        
                                <div class="entry-details">
                                
                                  <div class="entry-date <?php echo esc_attr($show_date_meta);?>">
                                      <div class="entry-date-month">
                                        <a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('Permalink to %s','pregnancy'), the_title_attribute('echo=0'));?>">  
										  <?php echo get_the_date('d');?>
                                          <span><?php echo get_the_date('M');?></span>
                                        </a>
                                      </div>
                                  </div>
                                    
                                    <!-- Category , author , comments & Tag -->
                                   <?php $tclass = ( ($show_tag_meta == "hidden" ) && ($show_category_meta == "hidden" ) && ($show_author_meta == "hidden" ) && ($show_comment_meta == "hidden" ) ) ? "hidden" : ""; ?>
                                   <div class="entry-meta-data <?php echo esc_attr($tclass);?>">
								   		<!-- category -->
                                        <p class="<?php echo esc_attr( $show_category_meta );?> category"><?php esc_html_e("In ","pregnancy");  the_category(', '); ?></p>
                                        <!-- author -->
                                        <?php if ( $show_author_meta != "hidden" && $show_category_meta != "hidden" ){  echo '/'; } ?>
                                        <p class="author <?php echo esc_attr( $show_author_meta );?>">
                                    	<?php esc_html_e("By ","pregnancy"); ?>
                                    	<a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>" 
                                        	title="<?php esc_attr_e('View all posts by ', 'pregnancy'); echo get_the_author();?>"><?php echo get_the_author();?></a>
                                    	</p>
                                        <!-- comments -->
                                        <?php if ( $show_comment_meta != "hidden" && ($show_author_meta != "hidden" || $show_category_meta != "hidden") ){  echo '/'; } ?>
                                        <p class="comments <?php echo esc_attr($show_comment_meta);?> "><?php
											comments_popup_link( pregnancy_wp_kses( __('No comments','pregnancy')),
												 pregnancy_wp_kses( __('1 comment','pregnancy')),
												 pregnancy_wp_kses( __('% comments','pregnancy')), '',
												 pregnancy_wp_kses( __('Comments are off','pregnancy')));?>
										</p>
                                        <!-- tags -->
                                        <?php if ( $show_tag_meta != "hidden" && has_tag() && ( $show_comment_meta != "hidden" || $show_author_meta != "hidden" || $show_category_meta != "hidden" ) ){  echo '/'; } ?>
                                        <?php the_tags("<p class='tags {$show_tag_meta}'> ",', ',"</p>");?>
                                        
                                        
                                   </div><!-- Category , author , comments & Tag -->
                                   <div class="entry-title"><?php
                                    	if(is_sticky()): ?>
                                            <div class="featured-post"> <span class="fa fa-trophy"> </span> <span class="text"> <?php esc_html_e('Featured','pregnancy'); ?> </span> </div><?php 
										endif;?>
                                    	<h4><a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','pregnancy'), the_title_attribute('echo=0'));?>"><?php the_title(); ?></a></h4>
                                    </div>
                                    
                                   <?php if( isset($allow_excerpt) && isset($excerpt) ):?>
                                    		<div class="entry-body"><?php echo pregnancy_excerpt($excerpt);?></div>
                                   <?php endif;?>
                                   
                                   <!-- Read More Button -->
                                    <?php if( isset($allow_read_more) && isset($read_more) ):
											$sc = do_shortcode($read_more);
											$sc = str_replace("href='#'",'href="'.$link.'"',$sc);
											echo !empty( $sc ) ? '<div class="read-more">'.$sc.'</div>' : '';
										  endif;?><!-- Read More Button -->
                                          
                                </div>
                        <?php //pregnancy style ends ?>
                        
						<?php else: # Default Post Style ?>
                        		<div class="entry-details">
                                	<!-- Meta -->
                                    <div class="entry-meta">
                                    
                                    	<div class="date <?php echo esc_attr($show_date_meta);?>"><?php esc_html_e('Posted on','pregnancy'); echo get_the_date(' d M Y');?></div>
                                        
                                        <div class="comments <?php echo esc_attr($show_comment_meta);?>"> / <?php
											comments_popup_link( pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i> 0 Comment','pregnancy')),
												pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i> 1 Comment','pregnancy')),
												pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i> % Comments','pregnancy')), '',
												pregnancy_wp_kses( __('<i class="pe-icon pe-chat"> </i>','pregnancy')));?>
                                        </div>
                                        
                                        <div class="author <?php echo esc_attr( $show_author_meta );?>">
                                        	/ <i class="pe-icon pe-user"> </i>
                                            
                                            <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>" 
                                            	title="<?php esc_attr_e('View all posts by ', 'pregnancy'); echo get_the_author();?>"><?php echo get_the_author();?></a>
                                        </div>
                                    
                                    </div><!-- Meta -->
                                    
                                    <div class="entry-title"><?php
                                    	if(is_sticky()): ?>
                                            <div class="featured-post"> <span class="fa fa-trophy"> </span> <span class="text"> <?php esc_html_e('Featured','pregnancy'); ?> </span> </div><?php 
										endif;?>
                                    	<h4><a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('Permalink to %s','pregnancy'), the_title_attribute('echo=0'));?>"><?php the_title(); ?></a></h4>
                                    </div>
                                    
                                    <?php if( isset($allow_excerpt) && isset($excerpt) ):?>
                                    		<div class="entry-body"><?php echo pregnancy_excerpt($excerpt);?></div>
                                    <?php endif;?>
                                    
                                    <!-- Category & Tag -->
                                    <div class="entry-meta-data">
                                    	<?php the_tags("<p class='tags {$show_tag_meta}'> <i class='pe-icon pe-ticket'> </i>",', ',"</p>");?>
                                        <p class="<?php echo esc_attr( $show_category_meta );?> category"><i class="pe-icon pe-network"> </i> <?php the_category(', '); ?></p>
                                    </div><!-- Category & Tag -->
                                    
                                    <!-- Read More Button -->
                                    <?php if( isset($allow_read_more) && isset($read_more) ):
											$sc = do_shortcode($read_more);
											$sc = str_replace("href='#'",'href="'.$link.'"',$sc);
											echo !empty( $sc ) ? $sc : '';
										  endif;?><!-- Read More Button -->
                            	</div><!-- .entry-details -->
                        <?php endif;?>
                        <!-- Content -->                        
                    </article>
				</div><?php
		endwhile;
		
		echo '</div>';?>
        
        	<!-- **Pagination** -->
            <div class="pagination blog-pagination"><?php echo pregnancy_pagination();?></div><!-- **Pagination** -->
            <!-- Blog Template Ends --><?php
		
	else:?>
    	<h2><?php esc_html_e('Nothing Found.', 'pregnancy'); ?></h2>
        <p><?php esc_html_e('Apologies, but no results were found for the requested archive.', 'pregnancy'); ?></p><?php
	endif;?>