<?php
/* ---------------------------------------------------------------------------
 * Loading Theme Scripts
 * --------------------------------------------------------------------------- */
add_action('wp_enqueue_scripts', 'pregnancy_enqueue_scripts');
function pregnancy_enqueue_scripts() {

	$library_uri = PREGNANCY_THEME_URI.'/functions';

	// ie scripts ----------------------------------------------------------------
	wp_enqueue_style('html5shiv', 'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js', array(), '3.7.2', true);
	wp_style_add_data( 'html5shiv', 'conditional', 'IE' );
	
	wp_enqueue_style('excanvas', 'http://explorercanvas.googlecode.com/svn/trunk/excanvas.js', array(), '2.0', true);
	wp_style_add_data( 'excanvas', 'conditional', 'IE' );		

	// comment reply script ------------------------------------------------------
	if (is_singular() AND comments_open()):
		 wp_enqueue_script( 'comment-reply' );
	endif;

	// scipts variable -----------------------------------------------------------
	$stickynav = ( pregnancy_option("layout","layout-stickynav") ) ? "enable" : "disable";
	$loadingbar = ( pregnancy_option("general","enable-loader") ) ? "enable" : "disable";
	$nicescroll = ( pregnancy_option("general","enable-nicescroll") ) ? "enable" : "disable";
	if(is_rtl()) $rtl = true; else $rtl = false;

	$htype = pregnancy_option('layout','header-type');
	$stickyele = "";
	switch( $htype ){
		case 'fullwidth-header':
		case 'boxed-header':
		case 'split-header fullwidth-header':
		case 'split-header boxed-header':
		case 'two-color-header':
			$stickyele = "main-header-wrapper";
		break;
			
		case 'fullwidth-header header-align-center fullwidth-menu-header':
		case 'fullwidth-header header-align-left fullwidth-menu-header':
			$stickyele = "menu-wrapper";
		break;

		case 'left-header':
		case 'left-header-boxed':
		case 'creative-header':
		case 'overlay-header':
			$stickyele = "menu-wrapper";
			$stickynav = "disable";
		break;
	}

	wp_enqueue_script('ui.totop', $library_uri.'/js/jquery.ui.totop.min.js', array(), false, true);
	wp_enqueue_script('jq.plugins', $library_uri.'/js/jquery.plugins.js', array(), false, true);
	wp_enqueue_script('visualNav', $library_uri.'/js/jquery.visualNav.min.js', array(), false, true);

	//reservation starts
	if( pregnancy_is_plugin_active('designthemes-pregnancy-addon/designthemes-pregnancy-addon.php') ):
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-ui-datepicker','http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css' );
		wp_enqueue_script( 'reservation',  $library_uri.'/js/reservation.js', array( 'jquery' ), false, true );
		wp_localize_script('reservation', 'dtAppointmentCustom', array(
				'plugin_url' => plugin_dir_url ( __FILE__ ),
				'eraptdatepicker' => esc_html__('Please Select Service and Date','pregnancy'),
				));
	endif;
	//reservation ends
	
	$picker = pregnancy_option('general', 'enable-stylepicker');
	if( isset($picker) ) {
		wp_enqueue_script('cookie', $library_uri.'/js/jquery.cookie.min.js',array(),false,true);
		wp_enqueue_script('jq.cpanel', $library_uri.'/js/controlpanel.js',array(),false,true);
	}
	
	if( $loadingbar == 'enable' ) {
		wp_enqueue_script('pacemin', $library_uri.'/js/pace.min.js',array(),false,true);
		wp_localize_script('pacemin', 'paceOptions', array(
			'restartOnRequestAfter' => 'false',
			'restartOnPushState' => 'false'
		));
	}
	
	wp_enqueue_script('stickyfloat', $library_uri.'/js/stickyfloat.js',array(),false,true);
	
	wp_enqueue_script('jq.custom', $library_uri.'/js/custom.js', array(), false, true);
	
	wp_localize_script('jq.plugins', 'dttheme_urls', array(
		'theme_base_url' => esc_js(PREGNANCY_THEME_URI),
		'framework_base_url' => esc_js(PREGNANCY_THEME_URI).'/framework/',
		'ajaxurl' => admin_url('admin-ajax.php'),
		'url' => get_site_url(),
		'stickynav' => esc_js($stickynav),
		'stickyele' => esc_js('.'.$stickyele),
		'isRTL' => esc_js($rtl),
		'loadingbar' => esc_js($loadingbar),
		'nicescroll' => esc_js($nicescroll)
	));	
}

/* ---------------------------------------------------------------------------
 * Meta tag for viewport scale
* --------------------------------------------------------------------------- */
function pregnancy_viewport()
{
	if(pregnancy_option('general', 'enable-responsive')){
		echo "<meta name='viewport' content='width=device-width, initial-scale=1'>\r";
	}
}

/* ---------------------------------------------------------------------------
 * Scripts of Custom JS from Theme Back-End
* --------------------------------------------------------------------------- */
function pregnancy_scripts_custom()
{
	if( ($custom_js = pregnancy_option('layout', 'customjs-content')) && pregnancy_option('layout','enable-customjs') ){
		wp_add_inline_script('jq.custom', pregnancy_wp_kses(stripslashes($custom_js)) ,'after');
	}
}
add_action('wp_footer', 'pregnancy_scripts_custom', 100);

/* ---------------------------------------------------------------------------
 * Loading Theme Styles
 * --------------------------------------------------------------------------- */
add_action( 'wp_enqueue_scripts', 'pregnancy_enqueue_styles', 100 );
function pregnancy_enqueue_styles() {

	$layout_opts = pregnancy_option('layout');
	$general_opts = pregnancy_option('general');
	$colors_opts = pregnancy_option('colors');
	$pageopt_opts = pregnancy_option('pageoptions');

	// site icons ---------------------------------------------------------------
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ):
		$url = ! empty ( $layout_opts ['favicon-url'] ) ? $layout_opts ['favicon-url'] : PREGNANCY_THEME_URI . "/images/favicon.ico";
		echo "<link href='$url' rel='shortcut icon' type='image/x-icon'>\n";
	
		$phone_url = ! empty ( $layout_opts ['apple-favicon'] ) ? $layout_opts ['apple-favicon'] : PREGNANCY_THEME_URI . "/images/apple-touch-icon.png";
		echo "<link href='$phone_url' rel='apple-touch-icon-precomposed'>\n";
	
		$phone_retina_url = ! empty ( $layout_opts ['apple-retina-favicon'] ) ? $layout_opts ['apple-retina-favicon'] : PREGNANCY_THEME_URI. "/images/apple-touch-icon-114x114.png";
		echo "<link href='$phone_retina_url' sizes='114x114' rel='apple-touch-icon-precomposed'>\n";
	
		$ipad_url = ! empty ( $layout_opts ['apple-ipad-favicon'] ) ? $layout_opts ['apple-ipad-favicon'] : PREGNANCY_THEME_URI . "/images/apple-touch-icon-72x72.png";
		echo "<link href='$ipad_url' sizes='72x72' rel='apple-touch-icon-precomposed'>\n";
	
		$ipad_retina_url = ! empty ( $layout_opts ['apple-ipad-retina-favicon'] ) ? $layout_opts ['apple-ipad-retina-favicon'] : PREGNANCY_THEME_URI . "/images/apple-touch-icon-144x144.png";
		echo "<link href='$ipad_retina_url' sizes='144x144' rel='apple-touch-icon-precomposed'>\n";
	endif;

	// wp_enqueue_style ---------------------------------------------------------------
	wp_enqueue_style( 'style',				get_stylesheet_uri(), false, PREGNANCY_THEME_VERSION, 'all' );
	wp_enqueue_style( 'prettyphoto',		PREGNANCY_THEME_URI .'/css/prettyPhoto.css', false, PREGNANCY_THEME_VERSION, 'all' );
	wp_enqueue_style( 'fancybox',		PREGNANCY_THEME_URI .'/css/jquery.fancybox.css', false, PREGNANCY_THEME_VERSION, 'all' );
	
	// icon fonts ---------------------------------------------------------------------
	wp_enqueue_style ( 'font-awesome',	PREGNANCY_THEME_URI . '/css/font-awesome.min.css', array (), '4.3.0' );
	wp_enqueue_style ( 'pe-icon-7-stroke',		PREGNANCY_THEME_URI . '/css/pe-icon-7-stroke.css', array () );
	wp_enqueue_style ( 'stroke-gap-icons-style',PREGNANCY_THEME_URI . '/css/stroke-gap-icons-style.css', array () );

	// comingsoon css
	if(isset($pageopt_opts['enable-comingsoon']))
		wp_enqueue_style("comingsoon",  	PREGNANCY_THEME_URI . "/css/comingsoon.css", false, PREGNANCY_THEME_VERSION, 'all' );

	// notfound css
	if ( is_404() )
		wp_enqueue_style("notfound",	  	PREGNANCY_THEME_URI . "/css/notfound.css", false, PREGNANCY_THEME_VERSION, 'all' );

	// loader css
	if(isset($general_opts['enable-loader']))
		wp_enqueue_style("loader",	  		PREGNANCY_THEME_URI . "/css/loaders.css", false, PREGNANCY_THEME_VERSION, 'all' );

	// show mobile slider
    if(empty($general_opts['show-mobileslider'])):
		$out =	'@media only screen and (max-width:320px), (max-width: 479px), (min-width: 480px) and (max-width: 767px), (min-width: 768px) and (max-width: 959px),
		 (max-width:1200px) { #slider { display:none !important; } }';
		wp_add_inline_style( 'style', $out );
	endif;

	// woocommerce css
	if( function_exists( 'is_woocommerce' ) )
		wp_enqueue_style( 'woo-style', 		PREGNANCY_THEME_URI .'/css/woocommerce.css', 'woocommerce-general-css', PREGNANCY_THEME_VERSION, 'all' );


	// static css
	if(isset($general_opts['enable-staticcss'])) :
		wp_enqueue_style("static",  		PREGNANCY_THEME_URI . "/style-static.css", false, PREGNANCY_THEME_VERSION, 'all' );

	// skin css
	else :
		$skin	  = $colors_opts['theme-skin'];

		if($skin == 'custom')	wp_enqueue_style("custom",	PREGNANCY_THEME_URI .'/css/style-custom-color.php', false, PREGNANCY_THEME_VERSION, 'all' );
		else					wp_enqueue_style("skin", 	PREGNANCY_THEME_URI ."/css/skins/$skin/style.css");

		// default-color.php
		wp_enqueue_style( 'default-color-php', 			PREGNANCY_THEME_URI .'/css/style-default-color.php', false, PREGNANCY_THEME_VERSION, 'all' );
		
		// style.php
		wp_enqueue_style( 'style-php', 			PREGNANCY_THEME_URI .'/css/style-fonts.php', false, PREGNANCY_THEME_VERSION, 'all' );
	endif;

	// Theme Options > Layout > Custom CSS
	if( ($custom_css = pregnancy_option('layout','customcss-content')) &&  pregnancy_option('layout','enable-customcss')){		
		wp_enqueue_style( 'dt-theme-option-custom-css', PREGNANCY_THEME_URI .'/css/dt-theme-option-custom-css.php', false, PREGNANCY_THEME_VERSION, 'all' );
	}

	// tribe-events -------------------------------------------------------------------
	wp_enqueue_style( 'custom-event', 		PREGNANCY_THEME_URI .'/tribe-events/custom.css', false, PREGNANCY_THEME_VERSION, 'all' );

	// responsive ---------------------------------------------------------------------
	if(pregnancy_option('general', 'enable-responsive'))
		wp_enqueue_style("responsive",  	PREGNANCY_THEME_URI . "/css/responsive.css", false, PREGNANCY_THEME_VERSION, 'all' );

	wp_enqueue_style("responsive-style",  	PREGNANCY_THEME_URI . "/css/responsive-style.css", false, PREGNANCY_THEME_VERSION, 'all' );

	// google fonts -----------------------------------------------------------------
	$google_fonts = pregnancy_fonts();
	$google_fonts = $google_fonts['all'];

	$subset 	  = pregnancy_option('fonts','font-subset');
	if( $subset ) $subset = str_replace(' ', '', $subset);

	// style & weight  --------------------------------------------------------------
	if( $weight = pregnancy_option('fonts', 'font-style') )
		$weight = ':'. implode( ',', $weight );

	$fonts = pregnancy_fonts_selected();
	$fonts = array_unique($fonts);
	$fonts_url = ''; $font_families = array();
	foreach( $fonts as $font ){
		if( in_array( $font, $google_fonts ) ){
			// if google fonts
			$font_families[] .= $font . $weight;
		}
	}
	$query_args = array( 'family' => urlencode( implode( '|', $font_families ) ), 'subset' => urlencode( $subset ) );
	$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	wp_enqueue_style( 'pregnancy-fonts', 		esc_url_raw($fonts_url), false, PREGNANCY_THEME_VERSION );

	// custom css ---------------------------------------------------------------------
	wp_enqueue_style( 'dt-custom', 			PREGNANCY_THEME_URI .'/css/custom.css', false, PREGNANCY_THEME_VERSION, 'all' );

	// jquery scripts --------------------------------------------
	wp_enqueue_script('modernizr-custom', 	PREGNANCY_THEME_URI . '/functions/js/modernizr.custom.js', array('jquery'));
	wp_enqueue_script('jquery');

	// rtl ----------------------------------------------------------------------------
	if(is_rtl()) wp_enqueue_style('rtl', 	PREGNANCY_THEME_URI . '/css/rtl.css', false, PREGNANCY_THEME_VERSION, 'all' );
}

/* ---------------------------------------------------------------------------
 * Styles Minify
 * --------------------------------------------------------------------------- */
function pregnancy_styles_minify( $css ){

	// remove comments
	$css = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css );	

	// remove whitespace
	$css = str_replace( array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css );

	return $css;
}

/* ---------------------------------------------------------------------------
 * Styles Dynamic
 * --------------------------------------------------------------------------- */
function pregnancy_styles_dynamic()
{
	echo '<!-- pregnancy dynamic style -->'."\n";
	echo '<style id="pregnancy-dynamic-style-css">'."\n";
		ob_start();

		if( ! pregnancy_opts_get( 'enable-staticcss' ) ){

			// custom colors.php
			$colors_opts = pregnancy_option('colors');
			$skin	  = $colors_opts['theme-skin'];
			if($skin == 'custom'):
				include_once PREGNANCY_THEME_DIR . '/css/style-custom-color.php';
			endif;
			
			// default colors.php
			include_once PREGNANCY_THEME_DIR . '/css/style-default-color.php';

			// fonts.php
			include_once PREGNANCY_THEME_DIR . '/css/style-fonts.php';			
		}
		
		// custom optons css.php			
		if( ($custom_css = pregnancy_option('layout','customcss-content')) &&  pregnancy_option('layout','enable-customcss')):
			include_once PREGNANCY_THEME_DIR . '/css/dt-theme-option-custom-css.php';
		endif;		

		$css = ob_get_contents();

		ob_get_clean();

		echo pregnancy_styles_minify( $css ) ."\n";

	echo '</style>'."\n";
}
add_action( 'wp_head', 'pregnancy_styles_dynamic' );

/* ---------------------------------------------------------------------------
 * Styles of Custom Font
 * --------------------------------------------------------------------------- */
function pregnancy_styles_custom_font()
{
	$fonts 		  = pregnancy_fonts_selected();
	$font_custom  = pregnancy_option('fonts','customfont-name');
	$font_custom2 = pregnancy_option('fonts','customfont2-name');

	if( $font_custom && in_array( $font_custom, $fonts ) ){
		  $custom_font = '@font-face {';
			  $custom_font .= 'font-family: "'. $font_custom .'";';
			  $custom_font .= 'src: url("'. pregnancy_option('fonts','customfont-eot') .'");';
			  $custom_font .= 'src: url("'. pregnancy_option('fonts','customfont-eot') .'#iefix") format("embedded-opentype"),';
				  $custom_font .= 'url("'. pregnancy_option('fonts','customfont-woff') .'") format("woff"),';
				  $custom_font .= 'url("'. pregnancy_option('fonts','customfont-ttf') .'") format("truetype"),';
				  $custom_font .= 'url("'. pregnancy_option('fonts','customfont-svg') . $font_custom .'") format("svg");';
			  $custom_font .= 'font-weight: normal;';
			  $custom_font .= 'font-style: normal;';
		  $custom_font .= '}'."\n";
		wp_add_inline_style( 'style', $custom_font );
	}
	
	if( $font_custom2 && in_array( $font_custom2, $fonts ) ){
		
		$custom_font2 = '@font-face {';
			$custom_font2 .= 'font-family: "'. $font_custom2 .'";';
			$custom_font2 .= 'src: url("'. pregnancy_option('fonts','customfont2-eot') .'");';
			$custom_font2 .= 'src: url("'. pregnancy_option('fonts','customfont2-eot') .'#iefix") format("embedded-opentype"),';
				$custom_font2 .= 'url("'. pregnancy_option('fonts','customfont2-woff') .'") format("woff"),';
				$custom_font2 .= 'url("'. pregnancy_option('fonts','customfont2-ttf') .'") format("truetype"),';
				$custom_font2 .= 'url("'. pregnancy_option('fonts','customfont2-svg') . $font_custom2 .'") format("svg");';
			$custom_font2 .= 'font-weight: normal;';
			$custom_font2 .= 'font-style: normal;';
		$custom_font2 .= '}'."\n";
			
		wp_add_inline_style( 'style', $custom_font2 );
	}
}
add_action('wp_head', 'pregnancy_styles_custom_font');

/* ---------------------------------------------------------------------------
 * Fonts Selected in Theme Options Panel
 * --------------------------------------------------------------------------- */
function pregnancy_fonts_selected(){
	$fonts = array();
	
	$font_opts = pregnancy_option('fonts');
	
	$fonts['content'] 		= !empty ( $font_opts['content-font'] ) 	? 	$font_opts['content-font'] 		: 'Oswald';
	$fonts['menu'] 			= !empty ( $font_opts['menu-font'] ) 		? 	$font_opts['menu-font'] 		: 'Oswald';
	$fonts['title'] 		= !empty ( $font_opts['pagetitle-font'] ) 	? 	$font_opts['pagetitle-font'] 	: 'Oswald';
	$fonts['h1'] 		= !empty ( $font_opts['h1-font'] ) 	? 	$font_opts['h1-font'] 		: 'Oswald';
	$fonts['h2'] 		= !empty ( $font_opts['h2-font'] ) 	? 	$font_opts['h2-font'] 		: 'Oswald';
	$fonts['h3'] 		= !empty ( $font_opts['h3-font'] ) 	? 	$font_opts['h3-font'] 		: 'Oswald';
	$fonts['h4'] 		= !empty ( $font_opts['h4-font'] ) 	? 	$font_opts['h4-font'] 		: 'Oswald';
	$fonts['h5'] 		= !empty ( $font_opts['h5-font'] ) 	? 	$font_opts['h5-font'] 		: 'Oswald';
	$fonts['h6'] 		= !empty ( $font_opts['h6-font'] ) 	? 	$font_opts['h6-font'] 		: 'Oswald';

	return $fonts;
}

/* ---------------------------------------------------------------------------
 * Site SSL Compatibility
 * --------------------------------------------------------------------------- */
function pregnancy_ssl( $echo = false ){
	$ssl = '';
	if( is_ssl() ) $ssl = 's';
	if( $echo ){
		echo !empty($ssl) ? $ssl : '';
	}
	return $ssl;
}

/* ---------------------------------------------------------------------------
 * Layout Selected in Theme Options Panel
 * --------------------------------------------------------------------------- */
add_action('wp_head', 'pregnancy_appearance_css', 9);
function pregnancy_appearance_css() {
	$output = NULL;

	if (pregnancy_option('layout', 'site-layout') == 'boxed') :

		if (pregnancy_option('layout', 'bg-type') == 'bg-patterns') :
			$pattern 			= 	pregnancy_option('layout', 'boxed-layout-pattern');
			$pattern_repeat 	= 	pregnancy_option('layout', 'boxed-layout-pattern-repeat');
			$pattern_opacity 	= 	pregnancy_option('layout', 'boxed-layout-pattern-opacity');
			$enable_color 		= 	pregnancy_option('layout', 'show-boxed-layout-pattern-color');
			$pattern_color 		= 	pregnancy_option('layout', 'boxed-layout-pattern-color');

			$output .= "body { ";

			if (!empty($pattern)) {
				$output .= "background-image:url('" . PREGNANCY_THEME_URI . "/framework/theme-options/images/patterns/{$pattern}');";
			}

			$output .= "background-repeat:$pattern_repeat;";
			if ($enable_color) {
				if (!empty($pattern_opacity)) {
					$color = pregnancy_hex2rgb($pattern_color);
					$output .= "background-color:rgba($color[0],$color[1],$color[2],$pattern_opacity); ";
				} else {
					$output .= "background-color:$pattern_color;";
				}
			}
			$output .= "}\r\t";

		elseif (pregnancy_option('layout', 'bg-type') == 'bg-custom') :
			$bg 			= 	pregnancy_option('layout', 'boxed-layout-bg');
			$bg_repeat 		= 	pregnancy_option('layout', 'boxed-layout-bg-repeat');
			$bg_opacity 	= 	pregnancy_option('layout', 'boxed-layout-bg-opacity');
			$bg_color 		= 	pregnancy_option('layout', 'boxed-layout-bg-color');
			$enable_color 	= 	pregnancy_option('layout', 'show-boxed-layout-bg-color');
			$bg_position 	= 	pregnancy_option('layout', 'boxed-layout-bg-position');

			$output .= "body { ";

			if (!empty($bg)) {
				$output .= "background-image:url($bg);";
				$output .= "background-repeat:$bg_repeat;";
				$output .= "background-position:$bg_position;";
			}

			if ($enable_color) {
				if (!empty($bg_opacity)) {
					$color = pregnancy_hex2rgb($bg_color);
					$output .= "background-color:rgba($color[0],$color[1],$color[2],$bg_opacity);";
				} else {
					$output .= "background-color:$bg_color;";
				}
			}
			$output .= "}\r\t";
		endif;

	endif;
	
	if (!empty($output)) :
		wp_add_inline_style( 'style', $output );
	endif;
}

/* ---------------------------------------------------------------------------
 * Body Class Filter for layout changes
 * --------------------------------------------------------------------------- */
function pregnancy_body_classes( $classes ) {

	// layout
	$classes[] 		= 	'layout-'. pregnancy_option('layout','site-layout');

	// header
	$header_type 	= 	pregnancy_option('layout','header-type');
	if( isset($header_type) && ($header_type == 'left-header-boxed') ):
		$classes[]	=	'left-header left-header-boxed';
	elseif( isset($header_type) && ($header_type == 'creative-header') ):
		$classes[]	=	'left-header left-header-creative';
	else:
		$classes[]	=	$header_type;
	endif;

	$htrans 		= 	pregnancy_option('layout', 'header-transparant');
	if(isset($htrans)):
		$classes[]	=	pregnancy_option('layout', 'header-transparant');
	endif;
	
	$stickyhead		=	pregnancy_option('layout','layout-stickynav');
	if(isset($stickyhead)):
		$classes[]	=	'sticky-header';
	endif;

	$standard		=	pregnancy_option('layout','header-position');
	if( isset($standard) && ($standard == 'above slider') ):
		$classes[]	=	'standard-header';
	elseif( isset($standard) && ($standard == 'below slider') ):
		$classes[]	=	'standard-header header-below-slider';
	elseif ( isset($standard) && ($standard == 'on slider') ):
		$classes[]	=	'header-on-slider';
	endif;

	$topbar			=	pregnancy_option('layout','layout-topbar');
	if(isset($topbar)):
		$classes[]	=	'header-with-topbar';
	endif;

	
	$wootype		=	pregnancy_option('woo','product-style');
	if($wootype == 'preg-shop'){
		$wootype		= 'preg-shop';
	}else{
		$wootype		= 	!empty($wootype) ? 'woo-'.$wootype : 'woo-type1';
	}
	$classes[]		=	$wootype;

	if( is_page() ) {
		$pageid = pregnancy_ID();
		if(($slider_key = get_post_meta( $pageid, '_tpl_default_settings', true )) && (array_key_exists( 'show_slider', $slider_key )) ) {
			$classes[] = "page-with-slider";
		}
	} elseif( is_home() ) {
		$pageid = get_option('page_for_posts');
		if(($slider_key = get_post_meta( $pageid, '_tpl_default_settings', true )) && (array_key_exists( 'show_slider', $slider_key )) ) {
			$classes[] = "page-with-slider";
		}
	}

	return $classes;
}
add_filter( 'body_class', 'pregnancy_body_classes' );?>