<?php
/* ---------------------------------------------------------------------------
 * Hook of Top
 * --------------------------------------------------------------------------- */
function pregnancy_hook_top()
{
	if( pregnancy_option( 'pageoptions','enable-top-hook' ) ) :
		echo '<!-- pregnancy_hook_top -->';
			$hook = stripslashes(htmlspecialchars_decode(pregnancy_option('pageoptions','top-hook'),ENT_QUOTES));
			$hook = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "$1", $hook);
			if (!empty($hook))
				echo DTCoreShortcodesDefination::dtShortcodeHelper( $hook );
		echo '<!-- pregnancy_hook_top -->';
	endif;	
}
add_action( 'pregnancy_hook_top', 'pregnancy_hook_top' );


/* ---------------------------------------------------------------------------
 * Hook of Content before
 * --------------------------------------------------------------------------- */
function pregnancy_hook_content_before()
{
	if( pregnancy_option( 'pageoptions','enable-content-before-hook' ) ) :
		echo '<!-- pregnancy_hook_content_before -->';
			$hook = stripslashes(htmlspecialchars_decode(pregnancy_option('pageoptions','content-before-hook'),ENT_QUOTES));
			$hook = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "$1", $hook);
			if (!empty($hook))
				echo DTCoreShortcodesDefination::dtShortcodeHelper( $hook );
		echo '<!-- pregnancy_hook_content_before -->';
	endif;
}
add_action( 'pregnancy_hook_content_before', 'pregnancy_hook_content_before' );


/* ---------------------------------------------------------------------------
 * Hook of Content after
 * --------------------------------------------------------------------------- */
function pregnancy_hook_content_after()
{
	if( pregnancy_option( 'pageoptions','enable-content-after-hook' ) ) :
		echo '<!-- pregnancy_hook_content_after -->';
			$hook = stripslashes(htmlspecialchars_decode(pregnancy_option('pageoptions','content-after-hook'),ENT_QUOTES));
			$hook = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "$1", $hook);
			if (!empty($hook))
				echo DTCoreShortcodesDefination::dtShortcodeHelper( $hook );
		echo '<!-- pregnancy_hook_content_after -->';
	endif;
}
add_action( 'pregnancy_hook_content_after', 'pregnancy_hook_content_after' );


/* ---------------------------------------------------------------------------
 * Hook of Bottom
 * --------------------------------------------------------------------------- */
function pregnancy_hook_bottom()
{
	if( pregnancy_option( 'pageoptions','enable-bottom-hook' ) ) :
		echo '<!-- pregnancy_hook_bottom -->';
			$hook = stripslashes(htmlspecialchars_decode(pregnancy_option('pageoptions','bottom-hook'),ENT_QUOTES));
			$hook = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "$1", $hook);
			if (!empty($hook))
				echo DTCoreShortcodesDefination::dtShortcodeHelper( $hook );
		echo '<!-- pregnancy_hook_bottom -->';
	endif;
}
add_action( 'pregnancy_hook_bottom', 'pregnancy_hook_bottom' );

?>