<?php
/* ---------------------------------------------------------------------------
 * Registers some menu location to use with navigation menus.
 * --------------------------------------------------------------------------- */
if (!function_exists('pregnancy_navigation_menus')) {

	// register navigation menus
	function pregnancy_navigation_menus() {
		$locations = array(
			'main-menu' 		=> esc_html__('Main Menu', 'pregnancy'),
			'secondary-menu' 	=> esc_html__('Secondary Menu', 'pregnancy'),
			'shortcode-menu'    => esc_html__('Shortcode Menu', 'pregnancy'),
		);
		register_nav_menus($locations);
	}

	// hook into the 'init' action
	add_action('init', 'pregnancy_navigation_menus');
}

/* ---------------------------------------------------------------------------
 * Main Menu
 * --------------------------------------------------------------------------- */
function pregnancy_wp_nav_menu($depth = 0)
{
	$args = array( 
		'container' 		=> 'nav',
		'container_id'		=> 'main-menu',
		'menu_class'		=> 'menu',
		'menu_id'			=> '', 
		'fallback_cb'		=> 'pregnancy_wp_page_menu',
		'items_wrap'      	=> '<ul class="%2$s">%3$s</ul>', 
 		'theme_location'	=> 'main-menu',
		'depth' 			=> $depth,
		'walker' 			=> new Pregnancy_FrontEndMenuWalker,
	);

	wp_nav_menu( $args );
}

function pregnancy_wp_page_menu() {
	echo '<nav id="main-menu" class="menu-main-menu-container"><ul id="menu-main-menu" class="menu">';
	$args = array(
		'depth' 		=> 0,
		'title_li' 		=> '',
		'echo' 			=> 0,
		'post_type' 	=> 'page',
		'post_status' 	=> 'publish'
	);
	$pages = wp_list_pages($args);
	if ($pages)
		echo !empty($pages) ? $pages : '';
	echo '</ul></nav>';
}

/* ---------------------------------------------------------------------------
 * Split Menu
 * --------------------------------------------------------------------------- */
function pregnancy_wp_split_menu() 
{
	// Main Menu Left ----------------------------
	$args = array( 
		'container' 		=> false,
		'menu_id'         	=> false,
		'menu_class'		=> 'menu menu-left',
		'fallback_cb'		=> false, 
		'theme_location'	=> 'main-menu',
		'depth' 			=> 0,
		'walker' 			=> new Pregnancy_FrontEndMenuWalker,
	);
	wp_nav_menu( $args );

	// Main Menu Right ----------------------------
	$args = array( 
		'container' 		=> false,
		'menu_id'         	=> false,
		'menu_class'		=> 'menu menu-right',
		'fallback_cb'		=> false, 
		'theme_location'	=> 'secondary-menu',
		'depth' 			=> 0,
		'walker' 			=> new Pregnancy_FrontEndMenuWalker,
	);
	wp_nav_menu( $args );
}

/* ---------------------------------------------------------------------------
 * Secondary menu
 * --------------------------------------------------------------------------- */
function pregnancy_wp_secondary_menu()
{
	$args = array(
		'container' 		=> false,
		'container_id'		=> 'secondary-menu', 
		'menu_class'		=> 'secondary-menu', 
		'fallback_cb'		=> false,
		'theme_location' 	=> 'secondary-menu',
		'depth'				=> 1,
	);
	wp_nav_menu( $args );
}
?>