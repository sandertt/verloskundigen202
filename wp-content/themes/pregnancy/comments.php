<?php if ( post_password_required() ) : ?>
	<p class="nopassword"><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.','pregnancy'); ?></p>
<?php  return;
	endif;?>
    
    <h3><?php comments_number(esc_html__('No Comments','pregnancy'), esc_html__('Comment ( 1 )','pregnancy'), esc_html__('Comments ( % )','pregnancy') );?></h3>
    <?php if ( have_comments() ) : ?>
    
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
                    <div class="navigation">
                        <div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments','pregnancy'  ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments','pregnancy') ); ?></div>
                    </div> <!-- .navigation -->
        <?php endif; // check for comment navigation ?>
        
        <ul class="commentlist">
     		<?php wp_list_comments( array( 'callback' => 'pregnancy_comment_style' ) ); ?>
        </ul>
    
    <?php else: ?>
		<?php if ( ! comments_open() ) : ?>
            <p class="nocomments"><?php esc_html_e( 'Comments are closed.','pregnancy'); ?></p>
        <?php endif;?>    
    <?php endif; ?>
	
    <!-- Comment Form -->
    <?php if ('open' == $post->comment_status) :
			$comment = "<div class='column dt-sc-one-half first'><textarea id='comment' name='comment' cols='5' rows='3' placeholder='".esc_html__("Comment",'pregnancy')."' ></textarea></div>";
			$author = "<div class='column dt-sc-one-half'><p><input id='author' name='author' type='text' placeholder='".esc_html__("Name",'pregnancy')."' required /></p>";
			$email = "<p> <input id='email' name='email' type='text' placeholder='".esc_html__("Email",'pregnancy')."' required /> </p></div>";

				$comments_args = array(
					'title_reply' => esc_html__( 'Give a Reply','pregnancy' ),
					'fields'=>array('author' => $author,'email' =>	$email),
					'comment_field'=> $comment,
					'comment_notes_before'=>'','comment_notes_after'=>'','label_submit'=>esc_html__('Comment','pregnancy'));
            comment_form($comments_args);?>
	<?php endif; ?>