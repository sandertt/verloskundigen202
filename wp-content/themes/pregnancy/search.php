<?php get_header();
	$page_layout = pregnancy_option('pageoptions','post-archives-page-layout');
	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";
	
	$show_sidebar = $show_left_sidebar = $show_right_sidebar = false;
	$sidebar_class = "";
	
	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;
		
		case 'with-both-sidebar':
			$page_layout = "page-with-sidebar with-both-sidebar";
			$show_sidebar = $show_left_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php get_sidebar('left');?></section>
			<!-- Secondary Left --><?php
		endif;
	endif;?>
    <section id="primary" class="<?php echo esc_attr( $page_layout );?>">
<?php $search_text = empty($_GET['s']) ? esc_html__("Enter Keyword",'pregnancy') : get_search_query(); ?>
      <h10> U heeft gezocht op <h9><?php echo esc_attr( $search_text );?></h9><br> Resultaten bevinden zich hieronder:</h10>
    	<?php get_template_part('functions/loops/content', 'archive');?>
 
    </section><!-- **Primary - End** --><?php
	
	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php get_sidebar('right');?></section>
			<!-- Secondary Right --><?php
		endif;
	endif;
get_footer();?>
<!-- style voor search results pagina =-->
<style>
.blog-entry.entry-date-left .entry-title {
    margin-top: -22px;
}
.entry-meta-data, .entry-date  {
    opacity: 0;
}
.tpl-blog-holder.apply-isotope.blog-grid.isotope {
    min-height: 175px !important;
}

.tpl-blog-holder.apply-isotope.blog-grid.isotope {
    max-height: 100px !important;
}
#primary {
    text-align: center;
}



</style>
