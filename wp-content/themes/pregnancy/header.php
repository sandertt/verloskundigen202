<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php pregnancy_viewport(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=News+Cycle:700" rel="stylesheet">
</head>

<body <?php body_class(); ?>>

<?php
// loading
$loader = pregnancy_option('general','enable-loader');
if( isset($loader) ) echo '<div class="loader"><div class="loader-inner ball-scale-multiple"><div></div><div></div><div></div></div></div>';
// top hook
do_action( 'pregnancy_hook_top' ); ?>

<!-- **Wrapper** -->
<div class="wrapper">
    <div class="inner-wrapper">

        <!-- **Header Wrapper** -->
        <?php $hdarkbg = pregnancy_option('layout','header-darkbg'); $class = isset( $hdarkbg ) ? "dt-sc-dark-bg" : ""; ?>
        <div id="header-wrapper" class="<?php echo esc_attr( $class );?>">
            <!-- **Header** -->
            <header id="header"><?php
                //top bar
                $topbar 	= pregnancy_option('layout','layout-topbar');
                $topcontent = pregnancy_option('layout','top-content');
                if( isset($topbar) && isset($topcontent) && $topcontent != '' ):?>
                    <div class="top-bar">
                    <div class="container"><?php
                        $content = pregnancy_option('layout','top-content');
                        $content = do_shortcode( stripslashes($content) );
                        echo pregnancy_wp_kses( $content );?>
                    </div>
                    </div><?php
                endif;

                // header types
                $htype = pregnancy_option('layout','header-type');
                if( $htype != "left-header" && $htype != "left-header-boxed" && $htype != "creative-header" && $htype != "overlay-header" ):
                    // header position
                    $headerpos = pregnancy_option('layout','header-position');
                    if( isset($headerpos) && $headerpos == 'below slider' ):
                        echo pregnancy_slider();
                    endif;
                endif;?>

                <!-- **Main Header Wrapper** -->
                <!-- <div class="megamenu-child-container menu-hasbg " style="left: -696.873px; display: block; background-image: url(http://wedesignthemes.com/themes/dt-pregnancy/wp-content/uploads/2016/05/megamenuimage1.jpg);">

                     <ul class="sub-menu ">
                         <li id="menu-item-8648" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1"><a href="#">Thumb Layout</a>
                             <ul class="sub-menu ">
                                 <li id="menu-item-7780" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-thumb/">Fullwidth</a></li>
                                 <li id="menu-item-8655" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-thumb-right-sidebar/">With Right Sidebar</a></li>
                                 <li id="menu-item-8654" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-thumb-left-sidebar/">With Left Sidebar</a></li>
                             </ul>
                             <a class="dt-menu-expand">+</a></li>
                         <li id="menu-item-8660" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1"><a href="#">Grid Layout</a>
                             <ul class="sub-menu ">
                                 <li id="menu-item-8689" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-grid/">Fullwidth</a></li>
                                 <li id="menu-item-8687" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-grid-with-rhs/">With Right Sidebar</a></li>
                                 <li id="menu-item-8688" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-grid-with-lhs/">With Left Sidebar</a></li>
                             </ul>
                             <a class="dt-menu-expand">+</a></li>
                         <li id="menu-item-8668" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1"><a href="#">Classic Layout</a>
                             <ul class="sub-menu ">
                                 <li id="menu-item-8667" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-classic/">Fullwidth</a></li>
                                 <li id="menu-item-8666" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-classic-with-rhs/">With Right Sidebar</a></li>
                                 <li id="menu-item-8665" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2"><a href="http://wedesignthemes.com/themes/dt-pregnancy/blog-classic-with-lhs/">With Left Sidebar</a></li>
                             </ul>
                             <a class="dt-menu-expand">+</a></li>
                         <li id="menu-item-8042" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1 menu-item-fullwidth  menu-item-with-widget-area  fill-four-columns "><div class="menu-item-widget-area-container"><ul><li id="text-10" class="widget widget_text">			<div class="textwidget"><div class="mega_menu_intro_text">
                                                 <div class="mega_menu_intro_text_title">
                                                     <h5>Having Personal Questions?</h5>
                                                     <h6>You can talk to a doctor</h6>
                                                 </div>
                                                 <p><a href="http://wedesignthemes.com/themes/dt-pregnancy/" target=" _blank" title="Link" class="dt-sc-button   medium icon-right with-icon  fully-rounded-border  ">Talk To Us <span class="fa fa-comments-o"> </span></a> <span>or</span>
                                                     <a href="http://wedesignthemes.com/themes/dt-pregnancy/" target=" _blank" title="Link" class="dt-sc-button   medium icon-right with-icon  fully-rounded-border  ">Fix an Appointment <span class="fa fa-user-md"> </span></a></p></div>

                                         </div>
                                     </li></ul></div></li>
                     </ul>
                     <a class="dt-menu-expand">+</a>
                 </div> -->




                <div id="main-header-wrapper" class="main-header-wrapper">

                    <div id="menu-container">
                        <div class="container">

                            <!-- **Main Header** -->
                            <div class="main-header"><?php
                                if( isset($htype) && ($htype == 'fullwidth-header header-align-center fullwidth-menu-header') ):?>
                                    <div class="header-left"><?php
                                    $leftcontent = pregnancy_option('layout','menu-top-left-content');
                                    if( isset($leftcontent) && $leftcontent != '') :
                                        echo do_shortcode( stripcslashes( $leftcontent ) );
                                    else:
                                        echo '<div class="dt-sc-hr-invisible-xsmall"></div>';
                                    endif; ?>
                                    </div><?php
                                endif;
                                // Logo plek (oud)

                                if( isset($htype) && (($htype == 'fullwidth-header header-align-center fullwidth-menu-header') ||
                                        ($htype == 'fullwidth-header header-align-left fullwidth-menu-header')) ):?>
                                    <div class="header-right"><?php
                                    $rightcontent = pregnancy_option('layout','menu-top-right-content');
                                    if( isset($rightcontent) && $rightcontent != '') :
                                        echo do_shortcode( stripcslashes( $rightcontent ) );
                                    endif; ?>
                                    </div><?php
                                endif;
                                ?>
                                <?php pregnancy_header_logo(); ?>
                                <div id="menu-wrapper" class="menu-wrapper">

                                    <div class="dt-menu-toggle" id="dt-menu-toggle">
                                        <?php esc_html_e('Menu','pregnancy');?>
                                        <span class="dt-menu-toggle-icon"></span>
                                    </div><?php
                                    if( isset($htype) ):
                                        switch($htype):
                                            case 'split-header fullwidth-header':
                                            case 'split-header boxed-header':
                                                echo '<nav id="main-menu">';
                                                pregnancy_wp_split_menu();
                                                echo '</nav>';
                                                break;

                                            case 'overlay-header':
                                                echo '<div class="overlay overlay-hugeinc">';
                                                echo '<div class="overlay-close"></div>';
                                                pregnancy_wp_nav_menu(1);
                                                echo '</div>';
                                                break;

                                            case 'fullwidth-header':
                                            case 'boxed-header':
                                            case 'two-color-header':
                                            default:
                                                pregnancy_wp_nav_menu();
                                                require_once( PREGNANCY_THEME_DIR .'/headers/default.php' );
                                                break;
                                        endswitch;
                                    endif;

                                    ?>
                                </div><?php
                                // left header
                                if( isset($htype) && ( $htype == 'left-header' || $htype == 'left-header-boxed' || $htype == 'creative-header') ): ?>
                                    <div class="left-header-footer"><?php
                                    $content = pregnancy_option('layout','menu-left-header-content');
                                    $content = do_shortcode( stripcslashes( $content ) );
                                    echo !empty($content) ? $content : '';?>
                                    </div><?php
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </div><!-- **Main Header Wrapper end ** --><?php
                if( $htype != "left-header" && $htype != "left-header-boxed" && $htype != "creative-header" && $htype != "overlay-header" ):
                    // header position
                    if( isset($headerpos) && $headerpos != 'below slider' ):
                        echo pregnancy_slider();
                    endif;
                endif;?>

            </header><!-- **Header - End** -->
        </div><!-- **Header Wrapper - End** -->

        <?php if( $htype == "creative-header" ) echo '<div id="toggle-sidebar"></div>'; ?>

        <?php if( $htype == "overlay-header" ) echo '<div id="trigger-overlay"></div>'; ?>

        <!-- **Main** -->
        <div id="main"><?php

            if( $htype == "left-header" || $htype == "left-header-boxed" || $htype == "creative-header" || $htype == "overlay-header" ):
                echo pregnancy_slider();
            endif;

            // subtitle & breadcrumb section
            if( !is_front_page() ):
                global $post;
                $show_slider = '';
                if( !(is_null($post)) ) {
                    $tpl_default_settings = get_post_meta($post->ID,'_tpl_default_settings',TRUE);
                    $show_slider = isset($tpl_default_settings['show_slider']) ? TRUE : FALSE;
                }

                if($show_slider != TRUE):
                    require_once( PREGNANCY_THEME_DIR .'/headers/breadcrumb.php' );
                endif;
            endif;

            $class = "container";

            if( is_page_template('tpl-portfolio.php') ) {
                $tpl_default_settings = get_post_meta($post->ID,'_tpl_default_settings',TRUE);
                $tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();
                $class =  isset( $tpl_default_settings['portfolio-fullwidth'] ) ? "portfolio-fullwidth-container" : "container";
            }

            if( is_singular('tribe_events') ) {
                $tpl_default_settings = get_post_meta($post->ID,'_custom_settings',TRUE);
                $tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();
                $post_style = array_key_exists( "event-post-style", $tpl_default_settings ) ? $tpl_default_settings['event-post-style'] : "type1";
                switch( $post_style ):
                    case 'type2':
                        $class = "event-type2-fullwidth";
                        break;
                    case 'type5':
                        $class = "event-type5-fullwidth";
                        break;
                    default:
                        $class = "container";
                endswitch;
            }

            if( is_singular() ) {
                $tpl_default_settings = get_post_meta($post->ID,'_custom_settings',TRUE);
                $tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();
                $class =  ( isset( $tpl_default_settings['layout'] ) ) && ( $tpl_default_settings['layout'] == 'fullwidth-container') ? "show-in-fullwidth" : $class;
            } ?>
            <!-- ** Container ** -->
            <div class="<?php echo esc_attr($class);?>"><?php
                do_action( 'pregnancy_hook_content_before' ); ?>
                <style>
                    @media only screen and (min-width: 2134px) {
                        img.normal_logo {
                            left: 48% !important;
                        }
                    }

                    /*nieuwe updates*/
                    /*klein scherm logo plaatsing*/
                    @media only screen and (max-width: 1320px) {
                        .img.normal_logo {
                            position: relative !important;
                            top: 37px !important;
                            margin: -70px 0px 5px !important;
                            z-index: 10 !important;
                        }
                        /* #main-menu ul.menu li {
                            position: relative;
                            padding: 0px;
                            left: -288px;
                            top: -55px;
                        } */
                        .fa-search {
                            display: none;
                        }
                        img.normal_logo {
                            /* width: 195px !important; */



.menu-wrapper {
   
    margin-top: -30px;
}

                        }
                        }


  @media only screen and (min-width: 1200px) and (max-width:1917px) {



}


  @media only screen and (min-width: 800px) and (max-width:917px) {


.page-item-11, .page-item-13, .page-item-186, .page-item-15, .page-item-9 {

 left: 18px !important; 
}


}





                    @media only screen and (min-width: 1320px) and (max-width: 1452px) {
                        .img.normal_logo{
                            position: absolute;
                            z-index: 99999;
                            top: -64px !important;
                            margin: 0px -220px -10px !important;
                        }}

                                        @media only screen and (min-width: 1134px){
                                            #header-wrapper {
                                                position: relative !important;
                                                /* left: 120px; */
                                            }

                                            img.normal_logo {
                                                z-index: 99999;
                                                left: 50%;
                                                margin-left: -158px;
                                                position: fixed;

                                                width: 16%;
top: -10px;

                                            }

                                          
                                            }


/*linker kant */

                                            .page-item-11, .page-item-13, .page-item-186, .page-item-15, .page-item-9 {
                                                float: left;
                                                margin: 0px;
                                                padding: 0px;
                                                position: absolute;
                                                left: 15%;  
                                                top: 0;
                                                margin-left: -835px;


                                          }
@media only screen and (max-width: 1058px){
                                            .page-item-11, .page-item-13, .page-item-186, .page-item-15, .page-item-9, .page-item-22, .page-item-15, .page-item-25  {
                                             
                                                left: 0% !important;
                                              }
#main-menu ul.menu li {
float: none !important;
}
nav#main-menu {
    margin-left: 80px !important;
   
}
  
                                           }      




     @media all and (max-width: 1000px) {
                                                           nav#main-menu {
                                                               width: 80% !important;

                                                           }

                                                           nav#main-menu {
                                                               width: 100% !important;                                                           
                                                            margin-left: 40px;
                                                               
                                                           }
                                                       }
                                                /* smallest screens */
                                                           @media all and (max-width: 650px) {
                                                               .dt-sc-contact-info.type3:after {
                                                                   display: none;
                                                               }

                                                               .dt-sc-contact-info.with-image {
                                                                   display: none !important;
                                                               }

                                                               p {
                                                                   font-size: 15px;
                                                               }

                                                               .dt-sc-contact-info.type3 {
                                                                   border-radius: 6px;
                                                               }

                                                               #footer {
                                                                   margin-top: -310px;
                                                               }
                                                               .vc_column_container > .vc_column-inner {
                                                                   box-sizing: border-box;
                                                                   padding-left: 1px;
                                                                   padding-right: 1px;
                                                               }
                                                               .dt-sc-contact-info.type3:before {
                                                                   top: 4px;
                                                                   border-bottom: 1px;
                                                                   border-bottom-style: solid;
                                                                   border-bottom-color:white;
                                                               }
}
 @media all and (max-width: 770px) {

.container {
    width: 100% !important;
}
nav#main-menu {
 
    float: right;
}
     .vc_custom_1466059052838 h1 {

     }

}


 @media all and (max-width: 920px) {
 .main-header {
padding: 0 !important;
}


}
     @media all and (min-width: 720px) {
.vc_custom_1466059052838 {
    margin-top: -10px;
}    
}                                                  

/*general styling for all screen sizes */
                    .page-item-69, .page-item-9, .page-item-18, .page-item-20{
                        display:none !important;
                    }
                    /* rechter kant */
                    .page-item-22, .page-item-15, .page-item-25 {
                        float: right;
                        margin: 0px;
                        padding: 0px;
                        position: absolute;
                        left: 45%;
                        top: 0;
                        margin-left: -835px;
                    }
                    .search {
                        top: -45px;
left: 11px;
                    }
                                                           .dt-sc-contact-info.type3 {
                                                               height: 350px;
                                                           }
                                                           .vc_row.wpb_row.vc_row-fluid {
                                                               padding-left: 20px !important;
                                                               padding-right: 20px !important;
                                                           }
                                                           .dt-sc-contact-info.with-image {
                                                               -webkit-border-radius: 0 !important;
                                                           }

                                                           .langbf_right {
                                                               float: right;
                                                               padding-right: 8px;
                                                               position: relative;
                                                               /* top: 0px; */
                                                           }
                                                           /*nav bar lettergrootte*/
                                                           #main-menu ul.menu > li > a {
                                                               font-family: Quicksand, sans-serif;
                                                               font-size: 13px !important;
                                                           }
 img.retina_logo {
                        display: none;
                        opacity: 0
                    }
.page-item-25 a {
   text-decoration: underline;
    /* font-style:italic; */
}

.body{
    background-white;
}
                    .tp-fullwidth-forcer {
                        max-height: 650px !important;
                    }

                    @media only screen and (min-width: 1120px) {
                        .header-animate .normal_logo {
                            width: 9% !important;
                            margin: 0;
                            left: 45%;
                        }
                    }
                    @media only screen and (min-width: 1520px) {
                        .header-animate .normal_logo {
                            width: 7% !important;
                            margin: 0;
                            left: 46%;
                        }
                        }

                    .page-item-866 a, .page-item-862 a, .page-item-915 a {
                        display: none;
                    }

.ervaringenheader{
    font-family:"quicksand";
}


                </style>