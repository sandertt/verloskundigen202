		<?php do_action( 'pregnancy_hook_content_after' ); ?>
        </div><!-- **Container - End** -->

        </div><!-- **Main - End** --><?php

        // footer sections
        $footer 			= pregnancy_option('layout','enable-footer');


        $copyright_section  = pregnancy_option('layout','enable-copyright');

        if( isset($footer) || isset( $copyright_section) ) {?>
            <!-- **Footer** -->
            <footer id="footer"><?php
                if( isset( $footer ) ):
                    $darkbg = pregnancy_option('layout','footer-darkbg');
                    $class = isset( $darkbg ) ? " dt-sc-dark-bg" : ""; ?>

<div style="    text-align: center;
    bottom: -40px;
    position: relative;
    left: 27%;
    width: 47%;"


 class="vc_row wpb_row vc_row-fluid vc_custom_float vc_custom_1466060670250">
 <div class="wpb_column vc_column_container vc_col-sm-12">
 <div class="vc_column-inner vc_custom_1466059244456">
 <div class="wpb_wrapper">
 <div class="vc_row wpb_row vc_inner vc_row-fluid dt-sc-highlight dt-sc-dark-bg vc_custom_1466059018185 vc_row-has-fill">
 <div class="wpb_column vc_column_container vc_col-sm-12">
 <div class="vc_column-inner vc_custom_1466059227684">
 <div class="wpb_wrapper">
	<div class="wpb_raw_code wpb_content_element wpb_raw_html vc_custom_1466059052838">
		<div class="wpb_wrapper">
			<h1 style="font-weight: 100; margin-bottom:0px; color:black; z-index:1;font-size: x-large;position: relative; text-align: center;top: -18px;min-width: 156px;     font-size: 23px;"><span class="fa fa-phone"></span> 020 - 4700067</h1>
		<div class="wpb_wrapper_contact">
			<h1 style="font-weight: 100; margin-bottom:0px; color:black; z-index:1;font-size: x-large;position: relative; text-align: center;top: -18px;min-width: 156px;     font-size: 23px; display:none;"></span>Contact</h1>
		</div>
	</div>
</div></div></div></div></div></div></div></div>
                    <div class="footer-widgets">
                        <div class="container"><div class="column vc_col-md-4 first"><aside id="text-2" class="widget widget_text">
                        <div class="textwidget"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner ">
                        <div class="wpb_wrapper">
</div></div></div></div>
<h11>Contact</h11>
<p><span class="fa fa-location-arrow"></span>
Verloskundigen 101<br>
Johannes Verhulststraat 101<br>
1071 MX Amsterdam</p>
<br>
<p><span class="fa fa-location-arrow"></span>
Echo Amsterdam <br>
Ite Boeremastraat 1 <br>
1054 PP Amsterdam</p>
<br>
<p> <a  href="tel:0204700067" >020 - 470 00 67 </a>  verloskundigenpraktijk <br>
<a  href="tel:0206166990" >020 - 616 69 90 </a> echopraktijk <br>
<a  href="tel:0204608422" >020 - 460 84 22 </a> fax <br>
<a  href="tel:0203330420" >020 - 333 04 20 </a> spoed & bevalling </p>
<br>
<p>
<a style="text-decoration: underline;" href="mailto:praktijk@verloskundigen101.nl"><strong>praktijk@verloskundigen101.nl</strong></a><br>
<a style="text-decoration: underline;" href="mailto:info@echoamsterdam.nl"><strong>info@echoamsterdam.nl</strong></a><br>
</p>
<br>
<div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner ">
<div class="wpb_wrapper"><div class="vc_empty_space" style="height: 20px">

                    </div>
</div></div></div></div></aside></div>
<!-- EINDE RIJ 1 -->
<div class="column vc_col-md-3 "><aside id="text-4" class="widget widget_text">
		<div class="textwidget">
		                         <ul class="no-bullet">
                                 <li style="list-style-type: none;"><h11>Onze Pagina's</h11></li>
								 <li> <a title="Homepage" href="http://test.verloskundigen101.nl/"> Home </a> </li>
                                 <li style="list-style-type: none;"> <a title="Over ons" href="http://test.verloskundigen101.nl/Over-ons/"> Over ons </a> </li>
                                 <li style="list-style-type: none;"> <a title="Onze zorg" href="http://test.verloskundigen101.nl/Onze-zorg/"> Onze zorg</a> </li>
                                 <li style="list-style-type: none;"> <a title="Contact" href="http://test.verloskundigen101.nl/Contact/"> Medische informatie </a> </li>
                                 <li style="list-style-type: none;"> <a title="Nieuws" href="http://test.verloskundigen101.nl/Nieuws/"> Nieuws </a> </li>
                  <div class="search-container">
					<?php get_search_form(); ?>
                  </div>
				</div>
			</div>
			<p> Volg ons:</p>
 <a href="https://www.instagram.com/verloskundigen101/">
<i class="fa fa-instagram" aria-hidden="true" style="font-size:3em;"></i>
</a>
 <a href="https://www.facebook.com/Verloskundigen-101-521155427961146/">
<i class="fa fa-facebook-official" aria-hidden="true" style="font-size:3em;"></i>
</a>
 <a href="https://twitter.com/verloskunde101/">
<i class="fa fa-twitter" aria-hidden="true" style="font-size:3em;"></i>
</a>
			</div>
		</aside></div>
		<!-- EINDE RIJ 2-->
		<div class="column vc_col-md-4 "><aside id="text-5" class="widget widget_text">
		<div class="textwidget">
	<strong>Dé praktijk in Amsterdam voor persoonlijke,
professionele en complete zorg tijdens de
zwangerschap, bevalling en kraamtijd!</strong>
	<br><br>
<ul class="no-bullet">             <li style="list-style-type: none;"> <a title="Onze zorg" href="http://test.verloskundigen101.nl/voorwaarden/"> Colofoon</a> </li>
                                  <li style="list-style-type: none;"> <a title="Onze zorg" href="http://test.verloskundigen101.nl/voorwaarden/"> Disclaimer / Privacy</a> </li>
                                 <li style="list-style-type: none;"> <a title="Contact" href="http://test.verloskundigen101.nl/voorwaarden/">Kwaliteit </a> </li>
                                 <li style="list-style-type: none;"> <a title="E-consult" href="http://test.verloskundigen101.nl/voorwaarden/"> Klachtenregeling </a></li>
                                 <li style="list-style-type: none;"> <a title="Inschrijven" href="http://test.verloskundigen101.nl/voorwaarden/"> Betalingsvoorwaarden </a> </li>
                                 </ul>
                                 <br><br><br>
<strong><a href="https://www.zorgkaartnederland.nl/zorginstelling/verloskundepraktijk-verloskundigen-101-amsterdam-3062032/waardering">Waardeer onze zorg op Zorgkaart.</a></strong>
<a href="http://www.zorgkaartnederland.nl" title="ZorgkaartNederland"><img style="    width: 330px;
    float: left; position: relative;
    left: -20px;" src="http://test.verloskundigen101.nl/wp-content/uploads/2016/12/ZorgkaartNederland-logo.png" alt="Waardeer mij" title="Waardeer mij" /></a>
		</aside></div></div></div></div></div></aside></div>                        </div>
                    </div>
<!-- EINDE RIJ 3-->
                <?php
                endif;

                if( isset( $copyright_section) ):
                    $darkbg = pregnancy_option('layout','copyright-darkbg');
                    $class = isset( $darkbg ) ? " dt-sc-dark-bg" : ""; ?>

                    <div class="footer-copyright<?php echo esc_attr( $class );?>">
                        <div class="container"><?php
                            $content = pregnancy_option('layout','copyright-content');
                            (max-width: 770px)   $content = stripslashes ( $content );
                            $content = do_shortcode( $content );						
                            echo pregnancy_wp_kses( $content );?>
                        </div>
                    </div><?php
                endif;?>
            </footer><!-- **Footer - End** --><?php
        } ?>

	</div><!-- **Inner Wrapper - End** -->
</div><!-- **Wrapper - End** -->
<?php do_action( 'pregnancy_hook_bottom' ); ?>
<?php wp_footer(); ?>
<style>
#footer .widget ul.dt-sc-sociable li a {
    padding: 0px;
    /* position: absolute; */
    background-color: red;
    z-index: -444;
    margin-left: -40px;
    margin-top: -40px;
     opacity: 0; 
}
</style>