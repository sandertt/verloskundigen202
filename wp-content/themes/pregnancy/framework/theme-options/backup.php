<!-- #backup -->
<div id="backup" class="bpanel-content">

    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">
        <ul class="sub-panel"> 
            <li><a href="#tab1"><?php esc_html_e('Backup', 'pregnancy');?></a></li>
        </ul>
        
        <!-- #tab1-backup -->
        <div id="tab1" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Backup & Restore Options', 'pregnancy');?></h3>
                </div>

                <div class="box-content">
                	<div><?php
                    	esc_html_e('You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.', 'pregnancy');?>
                    </div><?php
					$backup = get_option('dt_theme_backup');
					$log = ( is_array( $backup) && array_key_exists('backup',$backup) ) ? $backup['backup'] : esc_html__('No backups yet', 'pregnancy');?>
					<p><strong><?php esc_html_e('Last Backup : ', 'pregnancy');?><span class="backup-log"><?php echo esc_html($log); ?></span></strong></p>
					<div class="clar"></div>
					<div class="hr_invisible"></div>
					<a href="#" id="dttheme_backup_button" class="bpanel-button black-btn" title="<?php esc_attr_e('Backup Options', 'pregnancy');?>"><?php esc_html_e('Backup Options', 'pregnancy');?></a>
					<a href="#" id="dttheme_restore_button" class="bpanel-button black-btn" title="<?php esc_attr_e('Restore Options', 'pregnancy');?>"><?php esc_html_e('Restore Options', 'pregnancy');?></a>
                </div><!-- .box-content -->

                <div class="box-title">
                    <h3><?php esc_html_e('Transfer Theme Options Data', 'pregnancy');?></h3>
                </div>

                <div class="box-content">
                	<div><?php
						esc_html_e("You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click 'Import Options'", 'pregnancy');?>
                    </div>
                    <div class="clar"></div>
                    <div class="hr_invisible"></div>
                	<?php $data = array( 'general' => pregnancy_option('general'),
										 'layout' => pregnancy_option('layout'),
										 'widgetarea' => pregnancy_option('widgetarea'),
										 'pageoptions' => pregnancy_option('pageoptions'),
										 'woo' => pregnancy_option('woo'),
										 'colors' => pregnancy_option("colors"),
										 'pregnancy_appointment' => pregnancy_option("pregnancy_appointment"),
										 'fonts' => pregnancy_option('fonts')); ?>
                    <textarea id="export_data" rows="13" cols="15"><?php echo base64_encode(serialize($data)); ?></textarea>
                    <div class="clear"></div>
                    <div class="hr_invisible"></div>
                    <a href="#" id="dttheme_import_button" class="bpanel-button black-btn" title="<?php esc_attr_e('Restore Options', 'pregnancy');?>"><?php esc_html_e('Import Options', 'pregnancy');?></a>
                </div>
            </div><!-- .bpanel-box end -->
        </div><!--#tab1-backup end-->

    </div><!-- .bpanel-main-content end-->
</div><!-- #backup end-->