<!-- #changelog -->
<div id="changelog" class="bpanel-content">

    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content no-margin">

        <!-- #tab1-changelog -->
        <div id="tab1" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php echo constant('PREGNANCY_THEME_NAME'); esc_html_e(' Theme Change Log', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                <pre>
2016.08.22 - version 1.2.2
 * Updated dummy data

2016.08.06 - version 1.2.1
 * Updated dummy data
 * Loading issues fixed
 
2016.07.15 - version 1.2
 * Plugins are moved to amazon
 * Some bpanel issues fixed
 * Some design tweaks updated

2016.07.01 - version 1.1.1
 * Fixed sidebar issue

2016.06.30 - version 1.1
 * Updated dummy data

2016.06.22 - version 1.0
 * First release!</pre>
                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->            
        </div><!--#tab1-import-demo end-->

    </div><!-- .bpanel-main-content end-->
</div><!-- #changelog end-->