<!-- #pregnancy appointment -->
<div id="pregnancy_appointment" class="bpanel-content">

    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">

        <ul class="sub-panel"> 
			<li><a href="#my-company"><?php  esc_html_e("Company",'pregnancy');?></a></li>
            <li><a href="#my-payment"><?php esc_html_e("Payment",'pregnancy');?></a></li>
            <li><a href="#my-notifications"><?php esc_html_e("Notifications",'pregnancy');?></a></li>
        </ul>
        
         <!-- #my-company start -->
        <div id="my-company" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box"><?php
            	if( pregnancy_is_plugin_active('designthemes-core-features/designthemes-core-features.php') ) : ?>
                    <div class="box-title"><h3><?php esc_html_e('Settings','pregnancy');?></h3></div>
                    <div class="box-content">
                        <h6><?php esc_html_e('Business Hours','pregnancy');?></h6><?php
                            echo '<table>'; 
                            foreach ( array( 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ) as $day ):
                                echo '<tr>';
                                echo '<td>'.ucfirst( $day ).'</td>';
                                echo '<td>';
                                echo pregnancy_dt_company_timer( "dttheme[pregnancy_appointment][dt_company_{$day}_start]",pregnancy_option('pregnancy_appointment', "dt_company_{$day}_start"));
                                echo '<span> - '.esc_html__( 'To', 'pregnancy' ).' - </span>';
                                echo pregnancy_dt_company_timer( "dttheme[pregnancy_appointment][dt_company_{$day}_end]", pregnancy_option('pregnancy_appointment', "dt_company_{$day}_end") ,false);
                                echo '</td>';
                                echo '</tr>';
                            endforeach;
                            echo '</table>';?>
                    </div><?php
                else:?>
                    <div class="box-title"><h3><?php esc_html_e('Warning','pregnancy');?></h3></div>
                    <div class="box-content">
                        <p class="note"><?php esc_html_e("You have to install and activate the Design Themes Core plugin to use this module ..",'pregnancy');?></p>
                    </div><?php
                endif;?>
            </div>
        </div><!-- #my-company end -->
        
        
        <!-- #my-payment start -->
        <div id="my-payment">
            <div class="bpanel-box"><?php
                if( pregnancy_is_plugin_active('designthemes-pregnancy-addon/designthemes-pregnancy-addon.php') ) : ?>
                    <div class="box-title"><h3><?php esc_html_e('Payments','pregnancy');?></h3></div>
                    <div class="box-content">

                        <h6><?php esc_html_e('Currency','pregnancy');?></h6>
                        <select name="dttheme[pregnancy_appointment][currency]"><?php
                            $selected = pregnancy_option('pregnancy_appointment', 'currency');
                            $currency_code_options = pregnancy_dt_appointment_currencies();
                            foreach ( $currency_code_options as $code => $name ) {
                                $symbol = pregnancy_dt_appointment_currency_symbol( $code );
                                $s = ( $code === $selected ) ? ' selected="selected" ' : "";
                                echo "<option value='{$code}' {$s}>{$name}( {$symbol} )</option>";
                            }?>
                        </select>
                        <div class="hr"></div>

                        <h6><?php esc_html_e('Enable Pay At Arrival','pregnancy');?></h6>
                        <div class="column one-fifth">
                            <?php pregnancy_switch("",'pregnancy_appointment','enable-pay-at-arrival');?>
                        </div>
                        <div class="column four-fifth last">
                            <p class="note no-margin"><?php esc_html_e('You can enable pay at arrival option to pay locally','pregnancy');?></p>
                        </div>
                        <div class="hr"></div>

                       
                    </div>                
                <?php
                else:?>
                    <div class="box-title"><h3><?php esc_html_e('Warning','pregnancy');?></h3></div>
                    <div class="box-content">
                        <p class="note"><?php esc_html_e("You have to install and activate the Design Themes Core plugin to use this module ..",'pregnancy');?></p>
                    </div><?php
                endif;?>
            </div>
        </div><!-- #my-payment end -->

        
        <!-- #my-notifications start -->
        <div id="my-notifications" class="tab-content">
        	<div class="bpanel-box"><?php
              if( pregnancy_is_plugin_active('designthemes-core-features/designthemes-core-features.php') ) :

                $sender_name = pregnancy_option('pregnancy_appointment', 'notification_sender_name'); 
                $sender_name = !empty($sender_name) ? $sender_name : get_option( 'blogname' );

                $sender_email = pregnancy_option('pregnancy_appointment', 'notification_sender_email'); 
                $sender_email = !empty( $sender_email ) ? $sender_email : get_option( 'admin_email' );?>
                <div class="box-title"><h3><?php esc_html_e('Settings','pregnancy');?></h3></div>
                <div class="box-content">
                    <h6><?php esc_html_e('Sender Name','pregnancy');?></h6>
                    <input type="text" name="dttheme[pregnancy_appointment][notification_sender_name]" value="<?php echo esc_attr( $sender_name );?>"/>
                    <h6><?php esc_html_e('Sender Emailid','pregnancy');?></h6>
                    <input type="text" name="dttheme[pregnancy_appointment][notification_sender_email]" value="<?php echo esc_attr( $sender_email );?>"/>
                    <div class="hr"></div>

                    <h6><?php esc_html_e('To send scheduled agenda please execute following script with your cron','pregnancy');?></h6>
                    <h5><?php echo WP_PLUGIN_DIR.'/designthemes-core-features/reservation/cron/send_agenda_cron.sh';?></h5>
                </div>
            
            	<div class="box-title"><h3><?php esc_html_e('To Staff','pregnancy');?></h3></div>
                <div class="box-content">
                
                	<h6><b><?php esc_html_e('New Appoinment Notification','pregnancy');?></b></h6>
                    <h5><?php esc_html_e('Subject','pregnancy');?></h5>
                    <input type="text" name="dttheme[pregnancy_appointment][appointment_notification_to_staff_subject]" class="full-width" 
                    	value="<?php echo pregnancy_option('pregnancy_appointment', 'appointment_notification_to_staff_subject'); ?>"/>
                    <h5><?php esc_html_e('Mesage','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'appointment_notification_to_staff_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][appointment_notification_to_staff_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                    <div class="hr"></div>
                    
                    
                    <h6><b><?php esc_html_e('Notification to the staff regarding modified Appointment', 'pregnancy');?></b></h6>
                    <h5><?php esc_html_e('Subject','pregnancy');?></h5>
                    <input type="text" name="dttheme[pregnancy_appointment][modified_appointment_notification_to_staff_subject]" class="full-width" 
                    	value="<?php echo pregnancy_option('pregnancy_appointment', 'modified_appointment_notification_to_staff_subject'); ?>"/>
                    <h5><?php esc_html_e('Mesage','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'modified_appointment_notification_to_staff_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][modified_appointment_notification_to_staff_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                        
                    <div class="hr"></div>

                    <h6><b><?php esc_html_e('Notification to the staff regarding Deleted / Declined Appointment', 'pregnancy');?></b></h6>
                    <h5><?php esc_html_e('Subject','pregnancy');?></h5>
                    <input type="text" name="dttheme[pregnancy_appointment][deleted_appointment_notification_to_staff_subject]" class="full-width" 
                    	value="<?php echo pregnancy_option('pregnancy_appointment', 'deleted_appointment_notification_to_staff_subject'); ?>"/>
                    <h5><?php esc_html_e('Mesage','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'deleted_appointment_notification_to_staff_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][deleted_appointment_notification_to_staff_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                        
                    <div class="hr"></div>

                    <h6><b><?php esc_html_e('Evening notification with the next day agenda to Staff Member', 'pregnancy');?></b></h6>
                    <h5><?php esc_html_e('Subject','pregnancy');?></h5>
                    <input type="text" name="dttheme[pregnancy_appointment][agenda_to_staff_subject]" class="full-width" 
                    	value="<?php echo pregnancy_option('pregnancy_appointment', 'agenda_to_staff_subject'); ?>"/>
                    <h5><?php esc_html_e('Mesage','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'agenda_to_staff_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][agenda_to_staff_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                        
                    <div class="hr"></div>
                </div>

            	<div class="box-title"><h3><?php esc_html_e('To Customer','pregnancy');?></h3></div>
                <div class="box-content">
                
                	 <h6><b><?php esc_html_e('Notification to the client about new Appointment', 'pregnancy');?></b></h6>
                     <h5><?php esc_html_e('Subject','pregnancy');?></h5>
                     <input type="text" name="dttheme[pregnancy_appointment][appointment_notification_to_client_subject]" class="full-width" 
                    	value="<?php echo pregnancy_option('pregnancy_appointment', 'appointment_notification_to_client_subject'); ?>"/>
                     <h5><?php esc_html_e('Mesage','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'appointment_notification_to_client_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][appointment_notification_to_client_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                     <div class="hr"></div>
                     

                	 <h6><b><?php esc_html_e('Notification to the client regarding modified Appointment', 'pregnancy');?></b></h6>
                     <h5><?php esc_html_e('Subject','pregnancy');?></h5>
                     <input type="text" name="dttheme[pregnancy_appointment][modified_appointment_notification_to_client_subject]" class="full-width" 
                    	value="<?php echo pregnancy_option('pregnancy_appointment', 'modified_appointment_notification_to_client_subject'); ?>"/>
                     <h5><?php esc_html_e('Mesage','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'modified_appointment_notification_to_client_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][modified_appointment_notification_to_client_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                     <div class="hr"></div>

                	 <h6><b><?php esc_html_e('Notification to the client regarding Deleted / Declined Appointment', 'pregnancy');?></b></h6>
                     <h5><?php esc_html_e('Subject','pregnancy');?></h5>
                     <input type="text" name="dttheme[pregnancy_appointment][deleted_appointment_notification_to_client_subject]" class="full-width" 
                    	value="<?php echo pregnancy_option('pregnancy_appointment', 'deleted_appointment_notification_to_client_subject'); ?>"/>
                     <h5><?php esc_html_e('Mesage','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'deleted_appointment_notification_to_client_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][deleted_appointment_notification_to_client_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                     <div class="hr"></div>
                     
                     <h6><b><?php esc_html_e('Notification to the Success Message', 'pregnancy');?></b></h6>
                     <h5><?php esc_html_e('Message','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'success_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][success_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                     <div class="hr"></div>
                     
                     <h6><b><?php esc_html_e('Notification to the Failure Message', 'pregnancy');?></b></h6>
                     <h5><?php esc_html_e('Message','pregnancy');?></h5><?php
						$value = pregnancy_option('pregnancy_appointment', 'error_message'); ?>
                        <textarea class="fullwidth-textarea" name="dttheme[pregnancy_appointment][error_message]" rows="" cols=""><?php echo esc_html($value);?></textarea>
                     <div class="hr"></div>
                </div><?php
            else:?>
                <div class="box-title"><h3><?php esc_html_e('Warning','pregnancy');?></h3></div>
                <div class="box-content">
                    <p class="note"><?php esc_html_e("You have to install and activate the Design Themes Core plugin to use this module ..",'pregnancy');?></p>
                </div><?php    
            endif;?>
            </div>
        </div><!-- #my-notifications end -->    
             
    </div><!-- .bpanel-main-content end-->
</div><!-- #pregnancy appointment options end-->