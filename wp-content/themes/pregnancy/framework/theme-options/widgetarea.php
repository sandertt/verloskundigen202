<!-- #widgetarea -->
<div id="widgetarea" class="bpanel-content">

    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">
        <ul class="sub-panel"> 
            <li><a href="#tab1"><?php esc_html_e('Sidebar', 'pregnancy');?></a></li>
        </ul>
        
        <!-- #tab1-custom-widgetarea -->
        <div id="tab1" class="tab-content">

            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Create New Widget Area', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                    <p class="note"><?php esc_html_e("You can create widget areas here, and assign them in individual page / post", 'pregnancy');?></p>
                    <div class="bpanel-option-set">
                        <input type="button" data-for="custom" value="<?php esc_attr_e('Add New Widget Area', 'pregnancy');?>" class="black dttheme_add_widgetarea" />
                        <div class="hr_invisible"></div><?php
                        $widgets = pregnancy_option('widgetarea','custom');
                        $widgets = is_array($widgets) ? array_unique($widgets) : array();
                        $widgets = array_filter($widgets); ?>
                    </div>
                    <div class="bpanel-option-set">
                      <ul class="added-menu"><?php
                          foreach( $widgets as $k => $v){?>
                              <li>
                                <div class="item-bar">
                                  <span class="item-title"><?php esc_html_e('Widget Area:', 'pregnancy'); echo" $v";?></span>
                                  <span class="item-control"><a class="item-edit"><?php esc_html_e('Edit', 'pregnancy');?></a></span>
                                </div>
                                <div class="item-content" style="display: none;">
                                  <span><label><?php esc_html_e('Name', 'pregnancy');?></label><input type="text" name="dttheme[widgetarea][custom][]" class="social-link" value="<?php echo esc_attr($v);?>" /></span>
                                  <div class="remove-cancel-links">
                                    <span class="remove-item"><?php esc_html_e('Remove', 'pregnancy');?></span>
                                    <span class="meta-sep"> | </span>
                                    <span class="cancel-item"><?php esc_html_e('Cancel', 'pregnancy');?></span>
                                  </div>
                                </div>
                              </li><?php
                          }?>
                      </ul>

                      <ul class="sample-to-edit" style="display:none;">
                        <li>
                          <div class="item-bar">
                            <span class="item-title"><?php esc_html_e('Widget Area', 'pregnancy');?></span>
                            <span class="item-control"><a class="item-edit"><?php esc_html_e('Edit', 'pregnancy');?></a></span>
                          </div>

                          <div class="item-content">
                            <span><label><?php esc_html_e('Name', 'pregnancy');?></label><input type="text" class="social-link" /></span>
                            <div class="remove-cancel-links">
                              <span class="remove-item"><?php esc_html_e('Remove', 'pregnancy');?></span>
                              <span class="meta-sep"> | </span>
                              <span class="cancel-item"><?php esc_html_e('Cancel', 'pregnancy');?></span>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->

            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Widget Styles', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                    <div class="column one-half">
						<h6><?php esc_html_e('Widget Title Style', 'pregnancy');?></h6>
                        <div class="column one-fifth">
                            <select name="dttheme[widgetarea][wtitle-style]" class="dt-chosen-select"><?php
                                $selected = pregnancy_option('widgetarea','wtitle-style');
                                $wtitle_styles = array( '' => esc_html__('Choose any  type', 'pregnancy'), 'type1' => esc_html__('Type 1','pregnancy'), 'type2' => esc_html__('Type 2','pregnancy'), 'type3' => esc_html__('Type 3','pregnancy'),
								'type4' => esc_html__('Type 4','pregnancy'), 'type5' => esc_html__('Type 5','pregnancy'), 'type6' => esc_html__('Type 6','pregnancy'), 'type7' => esc_html__('Type 7','pregnancy'), 'type8' => esc_html__('Type 8','pregnancy'),
								'type9' => esc_html__('Type 9','pregnancy'), 'type10' => esc_html__('Type 10','pregnancy'), 'type11' => esc_html__('Type 11','pregnancy'), 'type12' => esc_html__('Type 12','pregnancy'), 'type13' => esc_html__('Type 13','pregnancy'));
                                foreach( $wtitle_styles as $wt => $bv ):
                                    echo "<option value='{$wt}'".selected($selected,$wt,false).">{$bv}</option>";
                                endforeach;?></select>
                        </div>
                        <div class="column four-fifth last">
                              <p class="note"><?php esc_html_e('Choose the style of widget title.', 'pregnancy');?></p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div><!--#tab1-custom-widgetarea end-->

    </div><!-- .bpanel-main-content end-->
</div><!-- #widgetarea end-->