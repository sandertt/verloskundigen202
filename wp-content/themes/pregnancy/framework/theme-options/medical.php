<!-- #pageoptions -->
<div id="medical" class="bpanel-content">
  <!-- .bpanel-main-content -->
  <div class="bpanel-main-content">

    <ul class="sub-panel"> 
      <li><a href="#medical"><?php esc_html_e('Medical Add-on', 'pregnancy');?></a></li>
    </ul>

    <!-- #medical - Medical Custom Post Type -->
    <div id="medical" class="tab-content">

      <!-- .bpanel-box -->
      <div class="bpanel-box">

        <!-- Custom Fields -->
          <div class="box-title">
            <h3><?php esc_html_e('Doctor Custom Fields', 'pregnancy');?></h3>
          </div>

          <div class="box-content">
            <div class="portfolio-custom-fields">
              <input type="button" class="black add-custom-field" value="<?php esc_attr_e('Add New Field', 'pregnancy');?>" />
              <div class="hr_invisible"> </div><?php
                $custom_fields = pregnancy_option("pageoptions","doctor-custom-fields");
                $custom_fields = is_array($custom_fields) ? array_filter($custom_fields) : array();
                $custom_fields = array_unique($custom_fields);

                foreach( $custom_fields as $field ) {?>
                  <div class="custom-field-container">
                    <div class="hr_invisible"> </div>
                    <input class="medium" type="text" name="<?php echo "dttheme[pageoptions][doctor-custom-fields][]";?>" value="<?php echo esc_attr($field);?>">
                    <a href='' class='remove-custom-field'><?php esc_html_e('Remove', 'pregnancy');?></a>
                  </div><?php
                }?>

                <div class="clone hidden">
                  <div class="custom-field-container">
                    <div class="hr_invisible"> </div>
                    <input class="medium" type="text" name="<?php echo "dttheme[pageoptions][doctor-custom-fields][]";?>" value="">
                    <a href='' class='remove-custom-field'><?php esc_html_e('Remove', 'pregnancy');?></a>
                  </div>
                </div>
            </div>
          </div>
        <!-- Custom Fields -->

        <!-- Taxonomy Page Settings -->
        <!-- Taxonomy Page Settings -->

        <!-- Permalinks Settings -->
          <div class="box-title">
            <h3><?php esc_html_e('Permalinks', 'pregnancy');?></h3>
          </div>
          <div class="box-content">
            <div class="column one-third"><label><?php esc_html_e('Single Doctor slug', 'pregnancy');?></label></div>
            <div class="column two-third last">
              <input name="dttheme[pageoptions][single-doctor-slug]" type="text" class="medium" value="<?php echo trim(stripslashes(pregnancy_option('pageoptions','single-doctor-slug')));?>" />
              <p class="note"><?php esc_html_e('Do not use characters not allowed in links. Use, eg. doctor-item <br> <b>After made changes save permalinks.</b>', 'pregnancy');?></p>
            </div>
            <div class="hr"></div>


            <div class="column one-half">
              <label><?php esc_html_e('Singular Doctor Name', 'pregnancy');?></label>
              <div class="clear"></div>
              <input name="dttheme[pageoptions][singular-doctor-name]" type="text" class="medium" value="<?php echo trim(stripslashes(pregnancy_option('pageoptions','singular-doctor-name')));?>" />
              <p class="note"><?php esc_html_e('By default "Doctor", save options & reload.', 'pregnancy');?></p>
              <div class="hr"></div>
            </div>

            <div class="column one-half last">
              <label><?php esc_html_e('Plural Doctor Name', 'pregnancy');?></label>
              <div class="clear"></div>
              <input name="dttheme[pageoptions][plural-doctor-name]" type="text" class="medium" value="<?php echo trim(stripslashes(pregnancy_option('pageoptions','plural-doctor-name')));?>" />
              <p class="note"><?php esc_html_e('By default "Doctors". save options & reload.', 'pregnancy');?></p>
              <div class="hr"></div>
            </div>

            <div class="column one-half">
              <label><?php esc_html_e('Singular Doctor Deparetment Name', 'pregnancy');?></label>
              <div class="clear"></div>
              <input name="dttheme[pageoptions][singular-doctor-tax-name]" type="text" class="medium" value="<?php echo trim(stripslashes(pregnancy_option('pageoptions','singular-doctor-tax-name')));?>" />
              <p class="note"><?php esc_html_e('By default "Deparetment". save options & reload.', 'pregnancy');?></p>
              <div class="hr"></div>
            </div>

            <div class="column one-half last">
              <label><?php esc_html_e('Plural Doctor Deparetment Name', 'pregnancy');?></label>
              <div class="clear"></div>
              <input name="dttheme[pageoptions][plural-doctor-tax-name]" type="text" class="medium" value="<?php echo trim(stripslashes(pregnancy_option('pageoptions','plural-doctor-tax-name')));?>" />
              <p class="note"><?php esc_html_e('By default Departments". save options & reload.', 'pregnancy');?></p>
              <div class="hr"></div>
            </div>
          </div>
        <!-- Permalinks Settings -->
      </div><!-- .bpanel-box -->
    </div><!-- #medical - Medical Custom Post Type -->
  </div><!-- .bpanel-main-content end-->
</div><!-- #pageoptions end-->