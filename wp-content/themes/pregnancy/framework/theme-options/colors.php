<!-- #colors -->
<div id="colors" class="bpanel-content">

    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">
        <ul class="sub-panel"> 
            <li><a href="#tab1"><?php esc_html_e('General', 'pregnancy');?></a></li>
            <li><a href="#tab2"><?php esc_html_e('Header', 'pregnancy');?></a></li>
			<li><a href="#tab3"><?php esc_html_e('Menu', 'pregnancy');?></a></li>
            <li><a href="#tab4"><?php esc_html_e('Content', 'pregnancy');?></a></li>
            <li><a href="#tab5"><?php esc_html_e('Footer', 'pregnancy');?></a></li>
            <li><a href="#tab6"><?php esc_html_e('Heading', 'pregnancy');?></a></li>
        </ul>
        
        <!-- #tab1-general -->
        <div id="tab1" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Skin', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                    <div class="column one-third"><label><?php esc_html_e('Theme Skin', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <select id="dttheme-skin-color" name="dttheme[colors][theme-skin]" class="medium dt-chosen-select skin-types">
                        	<optgroup label="Custom">
								<option value="custom"><?php esc_html_e('Custom Skin', 'pregnancy'); ?></option>
							</optgroup>
							<optgroup label="Skins"><?php
								foreach(pregnancy_getfolders(PREGNANCY_THEME_DIR."/css/skins") as $skin):
									$s = selected(pregnancy_option('colors','theme-skin'),$skin,false);
									echo "<option $s >$skin</option>";
								endforeach;?>
                            </optgroup>    
                        </select>
                        <p class="note"><?php esc_html_e('Choose one of the predefined styles or set your own colors.', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Body Background Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][body-bgcolor]";
						$value =  (pregnancy_option('colors','body-bgcolor') != NULL) ? pregnancy_option('colors','body-bgcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom background color of the body.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <?php $panelvisible = ( pregnancy_option('colors','theme-skin') == 'custom' ) ? 'style="display:block"' : 'style="display:none"'; ?>

					<div class="custom-skin-panel" <?php echo esc_attr($panelvisible);?>>
                        <div class="column one-third"><label><?php esc_html_e('Default Color', 'pregnancy');?></label></div>
                        <div class="column two-third last"><?php
                            $name  =  "dttheme[colors][custom-default]";
                            $value =  (pregnancy_option('colors','custom-default') != NULL) ? pregnancy_option('colors','custom-default') :"";
                            pregnancy_admin_color_picker_two($name,$value);?>
                            <p class="note"><?php esc_html_e('Important: This option can be used only with the <b>"Custom Skin"</b>.', 'pregnancy');?></p>
                        </div>
                        <div class="hr"></div>
                        
                    </div>    

                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->
        </div><!--#tab1-general end-->

        <!-- #tab2-header -->
        <div id="tab2" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Header', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                    <div class="column one-half">
                    	<label><?php esc_html_e('Header BG Color', 'pregnancy');?></label>
                        <div class="clear"></div><?php
						$name  =  "dttheme[colors][header-bgcolor]";
						$value =  (pregnancy_option('colors','header-bgcolor') != NULL) ? pregnancy_option('colors','header-bgcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom background color of the header.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>

					<div class="column one-half last">
						<div class="bpanel-option-set">
	                        <?php echo pregnancy_admin_jqueryuislider( esc_html__("Background opacity", 'pregnancy'), "dttheme[colors][header-bgcolor-opacity]",
                                                                          pregnancy_option("colors","header-bgcolor-opacity"),"");?>
                        </div>
                        <p class="note"><?php esc_html_e('You can adjust opacity of the header BG color here.', 'pregnancy');?></p>
                    </div>
					<div class="hr"></div>
                </div><!-- .box-content -->
                
            </div><!-- .bpanel-box end -->            
        </div><!--#tab2-header end-->

        <!-- #tab3-menu -->
        <div id="tab3" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Menu', 'pregnancy');?></h3>
                </div>

                <div class="box-content">
                    <div class="column one-half">
                    	<label><?php esc_html_e('Menu BG Color', 'pregnancy');?></label>
                        <div class="clear"></div><?php
						$name  =  "dttheme[colors][menu-bgcolor]";
						$value =  (pregnancy_option('colors','menu-bgcolor') != NULL) ? pregnancy_option('colors','menu-bgcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom background color of the menu.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>

					<div class="column one-half last">
						<div class="bpanel-option-set">
	                        <?php echo pregnancy_admin_jqueryuislider( esc_html__("Background opacity", 'pregnancy'), "dttheme[colors][menu-bgcolor-opacity]",
                                                                          pregnancy_option("colors","menu-bgcolor-opacity"),"");?>
                        </div>
                        <p class="note"><?php esc_html_e('You can adjust opacity of the menu BG color here.', 'pregnancy');?></p>
                    </div>
					<div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Menu Link Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][menu-linkcolor]";
						$value =  (pregnancy_option('colors','menu-linkcolor') != NULL) ? pregnancy_option('colors','menu-linkcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the menu links.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Menu Hover Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][menu-hovercolor]";
						$value =  (pregnancy_option('colors','menu-hovercolor') != NULL) ? pregnancy_option('colors','menu-hovercolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the hover menu links.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Menu Link Active Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][menu-activecolor]";
						$value =  (pregnancy_option('colors','menu-activecolor') != NULL) ? pregnancy_option('colors','menu-activecolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the active menu links.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->            
        </div><!--#tab3-menu end-->

        <!-- #tab4-content -->
        <div id="tab4" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Content', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                    <div class="column one-third"><label><?php esc_html_e('Text Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][content-text-color]";
						$value =  (pregnancy_option('colors','content-text-color') != NULL) ? pregnancy_option('colors','content-text-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the body content text.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Link Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][content-link-color]";
						$value =  (pregnancy_option('colors','content-link-color') != NULL) ? pregnancy_option('colors','content-link-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the body content link.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Link Hover Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][content-link-hcolor]";
						$value =  (pregnancy_option('colors','content-link-hcolor') != NULL) ? pregnancy_option('colors','content-link-hcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom hover color of the body content link.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->            
        </div><!--#tab4-content end-->

        <!-- #tab5-footer -->
        <div id="tab5" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Footer', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                    <div class="column one-half">
                    	<label><?php esc_html_e('Footer Background Color', 'pregnancy');?></label>
                        <div class="clear"></div><?php
						$name  =  "dttheme[colors][footer-bgcolor]";
						$value =  (pregnancy_option('colors','footer-bgcolor') != NULL) ? pregnancy_option('colors','footer-bgcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the footer background.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>

					<div class="column one-half last">
						<div class="bpanel-option-set">
	                        <?php echo pregnancy_admin_jqueryuislider( esc_html__("Background opacity", 'pregnancy'), "dttheme[colors][footer-bgcolor-opacity]",
                                                                          pregnancy_option("colors","footer-bgcolor-opacity"),"");?>
                        </div>
                        <p class="note"><?php esc_html_e('You can adjust opacity of the footer BG color here.', 'pregnancy');?></p>
                    </div>
					<div class="hr"></div>

                    <div class="column one-half">
                    	<label><?php esc_html_e('Copyright Section BG Color', 'pregnancy');?></label>
                        <div class="clear"></div><?php
						$name  =  "dttheme[colors][copyright-bgcolor]";
						$value =  (pregnancy_option('colors','copyright-bgcolor') != NULL) ? pregnancy_option('colors','copyright-bgcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the copyright section background.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>

					<div class="column one-half last">
						<div class="bpanel-option-set">
	                        <?php echo pregnancy_admin_jqueryuislider( esc_html__("Background opacity", 'pregnancy'), "dttheme[colors][copyright-bgcolor-opacity]",
                                                                          pregnancy_option("colors","copyright-bgcolor-opacity"),"");?>
                        </div>
                        <p class="note"><?php esc_html_e('You can adjust opacity of the copyright section BG color here.', 'pregnancy');?></p>
                    </div>
					<div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Footer Text Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][footer-text-color]";
						$value =  (pregnancy_option('colors','footer-text-color') != NULL) ? pregnancy_option('colors','footer-text-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the footer text elements.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Footer Link Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][footer-link-color]";
						$value =  (pregnancy_option('colors','footer-link-color') != NULL) ? pregnancy_option('colors','footer-link-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the footer links.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Footer Hover Link Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][footer-link-hcolor]";
						$value =  (pregnancy_option('colors','footer-link-hcolor') != NULL) ? pregnancy_option('colors','footer-link-hcolor') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom hover color of the footer links.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Footer Heading Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][footer-heading-color]";
						$value =  (pregnancy_option('colors','footer-heading-color') != NULL) ? pregnancy_option('colors','footer-heading-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the footer headings.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->            
        </div><!--#tab5-footer end-->

        <!-- #tab6-heading -->
        <div id="tab6" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Heading', 'pregnancy');?></h3>
                </div>
                
                <div class="box-content">
                    <div class="column one-third"><label><?php esc_html_e('Heading H1 Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][heading-h1-color]";
						$value =  (pregnancy_option('colors','heading-h1-color') != NULL) ? pregnancy_option('colors','heading-h1-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the heading tag h1.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>
                    
                    <div class="column one-third"><label><?php esc_html_e('Heading H2 Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][heading-h2-color]";
						$value =  (pregnancy_option('colors','heading-h2-color') != NULL) ? pregnancy_option('colors','heading-h2-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the heading tag h2.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Heading H3 Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][heading-h3-color]";
						$value =  (pregnancy_option('colors','heading-h3-color') != NULL) ? pregnancy_option('colors','heading-h3-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the heading tag h3.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Heading H4 Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][heading-h4-color]";
						$value =  (pregnancy_option('colors','heading-h4-color') != NULL) ? pregnancy_option('colors','heading-h4-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the heading tag h4.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Heading H5 Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][heading-h5-color]";
						$value =  (pregnancy_option('colors','heading-h5-color') != NULL) ? pregnancy_option('colors','heading-h5-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the heading tag h5.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Heading H6 Color', 'pregnancy');?></label></div>
                    <div class="column two-third last"><?php
						$name  =  "dttheme[colors][heading-h6-color]";
						$value =  (pregnancy_option('colors','heading-h6-color') != NULL) ? pregnancy_option('colors','heading-h6-color') :"";
                        pregnancy_admin_color_picker_two($name,$value);?>
                        <p class="note"><?php esc_html_e('Pick a custom color of the heading tag h6.(e.g. #a314a3)', 'pregnancy');?></p>
                    </div>
                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->            
        </div><!--#tab6-heading end-->

    </div><!-- .bpanel-main-content end-->
</div><!-- #colors end-->