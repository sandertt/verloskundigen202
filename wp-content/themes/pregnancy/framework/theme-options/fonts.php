<!-- #fonts -->
<div id="fonts" class="bpanel-content">

    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">
        <ul class="sub-panel">
            <li><a href="#tab1"><?php esc_html_e('Font Family', 'pregnancy');?></a></li>
            <li><a href="#tab2"><?php esc_html_e('Font Size', 'pregnancy');?></a></li>
            <li><a href="#tab3"><?php esc_html_e('Font Weight', 'pregnancy');?></a></li>
            <li><a href="#tab4"><?php esc_html_e('Letter Spacing', 'pregnancy');?></a></li>
            <li><a href="#tab5"><?php esc_html_e('Others', 'pregnancy');?></a></li>
        </ul>

        <!-- #tab1-font-family -->
        <div id="tab1" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Font Family', 'pregnancy');?></h3>
                </div>
				<?php $fonts = pregnancy_fonts(); ?>
                <div class="box-content">
                    <div class="column one-third"><label><?php esc_html_e('Content Font', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <select id="dttheme-fonts-content" name="dttheme[fonts][content-font]" class="medium dt-chosen-select"><?php
							#system fonts
							echo '<optgroup label="'. esc_attr__('System', 'pregnancy') .'">';
							foreach ( $fonts['system'] as $font ) {
								echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','content-font'), $font, false).'>'. $font .'</option>';
							}
							echo '</optgroup>';

							#custom font | uploaded in theme options
							if( key_exists( 'custom', $fonts ) ){
								echo '<optgroup label="'. esc_attr__('Custom (Uploaded below)', 'pregnancy') .'">';
								foreach ( $fonts['custom'] as $font ) {
									echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','content-font'), $font, false).'>'. $font .'</option>';
								}
								echo '</optgroup>';
							}

							#google fonts | all
							echo '<optgroup label="'. esc_attr__('Google Fonts', 'pregnancy') .'">';
							foreach ( $fonts['all'] as $font ) {
								echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','content-font'), $font, false).'>'. $font .'</option>';
							}
							echo '</optgroup>'; ?>
                        </select>
                        <p class="note"><?php esc_html_e('All theme texts except headings and menu.', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Menu Font', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <select id="dttheme-fonts-menu" name="dttheme[fonts][menu-font]" class="medium dt-chosen-select"><?php
							#system fonts
							echo '<optgroup label="'. esc_attr__('System', 'pregnancy') .'">';
							foreach ( $fonts['system'] as $font ) {
								echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','menu-font'), $font, false).'>'. $font .'</option>';
							}
							echo '</optgroup>';

							#custom font | uploaded in theme options
							if( key_exists( 'custom', $fonts ) ){
								echo '<optgroup label="'. esc_attr__('Custom (Uploaded below)', 'pregnancy') .'">';
								foreach ( $fonts['custom'] as $font ) {
									echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','menu-font'), $font, false).'>'. str_replace('#', '', $font) .'</option>';
								}
								echo '</optgroup>';
							}

							#google fonts | all
							echo '<optgroup label="'. esc_attr__('Google Fonts', 'pregnancy') .'">';
							foreach ( $fonts['all'] as $font ) {
								echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','menu-font'), $font, false).'>'. $font .'</option>';
							}
							echo '</optgroup>'; ?>
                        </select>
                        <p class="note"><?php esc_html_e('The selected font can apply for header menu.', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_attr_e('Page Title Font', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <select id="dttheme-fonts-pagetitle" name="dttheme[fonts][pagetitle-font]" class="medium dt-chosen-select"><?php
							#system fonts
							echo '<optgroup label="'. esc_html_e('System', 'pregnancy') .'">';
							foreach ( $fonts['system'] as $font ) {
								echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','pagetitle-font'), $font, false).'>'. $font .'</option>';
							}
							echo '</optgroup>';

							#custom font | uploaded in theme options
							if( key_exists( 'custom', $fonts ) ){
								echo '<optgroup label="'. esc_attr__('Custom (Uploaded below)', 'pregnancy') .'">';
								foreach ( $fonts['custom'] as $font ) {
									echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','pagetitle-font'), $font, false).'>'. str_replace('#', '', $font) .'</option>';
								}
								echo '</optgroup>';
							}

							#google fonts | all
							echo '<optgroup label="'. esc_attr__('Google Fonts', 'pregnancy') .'">';
							foreach ( $fonts['all'] as $font ) {
								echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts','pagetitle-font'), $font, false).'>'. $font .'</option>';
							}
							echo '</optgroup>'; ?>
                        </select>
                        <p class="note"><?php esc_html_e('The selected font can apply for page titles.', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

			  <?php	for( $i = 1; $i <= 6; $i++) :
			  			$class = (($i % 2) == 0) ? 'last' : ''; ?>
                        <div class="column one-half <?php echo esc_attr($class); ?>">
                        	<label><?php echo sprintf(esc_html__('Heading H%s Font', 'pregnancy'), $i);?></label>
                            <div class="clear"></div>
                            <select id="dttheme-fonts-head<?php echo esc_attr($i);?>" name="dttheme[fonts][h<?php echo esc_attr($i);?>-font]" class="medium dt-chosen-select"><?php
                                #system fonts
                                echo '<optgroup label="'. esc_attr__('System', 'pregnancy') .'">';
                                foreach ( $fonts['system'] as $font ) {
                                    echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts', 'h'.$i.'-font'), $font, false).'>'. $font .'</option>';
                                }
                                echo '</optgroup>';

                                #custom font | uploaded in theme options
                                if( key_exists( 'custom', $fonts ) ){
                                    echo '<optgroup label="'. esc_attr__('Custom (Uploaded below)', 'pregnancy') .'">';
                                    foreach ( $fonts['custom'] as $font ) {
                                        echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts', 'h'.$i.'-font'), $font, false).'>'. str_replace('#', '', $font) .'</option>';
                                    } 
                                    echo '</optgroup>';
                                }

                                #google fonts | all
                                echo '<optgroup label="'. esc_attr__('Google Fonts', 'pregnancy') .'">';
                                foreach ( $fonts['all'] as $font ) {
                                    echo '<option value="'. $font .'"'.selected(pregnancy_option('fonts', 'h'.$i.'-font'), $font, false).'>'. $font .'</option>';
                                }
                                echo '</optgroup>'; ?>
                            </select>
                            <p class="note"><?php echo sprintf(esc_html__('The selected font can apply for heading H%s.', 'pregnancy'), $i);?></p>
                            <div class="hr"></div>
                        </div><?php
					endfor; ?>

                    <div class="column one-third"><label><?php esc_html_e('Google Font Style & Weight', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                    	<ul class="checkbox-list"><?php
							#google font styles
							$font_styles = pregnancy_font_style();
							$dtfonts = pregnancy_option('fonts', 'font-style');
							$dtfonts = !empty($dtfonts) ? $dtfonts : array();
							foreach($font_styles as $key => $option):
								$checked = ( in_array(trim($key), $dtfonts) ) ? ' checked="checked"' : ''; ?>
								<li><label><input type="checkbox" name="dttheme[fonts][font-style][]" value="<?php echo esc_attr($key);?>" <?php echo esc_attr($checked); ?> /><?php echo esc_html($option); ?></label></li><?php
							endforeach;	?>
                        </ul>
                        <p class="note"><?php esc_html_e('Some of the fonts in the Google Font Directory support multiple styles.', 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Google Font Subset', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                    	<input type="text" class="large" name="dttheme[fonts][font-subset]" value="<?php echo pregnancy_option('fonts','font-subset');?>" />
                        <p class="note"><?php esc_html_e('Specify which subsets should be downloaded. Multiple subsets should be separated with comma (,)', 'pregnancy');?></p>
                    </div>
                </div><!-- .box-content -->

                <div class="box-title">
                    <h3><?php esc_html_e('Custom Fonts', 'pregnancy');?></h3>
                </div>

                <div class="box-content">
                    <div class="column one-third"><label><?php esc_html_e('Custom Font Name', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                    	<input type="text" class="large" name="dttheme[fonts][customfont-name]" value="<?php echo pregnancy_option('fonts','customfont-name');?>" />
                        <p class="note"><?php esc_html_e('Please use only letters or spaces, eg. Patua One', 'pregnancy');?></p>
                    </div>
					<div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font (.woff)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont-woff]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont-woff');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .woff file for above custom font.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font (.ttf)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont-ttf]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont-ttf');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .ttf file for above custom font.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font (.svg)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont-svg]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont-svg');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .svg file for above custom font.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font (.eot)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont-eot]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont-eot');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .eot file for above custom font.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>
                    
                    <div class="column one-third"><label><?php esc_html_e('Custom Font 2 Name', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                    	<input type="text" class="large" name="dttheme[fonts][customfont2-name]" value="<?php echo pregnancy_option('fonts','customfont2-name');?>" />
                        <p class="note"><?php esc_html_e('Please use only letters or spaces, eg. Patua One', 'pregnancy');?></p>
                    </div>
					<div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font 2 (.woff)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont2-woff]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont2-woff');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .woff file for above custom font 2.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font 2 (.ttf)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont2-ttf]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont2-ttf');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .ttf file for above custom font 2.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font 2 (.svg)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont2-svg]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont2-svg');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .svg file for above custom font 2.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>

                    <div class="column one-third"><label><?php esc_html_e('Custom Font 2 (.eot)', 'pregnancy');?></label></div>
                    <div class="column two-third last">
                        <input id="dttheme-favicon" name="dttheme[fonts][customfont2-eot]" type="text" class="uploadfield medium" value="<?php echo pregnancy_option('fonts','customfont2-eot');?>" />
                        <input type="button" value="<?php esc_attr_e('Upload', 'pregnancy');?>" class="upload_image_button" />
                        <input type="button" value="<?php esc_attr_e('Remove', 'pregnancy');?>" class="upload_image_reset" />
	                    <p class="note"> <?php esc_html_e('Upload .eot file for above custom font 2.', 'pregnancy');?>  </p>
					</div>
                    <div class="hr"></div>                    
                </div>
            </div><!-- .bpanel-box end -->
        </div><!--#tab1-font-family end-->

        <!-- #tab2-font-size -->
        <div id="tab2" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Font Size', 'pregnancy');?></h3>
                </div>

                <div class="box-content">
					<div class="column one-half"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Content', 'pregnancy'), "dttheme[fonts][content-font-size]", pregnancy_option('fonts','content-font-size')); ?>
						<p class="note"> <?php esc_html_e('This font size will be used for all theme texts.', 'pregnancy');?>  </p>
                    </div>
					<div class="column one-half last"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Main Menu', 'pregnancy'), "dttheme[fonts][menu-font-size]", pregnancy_option('fonts','menu-font-size')); ?>
						<p class="note"> <?php esc_html_e('This font size will be used for top level only.', 'pregnancy');?>  </p>
                    </div>
					<div class="hr"></div>

					<div class="column one-half"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Heading H1', 'pregnancy'), "dttheme[fonts][h1-font-size]", pregnancy_option('fonts','h1-font-size')); ?>
						<p class="note"> <?php esc_html_e('This is Heading I font size.', 'pregnancy');?>  </p>
                    </div>
					<div class="column one-half last"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Heading H2', 'pregnancy'), "dttheme[fonts][h2-font-size]", pregnancy_option('fonts','h2-font-size')); ?>
						<p class="note"> <?php esc_html_e('This is Heading II font size.', 'pregnancy');?>  </p>
                    </div>
					<div class="hr"></div>

					<div class="column one-half"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Heading H3', 'pregnancy'), "dttheme[fonts][h3-font-size]", pregnancy_option('fonts','h3-font-size')); ?>
						<p class="note"> <?php esc_html_e('This is Heading III font size.', 'pregnancy');?>  </p>
                    </div>

					<div class="column one-half last"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Heading H4', 'pregnancy'), "dttheme[fonts][h4-font-size]", pregnancy_option('fonts','h4-font-size')); ?>
						<p class="note"> <?php esc_html_e('This is Heading IV font size.', 'pregnancy');?>  </p>
                    </div>
					<div class="hr"></div>

					<div class="column one-half"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Heading H5', 'pregnancy'), "dttheme[fonts][h5-font-size]", pregnancy_option('fonts','h5-font-size')); ?>
						<p class="note"> <?php esc_html_e('This is Heading V font size.', 'pregnancy');?>  </p>
                    </div>
					<div class="column one-half last"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Heading H6', 'pregnancy'), "dttheme[fonts][h6-font-size]", pregnancy_option('fonts','h6-font-size')); ?>
						<p class="note"> <?php esc_html_e('This is Heading VI font size.', 'pregnancy');?>  </p>
                    </div>

					<div class="column one-half"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Footer Content', 'pregnancy'), "dttheme[fonts][footer-font-size]", pregnancy_option('fonts','footer-font-size')); ?>
						<p class="note"> <?php esc_html_e('This font size will be used for all footer texts.', 'pregnancy');?>  </p>
                    </div>
                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->
        </div><!--#tab2-font-size end-->

        <!-- #tab3-font-weight -->
        <div id="tab3" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Font Weight', 'pregnancy');?></h3>
                    <?php $fweight = array('100' => '100', '200' => '200', '300' => '300', '400' => '400', '500' => '500', '600' => '600', '700' => '700', '800' => '800', '900' => '900', 'bold' => 'Bold', 'bolder' => 'Bolder', 'lighter' => 'Lighter', 'inherit' => 'Inherit'); ?>
                </div>

                <div class="box-content">
					<div class="column one-third">
                        <label><?php esc_html_e("Menu", 'pregnancy');?></label>
                    </div>
                    <div class="column two-third last">
                        <select id="dttheme-fonts-menu-weight" name="dttheme[fonts][menu-weight]" class="medium dt-chosen-select">
							<option value="normal"><?php esc_html_e('Normal', 'pregnancy');?></option><?php
                            foreach ( $fweight as $key => $w ) {
                                echo '<option value="'. $key .'"'.selected(pregnancy_option('fonts', 'menu-weight'), $key, false).'>'. $w .'</option>';
                            } ?>
                        </select>
                        <p class="note"><?php esc_html_e("The selected font weight can apply for menu links.", 'pregnancy');?></p>
                    </div>
                    <div class="hr"></div>

                <?php for( $i = 1; $i <= 6; $i++) :
			  			$class = (($i % 2) == 0) ? 'last' : ''; ?>

                        <div class="column one-half <?php echo esc_attr($class); ?>">
                            <label><?php echo sprintf(esc_html__('Heading H%s', 'pregnancy'), $i);?></label>
                            <div class="clear"></div>
                            <select id="dttheme-fonts-h<?php echo esc_attr($i);?>-weight" name="dttheme[fonts][h<?php echo esc_attr($i);?>-weight]" class="medium dt-chosen-select">
                                <option value="normal"><?php esc_html_e('Normal', 'pregnancy');?></option><?php
                                foreach ( $fweight as $key => $w ) {
                                    echo '<option value="'. $key .'"'.selected(pregnancy_option('fonts', 'h'.$i.'-weight'), $key, false).'>'. $w .'</option>';
                                } ?>
                            </select>
                            <p class="note"><?php echo sprintf(esc_html__('The selected font weight can apply for H%s texts.', 'pregnancy'), $i);?></p>
                            <div class="hr"></div>
                        </div>
                <?php endfor; ?>

				</div>
            </div>
        </div>

        <!-- #tab4-letter-spacing -->
        <div id="tab4" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Letter Spacing', 'pregnancy');?></h3>
                    <?php $lspacing = array('-3px' => '-3px', '-2px' => '-2px', '-1px' => '-1px', '-0.75px' => '-0.75px', '-0.5px' => '-0.5px', '-0.25px' => '-0.25px', '-0.1px' => '-0.1px', '-0.05px' => '-0.05px', '0px' => '0px', '0.05px' => '0.05px', '0.1px' => '0.1px', '0.25px' => '0.25px', '0.5px' => '0.5px', '0.75px' => '0.75px','0.8px' => '0.8px', '1px' => '1px', '2px' => '2px', '3px' => '3px'); ?>
                </div>

                <div class="box-content">
					<div class="column one-third">
                        <label><?php esc_html_e("Menu", 'pregnancy');?></label>
                    </div>
                    <div class="column two-third last">
                        <select id="dttheme-fonts-menu-letter-spacing" name="dttheme[fonts][menu-letter-spacing]" class="medium dt-chosen-select">
                            <option value=""><?php esc_html_e('Choose any one', 'pregnancy');?></option><?php
                            foreach ( $lspacing as $key => $l ) {
                                echo '<option value="'. $key .'"'.selected(pregnancy_option('fonts', 'menu-letter-spacing'), $key, false).'>'. $l .'</option>';
                            } ?>
                        </select>
						<p class="note"> <?php esc_html_e('This letter spacing will be used for all menu links.', 'pregnancy');?>  </p>
                    </div>
					<div class="hr"></div>

                <?php for( $i = 1; $i <= 6; $i++) :
			  			$class = (($i % 2) == 0) ? 'last' : ''; ?>

                        <div class="column one-half <?php echo esc_attr($class); ?>">
                            <label><?php echo sprintf(esc_html__('Heading H%s', 'pregnancy'), $i);?></label>
                            <div class="clear"></div>
                            <select id="dttheme-fonts-h<?php echo esc_attr($i);?>-letter-spacing" name="dttheme[fonts][h<?php echo esc_attr($i);?>-letter-spacing]" class="medium dt-chosen-select">
                                <option value=""><?php esc_html_e('Choose any one', 'pregnancy');?></option><?php
                                foreach ( $lspacing as $key => $l ) {
                                    echo '<option value="'. $key .'"'.selected(pregnancy_option('fonts', 'h'.$i.'-letter-spacing'), $key, false).'>'. $l .'</option>';
                                } ?>
                            </select>
                            <p class="note"><?php echo sprintf( esc_html__('This is Heading H%s letter spacing.', 'pregnancy'), $i);?></p>
                            <div class="hr"></div>
                        </div>
                <?php endfor; ?>

                </div><!-- .box-content -->
            </div><!-- .bpanel-box end -->
        </div><!--#tab4-letter-sspacing end-->

        <!-- #tab5-others -->
        <div id="tab5" class="tab-content">
            <!-- .bpanel-box -->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php esc_html_e('Line Height', 'pregnancy');?></h3>
                </div>

                <div class="box-content">
					<div class="column one-column"><?php
						pregnancy_admin_jqueryuislider(esc_html__('Body Line Height', 'pregnancy'), "dttheme[fonts][body-line-height]", pregnancy_option('fonts','body-line-height')); ?>
						<p class="note"> <?php esc_html_e('This line height will be applied for body contents.', 'pregnancy');?>  </p>
                    </div>
                </div>
            </div>        
        </div>
    </div><!-- .bpanel-main-content end-->
</div><!-- #fonts end-->