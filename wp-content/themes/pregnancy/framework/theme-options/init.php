<?php
/* ---------------------------------------------------------------------------
 * Create menu for theme options panel
 * --------------------------------------------------------------------------- */
function pregnancy_create_admin_menu() {
	/**
	 * Creates main options page.
	*/
	add_theme_page( PREGNANCY_THEME_NAME . esc_html__(' Theme Options', 'pregnancy'), PREGNANCY_THEME_NAME . esc_html__(' Options', 'pregnancy'), 'manage_options', 'pregnancy-opts',  'pregnancy_options_page' );
}
add_action('admin_menu', 'pregnancy_create_admin_menu');
require_once(PREGNANCY_THEME_DIR . '/framework/theme-options/settings.php');

/* ---------------------------------------------------------------------------
 * Create function to init dttheme options
 * --------------------------------------------------------------------------- */
add_action('admin_init', 'pregnancy_admin_options_init', 1);
function pregnancy_admin_options_init() {
	register_setting(PREGNANCY_SETTINGS, PREGNANCY_SETTINGS);
	add_option(PREGNANCY_SETTINGS, pregnancy_default_option());

	if (isset($_POST['dttheme-option-save'])) :
		pregnancy_ajax_option_save();
	endif;

	if (isset($_POST['dttheme']['reset'])) :
		delete_option(PREGNANCY_SETTINGS);
		update_option(PREGNANCY_SETTINGS, pregnancy_default_option()); # To set Default options
		wp_redirect(admin_url('admin.php?page=parent&reset=true'));
		exit;
	endif;
}

function pregnancy_ajax_option_save() {
	$data = $_POST;
	check_ajax_referer('dttheme_wpnonce', 'dttheme_admin_wpnonce');

	unset($data['_wp_http_referer'], $data['_wpnonce'], $data['action']);
	unset($data['dttheme_admin_wpnonce'], $data['dttheme-option-save'], $data['option_page']);

	$msg = array(
		'success' => false, 
		'message' => esc_html__('Error: Options not saved, please try again.', 'pregnancy')
	);

	$data = array_filter($data['dttheme']);

	if (get_option(PREGNANCY_SETTINGS) != $data) {
		if (update_option(PREGNANCY_SETTINGS, $data))
			$msg = array(
				'success' => 'options_saved',
				'message' => esc_html__('Options Saved.', 'pregnancy')
			);
	} else {
		$msg = array(
			'success' => true,
			'message' => esc_html__('Options Saved.', 'pregnancy')
		);
	}

	$echo = json_encode($msg);
	@header('Content-Type: application/json; charset='.get_option('blog_charset'));
	echo !empty($echo) ? $echo : '';
	exit;
}

/* ---------------------------------------------------------------------------
 * Backup And Restore theme options
 * --------------------------------------------------------------------------- */
add_action('wp_ajax_pregnancy_backup_and_restore_action', 'pregnancy_backup_and_restore_action');
function pregnancy_backup_and_restore_action() {
	
	$save_type = $_REQUEST['type'];
	
	if ($save_type == 'backup_options') :
	
		$data = array(
			'general' => pregnancy_option('general'),
			'layout' => pregnancy_option('layout'),
			'social' => pregnancy_option('social'),
			'pageoptions' => pregnancy_option('pageoptions'),
			'woo' => pregnancy_option('woo'),
			'colors' => pregnancy_option('colors'),
			'fonts' => pregnancy_option('fonts'),
			'backup' => date('r')
		);
		
		update_option("dt_theme_backup", $data);
		die('1');
	elseif ($save_type == 'restore_options') :
		$data = get_option("dt_theme_backup");
		update_option(PREGNANCY_SETTINGS, $data);
		die('1');
	elseif ($save_type == "import_options") :
		$data = $_REQUEST['data'];
		$data =  unserialize( stripcslashes($data) );
		update_option(PREGNANCY_SETTINGS, $data);
		die('1');
	elseif( $save_type == "reset_options") :
		delete_option(PREGNANCY_SETTINGS);
		update_option(PREGNANCY_SETTINGS, pregnancy_default_option()); #To set Default options
		die('1');
	endif;
}

/* ---------------------------------------------------------------------------
 * Create function to get theme options
 * --------------------------------------------------------------------------- */
function pregnancy_option($key1, $key2 = '') {
	$options = get_option ( PREGNANCY_SETTINGS );
	$output = NULL;

	if (is_array ( $options )) {

		if (array_key_exists ( $key1, $options )) {
			$output = $options [$key1];
			if (is_array ( $output ) && ! empty ( $key2 )) {
				$output = (array_key_exists ( $key2, $output ) && (! empty ( $output [$key2] ))) ? $output [$key2] : NULL;
			}
		} else {
			$output = $output;
		}
	}
	return $output;
}

/* ---------------------------------------------------------------------------
 * Create admin panel image preview
 * --------------------------------------------------------------------------- */
function pregnancy_adminpanel_image_preview($src, $backend = true, $default = "no-image.jpg") {
	$default = ($backend) ? PREGNANCY_THEME_URI . "/framework/theme-options/images/" . $default : PREGNANCY_THEME_URI . "/images/" . $default;
	$src = ! empty ( $src ) ? $src : $default;
	$output = "<div class='bpanel-option-help'>\n";
	$output .= "<a href='' title='' class='a_image_preivew'> <img src='" . PREGNANCY_THEME_URI . "/framework/theme-options/images/image-preview.png' alt='img' /> </a>\n";
	$output .= "\r<div class='bpanel-option-help-tooltip imagepreview'>\n";
	$output .= "\r<img src='{$src}' data-default='{$default}'/>";
	$output .= "\r</div>\n";
	$output .= "</div>\n";
	echo !empty($output) ? $output : '';
}

/* ---------------------------------------------------------------------------
 * List all images from specific directory
 * --------------------------------------------------------------------------- */
function pregnancy_listImage($dir) {
	$sociables = array ();
	$icon_types = array (
			'jpg',
			'jpeg',
			'gif',
			'png' 
	);

	if (is_dir ( $dir )) {
		$handle = opendir ( $dir );
		while ( false !== ($dirname = readdir ( $handle )) ) {
			if ($dirname != "." && $dirname != "..") {
				$parts = explode ( '.', $dirname );
				$ext = strtolower ( $parts [count ( $parts ) - 1] );

				if (in_array ( $ext, $icon_types )) {
					$option = $parts [count ( $parts ) - 2];
					$sociables [$dirname] = str_replace ( ' ', '', $option );
				}
			}
		}
		closedir ( $handle );
	}
	return $sociables;
}

/* ---------------------------------------------------------------------------
 * Types of Background option available
 * --------------------------------------------------------------------------- */
function pregnancy_bgtypes($name, $parent, $child) {
	$args = array (
		"bg-patterns" => esc_html__ ( "Pattern", 'pregnancy' ),
		"bg-custom" => esc_html__ ( "Custom Background", 'pregnancy' ),
		"bg-none" => esc_html__ ( "None", 'pregnancy' ) 
	);
	$out = '<div class="bpanel-option-set">';
	$out .= "<label>" . esc_html__ ( "Background Type", 'pregnancy' ) . "</label>";
	$out .= "<div class='clear'></div>";
	$out .= "<select class='bg-type dt-chosen-select' name='{$name}'>";
	foreach ( $args as $k => $v ) :
		$rs = selected ( $k, pregnancy_option ( $parent, $child ), false );
		$out .= "<option value='{$k}' {$rs}>{$v}</option>";
	endforeach;
	$out .= "</select>";
	$out .= '</div>';
	echo !empty($out) ? $out : '';
}

/* ---------------------------------------------------------------------------
 * Getting color picker for color option
 * --------------------------------------------------------------------------- */
function pregnancy_admin_color_picker($label, $name, $value, $tooltip = NULL) {
	$output = "<div class='bpanel-option-set'>\n";
	if (! empty ( $label )) :
		$output .= "<label>{$label}</label>";
		$output .= "<div class='hr_invisible'></div><div class='clear'></div>";
	endif;
	
	$output .= "<input type='text' class='my-color-field medium' name='{$name}' value='{$value}' />";

	echo !empty($output) ? $output : '';
	if ($tooltip != NULL)
		dt_theme_adminpanel_tooltip ( $tooltip );

	echo "</div>\n";
}

/* ---------------------------------------------------------------------------
 * Getting color picker for color option
 * --------------------------------------------------------------------------- */
function pregnancy_admin_color_picker_two($name, $value) {
	echo "<input type='text' class='my-color-field small' name='{$name}' value='{$value}' />";
}

/* ---------------------------------------------------------------------------
 * Getting jquery ui slider
 * --------------------------------------------------------------------------- */
function pregnancy_admin_jqueryuislider($label, $id = '', $value = '', $px = "px") {
	$div_value = (! empty ( $value ) && ($px == "px")) ? $value . "px" : $value;
	$output = "<label>{$label}</label>";
	$output .= "<div class='clear'></div>";
	$output .= "<div id='{$id}' class='dttheme-slider' data-for='{$px}'></div>";
	$output .= "<input type='hidden' class='' name='{$id}' value='{$value}'/>";
	$output .= "<div class='dttheme-slider-txt'>{$div_value}</div>";
	echo !empty($output) ? $output : '';
}

/* ---------------------------------------------------------------------------
 * Getting theme switch button
 * --------------------------------------------------------------------------- */
function pregnancy_switch($label, $parent, $name) {
	$checked = ("true" == pregnancy_option ( $parent, $name )) ? ' checked="checked"' : '';
	$switchclass = ("true" == pregnancy_option ( $parent, $name )) ? 'checkbox-switch-on' : 'checkbox-switch-off';
	$out = "<div data-for='dttheme-{$parent}-{$name}' class='checkbox-switch {$switchclass}'></div>";
	$out .= "<input id='dttheme-{$parent}-{$name}' class='hidden' name='dttheme[{$parent}][{$name}]' type='checkbox' value='true' {$checked} />";
	echo !empty($out) ? $out : '';
}


//Pregnancy Appointment starts

if( pregnancy_is_plugin_active('designthemes-pregnancy-addon/designthemes-pregnancy-addon.php') ):


		function pregnancy_dt_replace( $content , $array ){
			$replace = array(
			 '[STAFF_NAME]' => $array['staff_name'],
			 '[SERVICE]' => $array['service_name'],
			 '[CLIENT_NAME]' => $array['client_name'],
			 '[CLIENT_PHONE]' => $array['client_phone'],
			 '[CLIENT_EMAIL]' => $array['client_email'],
			 '[APPOINTMENT_ID]' => $array['appointment_id'],
			 '[APPOINTMENT_TIME]' => $array['appointment_time'],
			 '[APPOINTMENT_DATE]' => $array['appointment_date'],
			 '[APPOINTMENT_TITLE]' => $array['appointment_title'],   
			 '[APPOINTMENT_BODY]' => $array['appointment_body'],
			 '[AMOUNT]' => $array['amount'],
			 '[COMPANY_LOGO]' => $array['company_logo'],
			 '[COMPANY_NAME]' => $array['company_name'],
			 '[COMPANY_PHONE]' => $array['company_phone'],
			 '[COMPANY_ADDRESS]' => $array['company_address'],
			 '[COMPANY_WEBSITE]' => $array['company_website']);
		
			return str_replace( array_keys( $replace ), array_values( $replace ), $content );
		}
		
		function pregnancy_dt_replace_agenda( $content , $array ){
			$replace = array(
			 '[STAFF_NAME]' => $array['staff_name'],
			 '[TOMORROW]' => $array['tomorrow'],
			 '[TOMORROW_AGENDA]' => $array['tomorrow_agenda'],
			 '[COMPANY_LOGO]' => $array['company_logo'],
			 '[COMPANY_NAME]' => $array['company_name'],
			 '[COMPANY_PHONE]' => $array['company_phone'],
			 '[COMPANY_ADDRESS]' => $array['company_address'],
			 '[COMPANY_WEBSITE]' => $array['company_website']);
		
			return str_replace( array_keys( $replace ), array_values( $replace ), $content );
		}
		
		function pregnancy_dt_company_timer( $name, $selected, $is_start = true){
		
			$time_format = get_option( 'time_format' );
			$time_start = new DateTime( '00:00:00', new DateTimeZone( 'UTC' ) );
			$time_end = new DateTime( '23:45:00', new DateTimeZone( 'UTC' ) );
			$time_interval = '+ 15 min';
		
			$class = $is_start ? "select_start" : "select_end";
		
			$output = "<select name='{$name}' class='medium {$class}' data-current='{$selected}'>";
			$output .= ( $is_start ) ? "<option value=''>OFF</option>" : "";
			while( $time_start <= $time_end ){
				$value = $time_start->format('H:i');
				$name = $time_start->format($time_format);
				$s = ( $selected == $value ) ? " selected='selected' " : "";
				$output .= "<option value='{$value}' {$s}>{$name}</option>";
				$time_start->modify($time_interval);
			}
			$output .= '</select>';
			return $output;
		}
		
		function pregnancy_dt_member_timer( $name, $selected = "", $is_start = true ){
		
			$time_format = get_option( 'time_format' );
			$time_start = new DateTime( '00:00:00', new DateTimeZone( 'UTC' ) );
			$time_end = new DateTime( '23:45:00', new DateTimeZone( 'UTC' ) );
			$time_interval = '+ 15 min';
		
			$class = $is_start ? "select_start" : "select_end";
		
			$output = "<select name='{$name}' class='{$class}' data-current='{$selected}'>";
			$output .= ( $is_start ) ? "<option value=''>OFF</option>" : "";
			while( $time_start <= $time_end ){
				$value = $time_start->format('H:i');
				$name = $time_start->format($time_format);
				$s = ( $selected == $value ) ? " selected='selected' " : "";
				$output .= "<option value='{$value}' {$s}>{$name}</option>";
				$time_start->modify($time_interval);
			}
			$output .= '</select>';
			return $output;
		}
		
		function pregnancy_dt_appointment_currency_symbol( $currency = '' ) {
			switch ( $currency ) {
				case 'BRL' :
					$currency_symbol = '&#82;&#36;';
					break;
				case 'BGN' :
					$currency_symbol = '&#1083;&#1074;.';
					break;
				case 'AUD' :
				case 'CAD' :
				case 'CLP' :
				case 'MXN' :
				case 'NZD' :
				case 'HKD' :
				case 'SGD' :
				case 'USD' :
					$currency_symbol = '&#36;';
					break;
				case 'EUR' :
					$currency_symbol = '&euro;';
					break;
				case 'CNY' :
				case 'RMB' :
				case 'JPY' :
					$currency_symbol = '&yen;';
					break;
				case 'RUB' :
					$currency_symbol = '&#1088;&#1091;&#1073;.';
					break;
				case 'KRW' : $currency_symbol = '&#8361;'; break;
				case 'TRY' : $currency_symbol = '&#84;&#76;'; break;
				case 'NOK' : $currency_symbol = '&#107;&#114;'; break;
				case 'ZAR' : $currency_symbol = '&#82;'; break;
				case 'CZK' : $currency_symbol = '&#75;&#269;'; break;
				case 'MYR' : $currency_symbol = '&#82;&#77;'; break;
				case 'DKK' : $currency_symbol = '&#107;&#114;'; break;
				case 'HUF' : $currency_symbol = '&#70;&#116;'; break;
				case 'IDR' : $currency_symbol = 'Rp'; break;
				case 'INR' : $currency_symbol = 'Rs.'; break; #Rs. &#x20b9; &#8377;
				case 'ISK' : $currency_symbol = 'Kr.'; break;
				case 'ILS' : $currency_symbol = '&#8362;'; break;
				case 'PHP' : $currency_symbol = '&#8369;'; break;
				case 'PLN' : $currency_symbol = '&#122;&#322;'; break;
				case 'SEK' : $currency_symbol = '&#107;&#114;'; break;
				case 'CHF' : $currency_symbol = '&#67;&#72;&#70;'; break;
				case 'TWD' : $currency_symbol = '&#78;&#84;&#36;'; break;
				case 'THB' : $currency_symbol = '&#3647;'; break;
				case 'GBP' : $currency_symbol = '&pound;'; break;
				case 'RON' : $currency_symbol = 'lei'; break;
				case 'VND' : $currency_symbol = '&#8363;'; break;
				case 'NGN' : $currency_symbol = '&#8358;'; break;
				default    : $currency_symbol = ''; break;
			}
		
			return $currency_symbol;
		}
		
		function pregnancy_dt_appointment_currencies() {
			return array_unique(
					array(
						'AUD' => esc_html__( 'Australian Dollars', 'pregnancy' ),
						'BRL' => esc_html__( 'Brazilian Real', 'pregnancy' ),
						'BGN' => esc_html__( 'Bulgarian Lev', 'pregnancy' ),
						'CAD' => esc_html__( 'Canadian Dollars', 'pregnancy' ),
						'CLP' => esc_html__( 'Chilean Peso', 'pregnancy' ),
						'CNY' => esc_html__( 'Chinese Yuan', 'pregnancy' ),
						'COP' => esc_html__( 'Colombian Peso', 'pregnancy' ),             
						'CZK' => esc_html__( 'Czech Koruna', 'pregnancy' ),
						'DKK' => esc_html__( 'Danish Krone', 'pregnancy' ),
						'EUR' => esc_html__( 'Euros', 'pregnancy' ),
						'HKD' => esc_html__( 'Hong Kong Dollar', 'pregnancy' ),
						'HRK' => esc_html__( 'Croatia kuna', 'pregnancy' ),
						'HUF' => esc_html__( 'Hungarian Forint', 'pregnancy' ),
						'ISK' => esc_html__( 'Icelandic krona', 'pregnancy' ),
						'IDR' => esc_html__( 'Indonesia Rupiah', 'pregnancy' ),
						'INR' => esc_html__( 'Indian Rupee', 'pregnancy' ),
						'ILS' => esc_html__( 'Israeli Shekel', 'pregnancy' ),
						'JPY' => esc_html__( 'Japanese Yen', 'pregnancy' ),
						'KRW' => esc_html__( 'South Korean Won', 'pregnancy' ),
						'MYR' => esc_html__( 'Malaysian Ringgits', 'pregnancy' ),
						'MXN' => esc_html__( 'Mexican Peso', 'pregnancy' ),
						'NGN' => esc_html__( 'Nigerian Naira', 'pregnancy' ),
						'NOK' => esc_html__( 'Norwegian Krone', 'pregnancy' ),
						'NZD' => esc_html__( 'New Zealand Dollar', 'pregnancy' ),
						'PHP' => esc_html__( 'Philippine Pesos', 'pregnancy' ),
						'PLN' => esc_html__( 'Polish Zloty', 'pregnancy' ),
						'GBP' => esc_html__( 'Pounds Sterling', 'pregnancy' ),
						'RON' => esc_html__( 'Romanian Leu', 'pregnancy' ),
						'RUB' => esc_html__( 'Russian Ruble', 'pregnancy' ),
						'SGD' => esc_html__( 'Singapore Dollar', 'pregnancy' ),
						'ZAR' => esc_html__( 'South African rand', 'pregnancy' ),
						'SEK' => esc_html__( 'Swedish Krona', 'pregnancy' ),
						'CHF' => esc_html__( 'Swiss Franc', 'pregnancy' ),
						'TWD' => esc_html__( 'Taiwan New Dollars', 'pregnancy' ),
						'THB' => esc_html__( 'Thai Baht', 'pregnancy' ),
						'TRY' => esc_html__( 'Turkish Lira', 'pregnancy' ),
						'USD' => esc_html__( 'US Dollars', 'pregnancy' ),
						'VND' => esc_html__( 'Vietnamese Dong', 'pregnancy' ),
					)
				);
		}
		
		function pregnancydurationToString( $duration ) {
		
			$hours   = (int)( $duration / 3600 );
			$minutes = (int)( ( $duration % 3600 ) / 60 );
			$result  = '';
			if ( $hours > 0 ) {
				$result = sprintf( esc_html__( '%d h', 'pregnancy' ), $hours );
				if ( $minutes > 0 ) {
					$result .= ' ';
				}
			}
		
			if ( $minutes > 0 ) {
				$result .= sprintf( esc_html__( '%d min', 'pregnancy' ), $minutes );
			}
		 return $result;
		}
		
		function pregnancy_dates_range( $start_date, $end_date, $days = array() ){
		
			$interval = new DateInterval( 'P1D' );
		
			$realEnd = new DateTime( $end_date );
			$realEnd->add( $interval );
		
			$period = new DatePeriod( new DateTime( $start_date ), $interval, $realEnd );
			$dates = array();
		
			foreach ( $period as $date ) {
				$dates[] = in_array( strtolower( $date->format('l')) , $days ) ? $date->format( 'Y-m-d l' ) :"" ;
			}
			
			$dates = array_filter($dates);
			return $dates;
		}
		
		function pregnancy_dt_send_mail( $to, $subject, $message ){
			$sender_name =  pregnancy_option('pregnancy_appointment', 'notification_sender_name');
			$sender_name = !empty($sender_name) ? $sender_name : get_option( 'blogname' );
		
			$sender_email = pregnancy_option('pregnancy_appointment', 'notification_sender_email');
			$sender_email = !empty( $sender_email ) ? $sender_email : get_option( 'admin_email' );
		
			$from = $sender_name."<{$sender_email}>";
		
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: '.$from.'' . "\r\n";
			return @mail( $to, $subject, $message, $headers );
		}
	endif;
//Pregnancy Appointment ends


/* ---------------------------------------------------------------------------
 * Return List of social icons
 * --------------------------------------------------------------------------- */
function pregnancy_listSocial() {
	$sociables = array('fa-dribbble' => 'Dribble', 'fa-flickr' => 'Flickr', 'fa-github' => 'GitHub', 'fa-pinterest' => 'Pinterest', 'fa-stack-overflow' => 'Stack Overflow', 'fa-twitter' => 'Twitter', 'fa-youtube' => 'YouTube', 'fa-android' => 'Android', 'fa-dropbox' => 'Dropbox', 'fa-instagram' => 'Instagram', 'fa-windows' => 'Windows', 'fa-apple' => 'Apple', 'fa-facebook' => 'Facebook', 'fa-google-plus' => 'Google Plus', 'fa-linkedin' => 'LinkedIn', 'fa-skype' => 'Skype', 'fa-tumblr' => 'Tumblr', 'fa-vimeo-square' => 'Vimeo');
	
	return $sociables;
}

/* ---------------------------------------------------------------------------
 * Getting theme sociable selection box
 * --------------------------------------------------------------------------- */
function pregnancy_sociables_selection($name = '', $selected = "") {
	$sociables = pregnancy_listSocial();

	$name = ! empty ( $name ) ? "name='dttheme[social][{$name}][icon]'" : '';
	$out = "<select class='social-select' {$name}>"; // name attribute will be added to this by jQuery menuAdd()
	foreach ( $sociables as $key => $value ) :
		$s = selected ( $key, $selected, false );
		$v = ucwords ( $value );
		$out .= "<option value='{$key}' {$s} >{$v}</option>";
	endforeach;
	$out .= "</select>";

	return $out;
}

/* ---------------------------------------------------------------------------
 * Getting sub directories from parent directory
 * --------------------------------------------------------------------------- */
function pregnancy_getfolders($directory, $starting_with = "", $sorting_order = 0) {
	if (! is_dir ( $directory ))
		return false;
	$dirs = array ();
	$handle = opendir ( $directory );
	while ( false !== ($dirname = readdir ( $handle )) ) {
		if ($dirname != "." && $dirname != ".." && is_dir ( $directory . "/" . $dirname )) {
			if ($starting_with == "")
				$dirs [] = $dirname;
			else {
				$filter = strstr ( $dirname, $starting_with );
				if ($filter !== false)
					$dirs [] = $dirname;
			}
		}
	}
	
	closedir ( $handle );
	
	if ($sorting_order == 1) {
		rsort ( $dirs );
	} else {
		sort ( $dirs );
	}
	return $dirs;
}

/* ---------------------------------------------------------------------------
 * Add new mimes to use custom font upload
 * --------------------------------------------------------------------------- */
add_filter('upload_mimes', 'pregnancy_upload_mimes');
function pregnancy_upload_mimes( $existing_mimes = array() ){
	$existing_mimes['woff'] = 'font/woff';
	$existing_mimes['ttf'] 	= 'font/ttf';
	$existing_mimes['svg'] 	= 'font/svg';
	$existing_mimes['eot'] 	= 'font/eot';
	return $existing_mimes;
} ?>