<?php
function pregnancy_dt_get_page_permalink_by_its_template( $temlplate ) {
	$permalink = null;

	$pages = get_posts( array(
			'post_type' => 'page',
			'meta_key' => '_wp_page_template',
			'meta_value' => $temlplate,
			'suppress_filters' => false ) );

	if ( is_array( $pages ) && count( $pages ) > 0 ) {
		$login_page = $pages[0];
		$permalink = get_permalink( $login_page->ID );
	}
	return $permalink;
}

add_action( 'wp_ajax_pregnancy_dt_fill_staffs', 'pregnancy_dt_fill_staffs' ); # For logged-in users
add_action( 'wp_ajax_nopriv_pregnancy_dt_fill_staffs','pregnancy_dt_fill_staffs'); # For logged-out users 
function pregnancy_dt_fill_staffs() {
	if( isset($_POST['service_id']) ){
		$wp_query = new WP_Query();
		$staffs = array(
			'post_type' => 'dt_staffs',
			'posts_per_page' => '-1',
			'meta_query'=>array());

		$staffs['meta_query'][] = array(
			'key'     => '_services',
			'value'   =>  $_POST['service_id'],
			'compare' => 'LIKE');
		
		$wp_query->query( $staffs );
		echo "<option value=''>".esc_html__('Select','pregnancy')."</option>";
		if( $wp_query->have_posts() ):
			while( $wp_query->have_posts() ):
				$wp_query->the_post();
				$id = get_the_ID();
				$title = get_the_title($id);
				echo "<option value='{$id}'>{$title}</option>";
			endwhile;
		endif;	
	}
	die( '' );
}


//appointment type2
add_action( 'wp_ajax_pregnancy_dt_generate_schedule', 'pregnancy_dt_generate_schedule' );
add_action( 'wp_ajax_nopriv_pregnancy_dt_generate_schedule','pregnancy_dt_generate_schedule');
function pregnancy_dt_generate_schedule() {

	$seldate = $_REQUEST['datepicker'];
	$staffid = $_REQUEST['staffid'];
	$staff = get_the_title($staffid);
	$staffids_str = $_REQUEST['staffids'];
	$serviceid = $_REQUEST['serviceid'];
	$service = get_the_title($serviceid);
	$staffs_arr = array();
	
	if( empty( $staffid ) ) {
		$wp_query = new WP_Query();
		$staffs = array( 'post_type' => 'dt_staffs', 'orderby'=>'ID', 'order'=>'DESC', 'posts_per_page' => '-1', 'meta_query'=>array());
		
		if($staffids_str != '') {
			$staffids = explode(',', $staffids_str);
			$staffs['post__in'] = $staffids;
		}
		
		$staffs['meta_query'][] = array( 'key' => '_services', 'value' => $serviceid, 'compare' => 'LIKE');
		$wp_query->query( $staffs );
		if( $wp_query->have_posts() ):
			while( $wp_query->have_posts() ):
				$wp_query->the_post();
				$staffid = get_the_ID();
				$staff =  get_the_title($staffid);
				$staffs_arr[$staffid] = $staff;
			endwhile;
		endif;	
		wp_reset_postdata();
	} else {
		$staffs_arr = array($staffid=>$staff);
	}
	
	$serviceinfo = get_post_meta( $serviceid, "_info",true);
	$serviceinfo = is_array($serviceinfo) ? $serviceinfo : array();
	$service_duration = array_key_exists('duration', $serviceinfo) ? $serviceinfo['duration'] :  1800;
	
	$out = '';
	$out .= '<div class="border-title"><h2>'.esc_html__('Select Time','pregnancy').'</h2></div>';
	$out .= '<div class="dt-sc-available-times">';
		
	$seldate = new DateTime($seldate);
	$seldate = $seldate->format('Y-m-d');
	
	
	foreach( $staffs_arr as $sid => $sname ) {
	
		
		# 1. Get Staff Schedule Time
		$timer = get_post_meta( $sid, "_timer",true);
		$timer = is_array($timer) ? $timer : array();
		$timer = array_filter($timer);
		$timer = array_diff( $timer, array('00:00'));
	
		$working_hours = array();
	
		foreach ( array('monday','tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday') as $day ):
			if(  array_key_exists("{$day}_start",$timer)  ):
				$working_hours[$day] = array( 'start' => $timer["{$day}_start"] , 'end' => $timer["{$day}_end"]);
			endif;	
		endforeach;
		
		#Staff existing bookings
		$bookings = array();
		global $wpdb;
		$q = "SELECT option_value FROM $wpdb->options WHERE option_name LIKE '_dt_reservation_mid_{$sid}%' ORDER BY option_id ASC";
		$rows = $wpdb->get_results( $wpdb->prepare($q) );
		if( $rows ){
			foreach ($rows as $row ) {
				if( is_serialized($row->option_value ) ) {
					$data = unserialize($row->option_value);
					$data = $data['start'];
					$data = explode("(", $data);
					$data = new DateTime($data[0]);	
					$data = $data->format("Y-m-d G:i:s");
					$bookings[] = $data;
					
				}
			}
		}
		#Staff existing bookings
		
		$slots = array();
		
		if( count($working_hours) ){
			$slot = pregnancy_dt_findTimeSlot( $working_hours, $bookings, $seldate, $service_duration );
			if( !empty($slot) ){
				$slots[] = $slot;
				
			}
		}
		
		$out .= "<h6 class='staff-name'>";
		$out .= "{$sname}";
		$out .= "</h6>";
	
		if( !empty($slots) ) {
			
			$sinfo = get_post_meta( $sid , "_info",true);
			$sinfo = is_array($sinfo) ? $sinfo : array();
		
			$out .= '<ul class="time-table">';
			foreach( $slots as $slot ){
				
				
				
				if( is_array($slot) ){
					foreach( $slot as $date => $s  ){
						if(is_array($s)){
							$daydate = $date;
							$out .= '<ul class="time-slots">';
							foreach( $s as $time ){
								$start = new DateTime($time->start);
								$start = $start->format( 'm/d/Y H:i');
	
								$end = new DateTime($time->end);
								$end = $end->format( 'm/d/Y H:i');
	
								$date =  new DateTime($time->date);
								$date = $date->format( 'm/d/Y');
	
								$out .= '<li>';
								$out .= "<a href='#' data-staffid='{$sid}' data-staffname='{$sname}' data-serviceid='{$serviceid}' data-start='{$start}' data-end='{$end}' data-date='{$date}' data-time='{$time->hours}' data-daydate='{$daydate}' class='time-slot'>";
								$out .= $time->label;
								$out .= '</a>';
								$out .= '</li>';
							}
							$out .= '</ul>';
						}
						$out .= '</li>';
					}
				}
			}
			$out .= "</ul>";
		} else {
			$out .= '<div class="dt-sc-info-box">'.esc_html__('Time slots not available for your requested date!. Please try again by changing date.', 'pregnancy').'</div>';	
		}
	} # Staffs loops end
	
	$out .= '</div>';
	
	echo ($out != '') ? $out : '<p class="dt-sc-info-box">'.esc_html__('No Time slots available','pregnancy').'</p>';
	die();

}
//appointment type2

add_action( 'wp_ajax_pregnancy_dt_available_times', 'pregnancy_dt_available_times' ); # For logged-in users
add_action( 'wp_ajax_nopriv_pregnancy_dt_available_times', 'pregnancy_dt_available_times' ); # For logged-out users
function pregnancy_dt_available_times(){

	$date = $_REQUEST['date'];
	$stime = $_REQUEST['stime'];
	$etime = $_REQUEST['etime'];
	$staff = $_REQUEST['staff'];
	$staffid = $_REQUEST['staffid'];
	$service = $_REQUEST['service'];
	$serviceid = $_REQUEST['serviceid'];
	
	if( empty( $staffid ) ) {
		# Staff
		$wp_query = new WP_Query();
		$staffs = array( 'post_type' => 'dt_staffs', 'orderby'=>'ID', 'order'=>'DESC', 'posts_per_page' => '-1', 'meta_query'=>array());
		$staffs['meta_query'][] = array( 'key' => '_services', 'value' => $serviceid ,'compare' => 'LIKE');
		$wp_query->query( $staffs );
		
		if( $wp_query->have_posts() ):
			while( $wp_query->have_posts() ):
				$wp_query->the_post();
				$staffid = get_the_ID();
				$id = get_the_ID();
				$staff =  get_the_title($id);
				$mgs[$staffid] = $staff;
			endwhile;
		endif;	
		# Staff
	} else {
		$mgs = array($staffid=>$staff);
	}
	

	$info = get_post_meta( $serviceid, "_info",true);
	$info = is_array($info) ? $info : array();
	$service_duration = array_key_exists('duration', $info) ? $info['duration'] :  1800;
	
	$bookings = array();
	$working_hours = array();
	$out = "";

	foreach( $mgs as $sid => $sname ) {

		# 1. Get Staff Schedule Time
		$timer = get_post_meta( $sid, "_timer",true);
		$timer = is_array($timer) ? $timer : array();
		$timer = array_filter($timer);
		$timer = array_diff( $timer, array('00:00'));

		$working_hours = array();

		foreach ( array('monday','tuesday', 'wednesday2', 'thursday', 'friday', 'saturday', 'sunday') as $day ):
			if(  array_key_exists("{$day}_start",$timer)  ):
				$working_hours[$day] = array( 'start' => $timer["{$day}_start"] , 'end' => $timer["{$day}_end"]);
			endif;	
		endforeach;
		
		#Staff existing bookings
		global $wpdb;
		$q = "SELECT option_value FROM $wpdb->options WHERE option_name LIKE '_dt_reservation_mid_{$staffid}%' ORDER BY option_id ASC";
		$rows = $wpdb->get_results( $wpdb->prepare($q) );
		if( $rows ){
			foreach ($rows as $row ) {
				if( is_serialized($row->option_value ) ) {
					$data = unserialize($row->option_value);
					$data = $data['start'];
					$data = explode("(", $data);
					$data = new DateTime($data[0]);	
					$data = $data->format("Y-m-d G:i:s");
					$bookings[] = $data;
				}
			}
		}
		#Staff existing bookings
		
		$slots = array();
		
		if( count($working_hours) ){
			//$loop = ( count($working_hours) == 7 ) ? 7 : 10;
			$loop = 7;
			$i = 0;
			
			while( $i < $loop ){
				$slot = pregnancy_findTimeSlot( $working_hours, $bookings, $date , $service_duration );
				if( !empty($slot) ){
					$slots[] = $slot;
				}
				$date = new DateTime($date);
				$date->modify("+1 day");
				$date = $date->format('Y-m-d');
				$i++;
			}#endwhile
		}
		
		if( !empty($slots) ) {
			$out .= "<h5 class='hr-title'>";
			$out .= "<span> {$sname} </span>";
			$out .= "</h5>";
			$out .= "<ul class='time-table'>";
			foreach( $slots as $slot ){
				if( is_array($slot) ){
					foreach( $slot as $date => $s  ){
						$out .= "<li> <span> {$sname} </span> <span> {$date} </span>";
						if(is_array($s)){
							$out .= "<ul class='time-slots' >";
							foreach( $s as $time ){
								$start = new DateTime($time->start);
								$start = $start->format( 'm/d/Y H:i');

								$end = new DateTime($time->end);
								$end = $end->format( 'm/d/Y H:i');

								$date =  new DateTime($time->date);
								$date = $date->format( 'm/d/Y');

								$out .= '<li>';
								$out .= "<a href='#' data-sid='{$sid}' data-start='{$start}' data-end='{$end}' data-date='{$date}' data-time='{$time->hours}' class='time-slot'>";
								$out .= $time->label;
								$out .= '</a>';
								$out .= '</li>';
							}
							$out .= '</ul>';
						}
						$out .= '</li>';
					}
				}
			}
			$out .= "</ul>";
		}	
	} # Staffs loops end
	
	if( empty($out) )
		echo '<p>'.esc_html__('No Time slots available','pregnancy').'</p>';
	else
		echo !empty($out) ? $out : '';
		
	die('');
}

function pregnancy_findTimeSlot( $working_hours, $bookings, $date , $service_duration = 1800 ){

	$time_format = get_option('time_format');

	$timeslot= array();
	$dayofweek = date('l',strtotime($date));
	$dayofweek = strtolower($dayofweek);

	$is_date_today = ($date == date( 'Y-m-d', current_time( 'timestamp' ) ) );
	$current_time  = date( 'H:i:s', ceil( current_time( 'timestamp' ) / 900 ) * 900 );

	$past = ( $date <  date('Y-m-d') ) ? true : false;

	if( array_key_exists($dayofweek, $working_hours)  && !$past ){

		$working_start_time = ($is_date_today && $current_time > $working_hours[ $dayofweek ][ 'start' ]) ? $current_time : $working_hours[ $dayofweek ][ 'start' ];
		$working_end_time = $working_hours[ $dayofweek ][ 'end' ];

		$show = $is_date_today && ($current_time > $working_end_time) ? false : true;
		if( $show ) {
			
			$intersection = pregnancy_findIntersection( $working_start_time,$working_hours[ $dayofweek ][ 'end' ],$_REQUEST['stime'],$_REQUEST['etime']);
			
			for( $time = pregnancy_dtStrToTime($intersection['start']); $time <= ( pregnancy_dtStrToTime($intersection['end']) - $service_duration ); $time += $service_duration ){

				$value = $date.' '.date('G:i:s', $time);
				$end = $date.' '.date('G:i:s', ($time+$service_duration));

				if( !in_array($value, $bookings) ) { # if already booked in $time
					$object = new stdClass();
					$object->label = date( $time_format, $time );
					$object->date = $date;
					$object->start = $value;
					$object->hours = date('g:i A', $time).' - '.date('g:i A', ($time+$service_duration));
					$object->end = $end;
					$p = $date.'<span> ('.date('l',strtotime($date)).') </span>';
					$timeslot[$p][$time] = $object;
				}
			}
		}
	}
	return $timeslot;
}

//appointment type2
function pregnancy_dt_findTimeSlot( $working_hours, $bookings, $date , $service_duration = 1800 ){
	
	$time_format = get_option('time_format');
	

	$timeslot= array();
	$dayofweek = date('l',strtotime($date));
	$dayofweek = strtolower($dayofweek);

	$is_date_today = ($date == date( 'Y-m-d', current_time( 'timestamp' ) ) );
	$current_time  = date( 'H:i:s', ceil( current_time( 'timestamp' ) / 900 ) * 900 );

	$past = ( $date <  date('Y-m-d') ) ? true : false;
	

	if( array_key_exists($dayofweek, $working_hours)  && !$past ){
		

		$working_start_time = ($is_date_today && $current_time > $working_hours[ $dayofweek ][ 'start' ]) ? $current_time : $working_hours[ $dayofweek ][ 'start' ];
		$working_end_time = $working_hours[ $dayofweek ][ 'end' ];

		$show = $is_date_today && ($current_time > $working_end_time) ? false : true;
		if( $show ) {
			
			$intersection = pregnancy_findIntersection( $working_start_time,$working_hours[ $dayofweek ][ 'end' ],'00:00','23:59');
			
			for( $time = pregnancy_dtStrToTime($intersection['start']); $time <= ( pregnancy_dtStrToTime($intersection['end']) - $service_duration ); $time += $service_duration ){

				$value = $date.' '.date('G:i:s', $time);
				$end = $date.' '.date('G:i:s', ($time+$service_duration));

				if( !in_array($value, $bookings) ) { # if already booked in $time
					$object = new stdClass();
					$object->label = date( $time_format, $time );
					$object->date = $date;
					$object->start = $value;
					$object->hours = date('g:i A', $time).' - '.date('g:i A', ($time+$service_duration));
					$object->end = $end;
					$p = $date.' ('.date('l',strtotime($date)).')';
					$timeslot[$p][$time] = $object;
				}
			}
		}
	}
	return $timeslot;
}

//appointment type2

function pregnancy_findIntersection( $p1_start, $p1_end, $p2_start, $p2_end ) {
	$result = false;
	if ( $p1_start <= $p2_start && $p1_end >= $p2_start && $p1_end <= $p2_end ) {
		$result = array( 'start' => $p2_start, 'end' => $p1_end );
	} else if ( $p1_start <= $p2_start && $p1_end >= $p2_end ) {
		$result = array( 'start' => $p2_start, 'end' => $p2_end );
	} else if ( $p1_start >= $p2_start && $p1_start <= $p2_end && $p1_end >= $p2_end ) {
		$result = array( 'start' => $p1_start, 'end' => $p2_end );
	} else if ( $p1_start >= $p2_start && $p1_end <= $p2_end ) {
		$result = array( 'start' => $p1_start, 'end' => $p1_end );
    }
	return $result;
}

function pregnancy_dtStrToTime( $str ) {
	return strtotime( sprintf( '1985-03-17 %s', $str ) );
}


#Front End - tpl-reservation ajax
function pregnancy_dt_customer( $name, $email, $phone ){

	$user = array('name'=>$name,'emailid'=>$email,'phone'=>$phone);
	$users = array();

	$wp_query = new WP_Query();
	$customers = array( 'post_type'=>'dt_customers','posts_per_page'=>-1,'order_by'=> 'published');

		$wp_query->query( $customers );
		if( $wp_query->have_posts() ):
			while( $wp_query->have_posts() ):
				$wp_query->the_post();
				$the_id = get_the_ID();
				$title = get_the_title($the_id);

				$info = get_post_meta ( $the_id, "_info",true);
				$info = is_array($info) ? $info : array();
				$info['name'] = $title;
				$users[$the_id] = $info;
			endwhile;
		endif;

	$uid = array_search( $user, $users);

	if( $uid  ){
		$uid = $uid;
	} else {
		#Insert new customer
		$post_id = wp_insert_post( array('post_title' => $user['name'], 'post_type' => 'dt_customers', 'post_status' => 'publish'));
		if( $post_id > 0 ) {
			$info['emailid'] = $user['emailid'];
			$info['phone'] = $user['phone'];
			update_post_meta ( $post_id, "_info",$info);
			$uid = $post_id;
		}
	}
	return $uid;
}

//appointment type2
function pregnancy_appointment_type2_customer( $customer ){

	$user = $customer;
	$users = array();

	$wp_query = new WP_Query();
	$customers = array('post_type'=>'dt_customers','posts_per_page'=>-1,'order_by'=>'published');
	
		$wp_query->query($customers);
		if( $wp_query->have_posts() ):
			while( $wp_query->have_posts() ):
				$wp_query->the_post();
				$the_id = get_the_ID();
				$title = get_the_title($the_id);

				$info = get_post_meta ( $the_id, "_info",true);
				$info = is_array($info) ? $info : array();
				$info['name'] = $title;
				$users[$the_id] = $info;
			endwhile;
		endif;

	$uid = array_search( $user, $users);

	if( $uid  ){
		$uid = $uid;
	} else {
		#Insert new customer
		$post_id = wp_insert_post( array('post_title' => $user['firstname'].' '.$user['lastname'], 'post_type' => 'dt_customers', 'post_status' => 'publish'));
		if( $post_id > 0 ) {
			$info['firstname'] = $user['firstname'];
			$info['lastname'] = $user['lastname'];
			$info['phone'] = $user['phone'];
			$info['emailid'] = $user['emailid'];
			$info['address'] = $user['address'];
			$info['aboutyourproject'] = $user['aboutyourproject'];
			update_post_meta ($post_id, "_info", $info);
			$uid = $post_id;
		}
	}
	return $uid;
}
//appointment type2


add_action( 'wp_ajax_pregnancy_dt_new_reservation', 'pregnancy_dt_new_reservation' ); # For logged-in users
add_action( 'wp_ajax_nopriv_pregnancy_dt_new_reservation','pregnancy_dt_new_reservation'); # For logged-out users
function pregnancy_dt_new_reservation(){
	global $wpdb;

	#New Customer
		$name = $_REQUEST['name'];
		$email = $_REQUEST['email'];
		$phone = $_REQUEST['phone'];
		$customer = pregnancy_dt_customer($name,$email,$phone);
	#New Customer
	
	$id = $wpdb->get_var("SELECT max(option_id) FROM $wpdb->options");
	$title = esc_html__("New Reservation By ",'pregnancy').$name;
	$body =  $_REQUEST['body'];

	$staff = $_REQUEST['staff'];
	$service = $_REQUEST['service'];
	$start = $_REQUEST['start'];
	$end = $_REQUEST['end'];

	$option = "_dt_reservation_mid_{$staff}_id_{$id}";
	$data = array( 'id' => $id, 'title' => $title, 'body' => $body, 'start'=> $start, 'end'=>$end, 'service'=>$service, 'user'=>$customer, 'readOnly'=>true );
	
	# Sending Mail
		
		$client_name = $client_phone = $client_email = $amount = "";

		#Staff
		$staff_name = get_the_title($staff);
		$service_name = get_the_title($service);

		$sinfo = get_post_meta( $staff , "_info",true);
		$sinfo = is_array($sinfo) ? $sinfo : array();
		$staff_price = array_key_exists("price", $sinfo) ? $sinfo['price'] : 0;
		$staff_price = floatval($staff_price);

		#Service Price
		if( !empty( $data['service']) ){
			$serviceinfo = get_post_meta($data['service'],'_info',true );
			$serviceinfo = is_array( $serviceinfo ) ? $serviceinfo : array();
			$service_price = array_key_exists("price", $serviceinfo) ? $serviceinfo['price'] : 0;
			$service_price = floatval($service_price);
		}

		$amount = ( ($staff_price+$service_price) > 0 ) ?  pregnancy_dt_appointment_currency_symbol( pregnancy_option('pregnancy_appointment', 'currency') ).' '.( $staff_price+$service_price ) : $amount;

		#Client
		if( !empty($data['user']) ){

			$client_name = get_the_title($data['user']);
			$cinfo = get_post_meta( $data['user'], "_info",true);
			$cinfo = is_array($cinfo) ? $cinfo : array();

			$client_email = array_key_exists('emailid', $cinfo) ? $cinfo['emailid'] : "";
			$client_phone = array_key_exists('phone', $cinfo) ? $cinfo['phone'] : "";;
		}

		$array = array(
			'staff_name' => $staff_name,
			'service_name' => $service_name,
			'appointment_id' => $data['id'],
			'appointment_time' => $_POST['time'],
			'appointment_date' => $_POST['date'],
			'appointment_title' => $data['title'],
			'appointment_body' =>  $data['body'],
			'client_name' => $client_name,
			'client_phone' => $client_phone,
			'client_email' => $client_email,
			'amount' => $amount,
			'company_logo' => 'Company Logo',
			'company_name' => 'Company Name',
			'company_phone' => 'Company Phone',
			'company_address' => 'Company Address',
			'company_website' => 'Company Website');

		$subject = pregnancy_option('pregnancy_appointment', 'appointment_notification_to_staff_subject');
		$subject = pregnancy_dt_replace( $subject, $array);

		$message = pregnancy_option('pregnancy_appointment', 'appointment_notification_to_staff_message');
		$message = pregnancy_dt_replace( $message, $array);

		#Staff Mail
		pregnancy_dt_send_mail( $sinfo["emailid"], $subject, $message);

		#Client Mail
		if( !empty($client_email) ) {
			$subject = pregnancy_option('pregnancy_appointment', 'appointment_notification_to_client_subject');
			$subject = pregnancy_dt_replace( $subject, $array);

			$message = pregnancy_option('pregnancy_appointment', 'appointment_notification_to_client_message');
			$message = pregnancy_dt_replace( $message, $array);

			pregnancy_dt_send_mail( $client_email, $subject, $message);
		}


	# Sending Mail
	if( update_option( $option, $data ) ){
		#echo "Added";
		
		#Add Payment Details to options table
		$payment_id = str_replace('_dt_reservation_',"_dt_payment_",$option);
		#$amount = trim(str_replace(pregnancy_dt_appointment_currency_symbol( get_option("dt_currency") ),"",$amount));
		$amount = $staff_price+$service_price;

		$payment_data = array( 
			'date' =>  date('Y-m-d H:i:s'),
			'service' => get_the_title($data['service']),
			'type' => 'local',
			'customer_id' =>$data['user'],
			'total'=> $amount);

		update_option($payment_id,$payment_data);
		# $result['url'] = home_url('/');
		
		$url = pregnancy_dt_get_page_permalink_by_its_template('tpl-reservation-type1.php');
		$url = add_query_arg( array('action'=>'success'), $url );
		$result['url'] =  $url;
		echo json_encode( $result );
	}else{
		echo "FAiled";
	}
	die('');
}


//appointment type2
add_action( 'wp_ajax_pregnancy_dttheme_new_reservation', 'pregnancy_dttheme_new_reservation' ); # For logged-in users
add_action( 'wp_ajax_nopriv_pregnancy_dttheme_new_reservation','pregnancy_dttheme_new_reservation'); # For logged-out users
function pregnancy_dttheme_new_reservation(){
	global $wpdb;

	#New Customer
	$firstname = $_REQUEST['firstname'];
	$lastname = $_REQUEST['lastname'];
	$phone = $_REQUEST['phone'];
	$emailid = $_REQUEST['emailid'];
	$address = $_REQUEST['address'];
	$aboutyourproject = $_REQUEST['aboutyourproject'];
	
	$customer_data = array('firstname' => $firstname, 'lastname' => $lastname, 'phone' => $phone, 'emailid' => $emailid, 'address' => $address, 'aboutyourproject' => $aboutyourproject);
	$customer = pregnancy_appointment_type2_customer($customer_data);
	#New Customer
	
	$id = $wpdb->get_var("SELECT max(option_id) FROM $wpdb->options");
	$title = esc_html__("New Reservation By ",'pregnancy').$firstname;
	$body =  $aboutyourproject;

	$staffid = $_REQUEST['staffid'];
	$serviceid = $_REQUEST['serviceid'];
	if( pregnancy_is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
		global $sitepress;
		$default_lang = $sitepress->get_default_language();
		$current_lang = ICL_LANGUAGE_CODE;
		if( $default_lang != $current_lang ) {
			$serviceid =  icl_object_id(  $serviceid ,'dt_services', true ,$sitepress->get_default_language());
		}
	}
	
	$start = $_REQUEST['start'];
	$end = $_REQUEST['end'];
	
	$option = "_dt_reservation_mid_{$staffid}_id_{$id}";
	$data = array( 'id' => $id, 'title' => $title, 'body' => $body, 'start'=> $start, 'end'=>$end, 'service'=>$serviceid, 'user'=>$customer, 'readOnly'=>true );
	
	# Sending Mail
	$client_name = $client_phone = $client_email = $client_address = $amount = '';
	
	#Staff
	$staff_name = get_the_title($staffid);
	$service_name = get_the_title($serviceid);

	$sinfo = get_post_meta( $staffid , "_info",true);
	$sinfo = is_array($sinfo) ? $sinfo : array();

	#Client
	if( !empty($data['user']) ){
		$client_name = get_the_title($data['user']);
		$cinfo = get_post_meta( $data['user'], "_info",true);
		$cinfo = is_array($cinfo) ? $cinfo : array();

		$client_email = array_key_exists('emailid', $cinfo) ? $cinfo['emailid'] : "";
		$client_phone = array_key_exists('phone', $cinfo) ? $cinfo['phone'] : "";
		$client_address = array_key_exists('address', $cinfo) ? $cinfo['address'] : "";
	}

	$array = array(
		'staff_name' => $staff_name,
		'service_name' => $service_name,
		'appointment_id' => $data['id'],
		'appointment_time' => $_POST['time'],
		'appointment_date' => $_POST['date'],
		'appointment_title' => $data['title'],
		'appointment_body' =>  $data['body'],
		'client_name' => $client_name,
		'client_phone' => $client_phone,
		'client_email' => $client_email,
		'client_address' => $client_address,
		'company_logo' => 'Company Logo',
		'company_name' => 'Company Name',
		'company_phone' => 'Company Phone',
		'company_address' => 'Company Address',
		'company_website' => 'Company Website');
	
	$subject = pregnancy_option('appointments', 'appointment_notification_to_staff_subject');
	$subject = pregnancy_dt_replace( $subject, $array);

	$message = pregnancy_option('appointments', 'appointment_notification_to_staff_message');
	$message = pregnancy_dt_replace( $message, $array);
	
	#Staff Mail
	pregnancy_dt_send_mail( $sinfo["emailid"], $subject, $message);

	#Client Mail
	if( !empty($client_email) ) {
		$subject = pregnancy_option('appointments', 'appointment_notification_to_client_subject');
		$subject = pregnancy_option( $subject, $array);

		$message = pregnancy_option('appointments', 'appointment_notification_to_client_message');
		$message = pregnancy_option( $message, $array);

		pregnancy_dt_send_mail( $client_email, $subject, $message);
	}


	# Sending Mail
	if( update_option( $option, $data ) ){
		echo json_encode('Success');
	} else {
		echo json_encode('Failed');
	}
	die('');
	
}

?>