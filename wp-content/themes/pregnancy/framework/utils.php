<?php
/* ---------------------------------------------------------------------------
 * Load default theme options
 * - To return default options to store in database.
 * --------------------------------------------------------------------------- */
function pregnancy_default_option() {
	 
	$general = array(
		'show-pagecomments' => 'true',
		'enable-responsive' => 'true',
		'show-mobileslider' => 'true'
	);

	$layout = array(
		'logo' => 'true',
		'show-breadcrumb' => 'true',
		'breadcrumb-delimiter' => 'fa default',
		'show-boxed-layout-pattern-color' => 'true',
		'show-boxed-layout-bg-color' => 'true',
		'site-layout' => 'wide',
		'header-type' => 'fullwidth-header',
		'layout-stickynav' => 'true',
		'header-position' => 'on slider',
		'header-transparant' => 'semi-transparent-header',
		'menu-searchicon' => 'true',
		'menu-title-border-style' => 'dashed',
		'menu-border-style' => 'double',
		'menu-link-arrow-style' => 'double',
		'show-sociables' => 'on',
		'enable-footer' => 'true',
		'footer-columns' => '11',
		'footer-bg-repeat' => 'repeat-x',
		'footer-bg-position' => 'center center',
		'enable-copyright' => 'true',
		'copyright-content' => 'Copyright &copy; 2016 Pregnancy | <a href="http://themeforest.net/user/designthemes" title=""> Design Themes </a>'
	);	
	
	$social = array(
		'social-1' => array(
			'icon' => 'fa-facebook',
			'link' => '#'
		),
		'social-2' => array(
			'icon' => 'fa-twitter',
			'link' => '#'
		),
		'social-3' => array(
			'icon' => 'fa-google-plus',
			'link' => '#'
		)
	);

	$pageoptions = array(
		'show-standard-left-sidebar-for-post-archives' => 'true',
		'show-standard-right-sidebar-for-post-archives' => 'true',
		'single-post-comments' => 'true',
		'post-archives-enable-excerpt' => 'true',
		'post-archives-enable-readmore' => 'true',
		'post-author-meta' => 'true',
		'post-date-meta' => 'true',
		'post-comment-meta' => 'true',
		'post-category-meta' => 'true',
		'post-tag-meta' => 'true',
		'show-standard-left-sidebar-for-portfolio-archives' => 'true',
		'show-standard-right-sidebar-for-portfolio-archives' => 'true',
		'show-notfound-bg-color' => 'true',
		'show-launchdate' => 'true',
		'show-comingsoon-bg-color' => 'true',
		'post-style' => 'dt-sc-blog-content entry-date-left',
		'portfolio-archives-post-style' => 'dt-sc-portfolio-content',
		'notfound-style' => 'preg-404',
		'enable-404message' => 'true'
	);

	$woo = array(
		'product-style' => 'preg-shop',
		'show-shop-standard-left-sidebar-for-product-layout' => 'true',
		'show-shop-standard-right-sidebar-for-product-layout' => 'true',
		'show-shop-standard-left-sidebar-for-product-category-layout' => 'true',
		'show-shop-standard-right-sidebar-for-product-category-layout' => 'true',
		'show-shop-standard-left-sidebar-for-product-tag-layout' => 'true',
		'show-shop-standard-right-sidebar-for-product-tag-layout' => 'true'
	);

	$colors = array(
		'theme-skin' => 'pink',
		'footer-text-color' => '#959595',
		'footer-link-color' => '#959595'
		
	);

	$fonts = array(
		'content-font' => 'Open Sans',
		'menu-font' => 'Quicksand',
		'pagetitle-font' => 'Raleway',
		'h1-font' => 'Quicksand',
		'h2-font' => 'Quicksand',
		'h3-font' => 'Quicksand',
		'h4-font' => 'Quicksand',
		'h5-font' => 'Quicksand',
		'h6-font' => 'Quicksand',
		'font-style' => array( '300', '300italic', '400', '400italic', '500', '600', '700', '800' ),
		'content-font-size' => '18',
		'menu-font-size' => '16',
		'h1-font-size' => '40',
		'h2-font-size' => '30',
		'h3-font-size' => '28',
		'h4-font-size' => '24',
		'h5-font-size' => '22',
		'h6-font-size' => '18',
		'menu-letter-spacing' => '0.75px',
		'body-line-height' => '30'
	);
	
	$pregnancy_appointment = array(
		'currency' => 'USD',
		
		'dt_company_monday_start' => '08:00',
		'dt_company_monday_end' => '17:00',

		'dt_company_tuesday_start' => '08:00',
		'dt_company_tuesday_end' => '17:00',

		'dt_company_wednesday_start' => '08:00',
		'dt_company_wednesday_end' => '17:00',

		'dt_company_thursday_start' => '08:00',
		'dt_company_thursday_end' => '17:00',

		'dt_company_friday_start' => '08:00',
		'dt_company_friday_end' => '18:00',

		'notification_sender_name' => get_option( 'blogname' ),
		'notification_sender_email' => get_option( 'admin_email' ),
		
		'appointment_notification_to_staff_subject' =>  'Hi [STAFF_NAME] , New booking information ( Booking id: [APPOINTMENT_ID] )',
		'appointment_notification_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
	<p> Your new Booking id : [APPOINTMENT_ID] </p>
	<p> Service: [SERVICE]</p>
	<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
	<p>Client Name: [CLIENT_NAME]</p>
	<p>Client Phone: [CLIENT_PHONE]</p>
	<p>Client Email: [CLIENT_EMAIL]</p>
	<p>[APPOINTMENT_BODY]</p>',
	
	
		'modified_appointment_notification_to_staff_subject' => 'Hi [STAFF_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Modified',
		'modified_appointment_notification_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
		<p> Your Booking id : [APPOINTMENT_ID]  was modified </p>
		<p> Service: [SERVICE]</p>
		<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
		<p>Client Name: [CLIENT_NAME]</p>
		<p>Client Phone: [CLIENT_PHONE]</p>
		<p>Client Email: [CLIENT_EMAIL]</p>
		<p>[APPOINTMENT_BODY]</p>',
		
		'deleted_appointment_notification_to_staff_subject' => 'Hi [STAFF_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Deleted / Declined',
		'deleted_appointment_notification_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
		<p> Booking id : [APPOINTMENT_ID]  was Deleted / Declined </p>
		<p> Service: [SERVICE]</p>
		<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
		<p>Client Name: [CLIENT_NAME]</p>
		<p>Client Phone: [CLIENT_PHONE]</p>
		<p>Client Email: [CLIENT_EMAIL]</p>
		<p>[APPOINTMENT_BODY]</p>',
		
		'agenda_to_staff_subject' => 'Hi [STAFF_NAME] , Your Agenda for [TOMORROW]',
		'agenda_to_staff_message' => '<p> Hello [STAFF_NAME], </p>
	<p>Your agenda for tomorrow is </p>
	<p>[TOMORROW_AGENDA]</p>',
	
	'appointment_notification_to_admin_subject' =>  'Hi [ADMIN_NAME] , New booking information ( Booking id: [APPOINTMENT_ID] )',
		'appointment_notification_to_admin_message' => '<p> Hello [ADMIN_NAME], </p>
	<p> New Booking id : [APPOINTMENT_ID] </p>
	<p> Service: [SERVICE]</p>
	<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
	<p>Client Name: [CLIENT_NAME]</p>
	<p>Client Phone: [CLIENT_PHONE]</p>
	<p>Client Email: [CLIENT_EMAIL]</p>
        <p>Client Amount to pay : [AMOUNT]</p>
        <p>Staff Name: [STAFF_NAME]</p>
	<p>[APPOINTMENT_BODY]</p>',
	
	
		'modified_appointment_notification_to_admin_subject' => 'Hi [ADMIN_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Modified',
		'modified_appointment_notification_to_admin_message' => '<p> Hello [ADMIN_NAME], </p>
	<p> New Booking id : [APPOINTMENT_ID] </p>
	<p> Service: [SERVICE]</p>
	<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
	<p>Client Name: [CLIENT_NAME]</p>
	<p>Client Phone: [CLIENT_PHONE]</p>
	<p>Client Email: [CLIENT_EMAIL]</p>
        <p>Client Amount to pay : [AMOUNT]</p>
        <p>Staff Name: [STAFF_NAME]</p>
	<p>[APPOINTMENT_BODY]</p>',
		
		'deleted_appointment_notification_to_admin_subject' => 'Hi [ADMIN_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Deleted / Declined',
		'deleted_appointment_notification_to_admin_message' => '<p> Hello [ADMIN_NAME], </p>
	<p> New Booking id : [APPOINTMENT_ID] </p>
	<p> Service: [SERVICE]</p>
	<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
	<p>Client Name: [CLIENT_NAME]</p>
	<p>Client Phone: [CLIENT_PHONE]</p>
	<p>Client Email: [CLIENT_EMAIL]</p>
        <p>Client Amount to pay : [AMOUNT]</p>
        <p>Staff Name: [STAFF_NAME]</p>
	<p>[APPOINTMENT_BODY]</p>',
	

		'appointment_notification_to_client_subject' => 'Hi [CLIENT_NAME] , New booking information ( Booking id: [APPOINTMENT_ID] )',
		'appointment_notification_to_client_message' => '<p> Hello [CLIENT_NAME], </p>
	<p> Your new Booking id : [APPOINTMENT_ID] </p>
	<p> Service: [SERVICE]</p>
	<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
	<p> Amount to pay : [AMOUNT]</p>
	<p>[APPOINTMENT_BODY]</p>
	<p></p>
	<p>Thank you for choosing our company.</p>',

		'modified_appointment_notification_to_client_subject' => 'Hi [CLIENT_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Modified',
		'modified_appointment_notification_to_client_message' => '<p> Hello [CLIENT_NAME], </p>
	<p> Your Booking id : [APPOINTMENT_ID]  was modified </p>
	<p> Service: [SERVICE]</p>
	<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
	<p> Amount to pay : [AMOUNT]</p>
	<p>[APPOINTMENT_BODY]</p>
	<p></p>
	<p>Thank you for choosing our company.</p>',
		
		'deleted_appointment_notification_to_client_subject' => 'Hi [CLIENT_NAME] , ( Booking id: [APPOINTMENT_ID] ) - Deleted / Declined',
		'deleted_appointment_notification_to_client_message' => '<p> Hello [CLIENT_NAME], </p>
		<p> Your Booking id : [APPOINTMENT_ID]  was Deleted / Declined </p>
		<p> Service: [SERVICE]</p>
		<p> Date & Time: [APPOINTMENT_DATE] - [APPOINTMENT_TIME] </p>
		<p>[APPOINTMENT_BODY]</p>',
		
		'success_message' => 'Thanks for Contacting Us',
		
		'error_message' => 'Sorry, please try again later'		
	
	);
	
	$data = array(
		'general' => $general,
		'layout'  => $layout,
		'social'  => $social,
		'pageoptions' => $pageoptions,
		'woo'	  => $woo,
		'colors'  => $colors,
		'fonts'   => $fonts,
		'pregnancy_appointment' => $pregnancy_appointment
	);
	return $data;
}

/* ---------------------------------------------------------------------------
 * Check activated plugins
 * --------------------------------------------------------------------------- */
function pregnancy_is_plugin_active($plugin) {
	return in_array( $plugin, (array) get_option( 'active_plugins', array() ) ) || pregnancy_is_plugin_active_for_network( $plugin );
}

function pregnancy_is_plugin_active_for_network( $plugin ) {
	if ( !is_multisite() )
		return false;

	$plugins = get_site_option( 'active_sitewide_plugins');
	if ( isset($plugins[$plugin]) )
		return true;

	return false;
}

/* ---------------------------------------------------------------------------
 * Load default theme options
 * - To return default options to store in database.
 * --------------------------------------------------------------------------- */
function pregnancy_show_footer_widgetarea( $count ) {
	$classes = array (
		"1" => "dt-sc-full-width",
		"dt-sc-one-half",
		"dt-sc-one-third",
		"dt-sc-one-fourth",
		"1-2" => "dt-sc-one-half",
		"1-3" => "dt-sc-one-third",
		"1-4" => "dt-sc-one-fourth",
		"3-4" => "dt-sc-three-fourth",
		"2-3" => "dt-sc-two-third",
		"2-6" => "dt-sc-two-sixth",
		"1-6" => "dt-sc-one-sixth" );

	if ($count <= 4) :
		for($i = 1; $i <= $count; $i ++) :

			$class = $classes [$count];
			$class = esc_attr( $class );

			$first = ($i == 1) ? "first" : "";
			$first = esc_attr( $first );

			echo "<div class='column {$class} {$first}'>";
				if (function_exists ( 'dynamic_sidebar' ) && dynamic_sidebar ( "footer-sidebar-{$i}" )) : endif;
			echo "</div>";
		endfor;
	elseif ($count == 5 || $count == 6) :

		$a = array (
			"1-4",
			"1-4",
			"1-2" );

		$a = ($count == 5) ? $a : array_reverse ( $a );
		foreach ( $a as $k => $v ) :
			$class = $classes [$v];
			$class = esc_attr( $class );

			$first = ($k == 0 ) ? "first" : "";
			$first = esc_attr( $first );

			echo "<div class='column {$class} {$first}'>";
				if (function_exists ( 'dynamic_sidebar' ) && dynamic_sidebar ( "footer-sidebar-{$k}-{$v}" )) : endif;
			echo "</div>";
		endforeach;
	elseif ($count == 7 || $count == 8) :
		$a = array (
			"1-4",
			"3-4");

		$a = ($count == 7) ? $a : array_reverse ( $a );
		foreach ( $a as $k => $v ) :
			$class = $classes [$v];
			$class = esc_attr( $class );

			$first = ($k == 0 ) ? "first" : "";
			$first = esc_attr( $first );


			echo "<div class='column {$class} {$first}'>";
				if (function_exists ( 'dynamic_sidebar' ) && dynamic_sidebar ( "footer-sidebar-{$k}-{$v}" )) :endif;
			echo "</div>";
		endforeach;
	elseif ($count == 9 || $count == 10) :
		$a = array ( 
			"1-3",
			"2-3" );
		$a = ($count == 9) ? $a : array_reverse ( $a );

		foreach ( $a as $k => $v ) :
			$class = $classes [$v];
			$class = esc_attr( $class );

			$first = ($k == 0 ) ? "first" : "";
			$first = esc_attr( $first );

			echo "<div class='column {$class} {$first}'>";
				if (function_exists ( 'dynamic_sidebar' ) && dynamic_sidebar ( "footer-sidebar-{$k}-{$v}" )) :endif;
			echo "</div>";
		endforeach;
	elseif ($count == 11 ) :
		$a = array (
			"2-6",
			"1-6",
			"1-6",
			"2-6");

		$a = ($count == 11) ? $a : array_reverse ( $a );
		foreach ( $a as $k => $v ) :
			$class = $classes [$v];
			$class = esc_attr( $class );

			$first = ($k == 0 ) ? "first" : "";
			$first = esc_attr( $first );


			echo "<div class='column {$class} {$first}'>";
				if (function_exists ( 'dynamic_sidebar' ) && dynamic_sidebar ( "footer-sidebar-{$k}-{$v}" )) :endif;
			echo "</div>";
		endforeach;
	endif;
}?>