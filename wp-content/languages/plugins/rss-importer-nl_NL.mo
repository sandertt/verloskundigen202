��          �            h  $   i     �     �  �   �  
   �     �  Q   �     #     5     K     O     \  1   r     �  �   �  .   �     �     �         	  &     a   >     �     �     �     �     �  1        7                                                     	             
              All done. <a href="%s">Have fun!</a> Couldn&#8217;t get post ID Done! Howdy! This importer allows you to extract posts from an RSS 2.0 file into your WordPress site. This is useful if you want to import your posts from a system that is not handled by a custom import tool. Pick an RSS file to upload and click Import. Import RSS Import posts from an RSS feed. Import posts from an RSS feed. This plugin depends on the WP_Importer base class. Importing post... Post already imported RSS RSS Importer http://wordpress.org/ http://wordpress.org/extend/plugins/rss-importer/ wordpressdotorg PO-Revision-Date: 2015-05-06 10:15:25+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Development (trunk)
 Alles is klaar. <a href="%s">Veel plezier!</a> Kan bericht ID niet vinden Klaar! Hoi! De importeerder stelt je in staat om artikelen vanuit een RSS feed in WordPress te importeren. Dit is nuttig als je een blog wil importeren waar geen speciale importeerder voor is geschreven.
Kies een RSS bestand die je wilt uploaden en klik op 'importeer'. Importeer RSS Berichten importeren van een RSS feed. Berichten importeren van een RSS feed. Deze plugin is afhankelijk van de WP_Importer basisklasse. Bericht aan het importeren... Bericht is al geïmporteerd RSS RSS importeerder http://wordpress.org/ http://wordpress.org/extend/plugins/rss-importer/ wordpressdotorg 